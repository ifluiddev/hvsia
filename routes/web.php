<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', 'FrontEndPagesController@index');
Route::get('/constitution', 'FrontEndPagesController@constitution');
Route::get('/locations', 'FrontEndPagesController@locations');
Route::get('/contact-us', 'FrontEndPagesController@contactus');*/

// Frontend routes

/*
|--------------------------------------------------------------------------
| Web Routes Social Media and front-end
|--------------------------------------------------------------------------
|
| CMS Routes
|
|
|
*/
Route::get('/',[
    'uses' => 'CMSController@index',
    'as' => 'frontend-home',
]);

Route::get('/page/{pageSlug}',[
    'uses' => 'CMSController@cms',
]);

Route::get('/constitution', 'FrontEndPagesController@constitution');
Route::get('/locations', 'FrontEndPagesController@locations');
Route::get('/contact-us', 'FrontEndPagesController@contactus');
Route::get('/gallery', 'FrontEndPagesController@gallery');
Route::get('/meeting', 'FrontEndPagesController@meeting');

//Search
Route::get('/search', 'SearchController@search');

Route::post('/save-contact-message',[
    'uses' => 'ContactController@store',
    'as' => 'post.contact-message',
]);


/******************************************************
 *     Auth Routes                                   *
 *                                                    *
 ******************************************************/
Route::group(['middleware' => ['web']], function() {


// Login Routes...
    Route::post('login', ['as' => 'login.post', 'uses' => 'Auth\LoginController@login']);
    Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
    Route::post('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);



// Registration Routes...
    //Route::get('register', ['as' => 'show.register', 'uses' => 'Auth\RegisterController@showRegisterationForm']);
    //Route::post('register', ['as' => 'register', 'uses' => 'Auth\RegisterController@register']);


    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

    //Email verification route
    $this->get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
    $this->get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
    $this->get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');


});


/******************************************************
 *     Member Routes                                   *
 *                                                    *
 ******************************************************/
Route::group(['prefix' => 'member','as' => 'member.'],function () {


    Route::get('',[
        'uses'=>'Member\DashboardController@index',
        'as'=>'show.index',
    ]);


    ////////// Manage Meetings //////////
    Route::get('/meetings',[
        'uses'=>'Member\MeetingController@index',
        'as'=>'meetings.index',
    ]);


    Route::get('/meetings/{id}',[
        'uses'=>'Member\MeetingController@show',
        'as'=>'meetings.show',
    ]);

    ////////// Manage Matrices //////////
    Route::get('/matrices',[
        'uses'=>'Member\MatrixController@index',
        'as'=>'matrices.index',
    ]);
    Route::get('/matrices/{id}',[
        'uses'=>'Member\MatrixController@show',
        'as'=>'matrices.show',
    ]);

    ////////// Manage Matrix tables //////////
    Route::get('/matrix/table',[
        'uses'=>'Member\MatrixTableController@index',
        'as'=>'matrix.tables.index',
    ]);



    ////////// Manage Competency Areas //////////
    Route::get('/competency-areas/{id}',[
        'uses'=>'Member\CompetencyAreaController@show',
        'as'=>'competency-areas.show',
    ]);

    ////////// Manage Test //////////
    /// Route::get('/matrices',[
    Route::get('/tests',[
        'uses'=>'Member\TestController@index',
        'as'=>'tests.index',
    ]);

    Route::get('/tests/{id}',[
        'uses'=>'Member\TestController@show',
        'as'=>'tests.show',
    ]);

    Route::post('/tests',[
        'uses'=>'Member\TestController@store',
        'as'=>'tests.store',
    ]);

    Route::put('/tests/{id}',[
        'uses'=>'Member\TestController@update',
        'as'=>'tests.update',
    ]);
    Route::delete('/tests/{id}',[
        'uses'=>'Member\TestController@destroy',
        'as'=>'tests.destroy',
    ]);


    ////////// Manage Meetings //////////

    Route::get('/test/publications',[
        'uses'=>'Member\TestPublicationController@index',
        'as'=>'test.publications.index',
    ]);

    Route::post('/test/publications',[
        'uses'=>'Member\TestPublicationController@store',
        'as'=>'test.publications.store',
    ]);

    Route::delete('/test/publications/{id}',[
        'uses'=>'Member\TestPublicationController@destroy',
        'as'=>'test.publications.destroy',
    ]);

});


/******************************************************
 *     Admin Routes                                   *
 *                                                    *
 ******************************************************/
Route::group(['prefix' => 'admin','as' => 'admin.'],function () {

    Route::get('',[
        'uses'=>'Backend\DashboardController@index',
        'as'=>'show.index',
    ]);

    Route::get('login',[
        'uses'=>'Auth\Admin\LoginController@showLogin',
        'as'=>'show.login',
        //'middleware'=>'auth:admin',
    ]);

    Route::post('login',[
        'uses'=>'Auth\Admin\LoginController@login',
        'as'=>'login',
    ]);

    Route::get('logout',[
        'uses'=>'Auth\Admin\LoginController@logout',
        'as'=>'logout',
        //'middleware'=>'auth:admin',
    ]);

        /*
    | Web Routes Admin backend
    |--------------------------------------------------------------------------
    | CMS level routes
    */
    //
    Route::get('manage-cms',array(
        'uses'=>'Backend\CMSController@showManageCMS',
        'as'=>'show.manage-cms',
        'middleware' => 'auth:admin',
    ));

    //Page Preview
    Route::get('page-preview/{pageSlug}',[
        'uses' => 'Backend\CMSController@cms',
        'as'=>'show.page-preview',
        'middleware' => 'auth:admin',
    ]);

    Route::get('/page-preview/',[
        'uses' => 'Backend\CMSController@index',
        'as'=>'show.page-preview-home',
        'middleware' => 'auth:admin',
    ]);


    //publishContent
    Route::post('publish_content/{id}',array(
        'uses'=>'Backend\CMSController@publishContent',
        'as'=>'publish_content',
        'middleware' => 'auth:admin',
    ));



    //nEW ROUTE FOR MENUS

    Route::prefix('ajax/page')->group(function () {
        Route::post('all',array(
            'uses'=>'Backend\CMSController@getMenus',
            'as'=>'ajax.get.pages',
            'middleware' => 'auth:admin',
        ));
        Route::post('create',array(
            'uses'=>'Backend\CMSController@createMenu',
            'as'=>'ajax.create.page',
            'middleware' => 'auth:admin',
        ));
        Route::post('update',array(
            'uses'=>'Backend\CMSController@updateMenu',
            'as'=>'ajax.update.page',
            'middleware' => 'auth:admin',
        ));
        Route::post('delete',array(
            'uses'=>'Backend\CMSController@deleteMenu',
            'as'=>'ajax.delete.page',
            'middleware' => 'auth:admin',
        ));
        Route::post('dnd',array(
            'uses'=>'Backend\CMSController@dragAndDropMenu',
            'as'=>'ajax.dnd.page',
            'middleware' => 'auth:admin',
        ));
        Route::post('detail',array(
            'uses'=>'Backend\CMSController@getPageDetail',
            'as'=>'ajax.get.page-detail',
            'middleware' => 'auth:admin',
        ));
        Route::post('update-page-detail',array(
            'uses'=>'Backend\CMSController@updatePage',
            'as'=>'ajax.update.page-detail',
            'middleware' => 'auth:admin',
        ));

        //footer menu
        Route::post('footer/all',array(
            'uses'=>'Backend\CMSController@getFooterMenus',
            'as'=>'ajax.get.footer-menus',
            'middleware' => 'auth:admin',
        ));
        Route::post('footer/create',array(
            'uses'=>'Backend\CMSController@createFooterMenu',
            'as'=>'ajax.create.footer-menu',
            'middleware' => 'auth:admin',
        ));
        Route::post('footer/update',array(
            'uses'=>'Backend\CMSController@updateFooterMenu',
            'as'=>'ajax.update.footer-menu',
            'middleware' => 'auth:admin',
        ));
        Route::post('footer/delete',array(
            'uses'=>'Backend\CMSController@deleteFooterMenu',
            'as'=>'ajax.delete.footer-menu',
            'middleware' => 'auth:admin',
        ));
        Route::post('footer/dnd',array(
            'uses'=>'Backend\CMSController@dragAndDropFooterMenu',
            'as'=>'ajax.dnd.footer-menu',
            'middleware' => 'auth:admin',
        ));
        //publishContent
        Route::post('publish',array(
            'uses'=>'Backend\CMSController@publishContent',
            'as'=>'ajax.publish.page',
            'middleware' => 'auth:admin',
        ));
    });


    //Manage Banner
    Route::get('/manage-banner/',[
        'uses' => 'Backend\CMSController@showManageBanner',
        'as'=>'show.manage-banner',
        'middleware' => 'auth:admin',
    ]);

    Route::get('/create-banner/',[
        'uses' => 'Backend\CMSController@showCreateBanner',
        'as'=>'show.create-banner',
        'middleware' => 'auth:admin',
    ]);

    Route::get('/edit-banner/{id}',[
        'uses' => 'Backend\CMSController@showEditBanner',
        'as'=>'show.edit-banner',
        'middleware' => 'auth:admin',
    ]);


    Route::post('/post-banner/',[
        'uses' => 'Backend\CMSController@postBanner',
        'as'=>'post.banner',
        'middleware' => 'auth:admin',
    ]);

    Route::post('/update-banner/',[
        'uses' => 'Backend\CMSController@updateBanner',
        'as'=>'update.banner',
        'middleware' => 'auth:admin',
    ]);

    Route::post('/update-banner-image/',[
        'uses' => 'Backend\CMSController@updateBannerImage',
        'as'=>'update.banner-image',
        'middleware' => 'auth:admin',
    ]);

    Route::post('/update-banner-icon/',[
        'uses' => 'Backend\CMSController@updateBannerIcon',
        'as'=>'update.banner-icon',
        'middleware' => 'auth:admin',
    ]);

    Route::post('/delete-banner',[
        'uses' => 'Backend\CMSController@deleteBanner',
        'as'=>'delete.banner',
        'middleware' => 'auth:admin',
    ]);

    Route::post('/publish-banner/{id}',[
        'uses' => 'Backend\CMSController@updateStatus',
        'as'=>'post.publish-banner',
        'middleware' => 'auth:admin',
    ]);

    //Manage Gallery
    Route::get('/manage-gallery/',[
        'uses' => 'Backend\CMSController@showManageGallery',
        'as'=>'show.manage-gallery',
        'middleware' => 'auth:admin',
    ]);

    Route::get('/create-gallery/',[
        'uses' => 'Backend\CMSController@showCreateGallery',
        'as'=>'show.create-gallery',
        'middleware' => 'auth:admin',
    ]);

    Route::get('/edit-gallery/{id}',[
        'uses' => 'Backend\CMSController@showEditGallery',
        'as'=>'show.edit-gallery',
        'middleware' => 'auth:admin',
    ]);


    Route::post('/post-gallery/',[
        'uses' => 'Backend\CMSController@postGallery',
        'as'=>'post.gallery',
        'middleware' => 'auth:admin',
    ]);

    Route::post('/update-gallery/',[
        'uses' => 'Backend\CMSController@updateGallery',
        'as'=>'update.gallery',
        'middleware' => 'auth:admin',
    ]);

    Route::post('/delete-gallery',[
        'uses' => 'Backend\CMSController@deleteGallery',
        'as'=>'delete.gallery',
        'middleware' => 'auth:admin',
    ]);

    Route::post('/update-gallery-image/',[
        'uses' => 'Backend\CMSController@updateGalleryImage',
        'as'=>'update.gallery-image',
        'middleware' => 'auth:admin',
    ]);

    Route::post('/publish-gallery/{id}',[
        'uses' => 'Backend\CMSController@updateGalleryStatus',
        'as'=>'post.publish-gallery',
        'middleware' => 'auth:admin',
    ]);

    //Manage Category


    Route::post('/post-category/',[
        'uses' => 'Backend\CMSController@postCategory',
        'as'=>'post.category',
        'middleware' => 'auth:admin',
    ]);

    Route::post('/update-category/',[
        'uses' => 'Backend\CMSController@updateCategory',
        'as'=>'update.category',
        'middleware' => 'auth:admin',
    ]);

    Route::post('/delete-category',[
        'uses' => 'Backend\CMSController@deleteCategory',
        'as'=>'delete.category',
        'middleware' => 'auth:admin',
    ]);

    //Manage Admin
    Route::get('/admins/',[
        'uses' => 'Backend\AdminController@index',
        'as'=>'admins.index',
        'middleware' => 'auth:admin',
    ]);

    Route::get('/admins/create/',[
        'uses' => 'Backend\AdminController@create',
        'as'=>'admins.create',
        'middleware' => 'auth:admin',
    ]);

    Route::get('/admins/edit/{id}',[
        'uses' => 'Backend\AdminController@edit',
        'as'=>'admins.edit',
        'middleware' => 'auth:admin',
    ]);


    Route::post('/admins/',[
        'uses' => 'Backend\AdminController@store',
        'as'=>'admins.store',
        'middleware' => 'auth:admin',
    ]);

    Route::put('/admins/{id}',[
        'uses' => 'Backend\AdminController@update',
        'as'=>'admins.update',
        'middleware' => 'auth:admin',
    ]);

    Route::delete('/admins',[
        'uses' => 'Backend\AdminController@destroy',
        'as'=>'admins.destroy',
        'middleware' => 'auth:admin',
    ]);

    //Manage Location
    Route::resource('locations', 'Backend\LocationController');

    //Manage User
    Route::get('/users/',[
        'uses' => 'Backend\UserController@index',
        'as'=>'users.index',
        'middleware' => 'auth:admin',
    ]);

    Route::get('/users/create',[
        'uses' => 'Backend\UserController@create',
        'as'=>'users.create',
        'middleware' => 'auth:admin',
    ]);

    Route::get('/users/edit/{id}',[
        'uses' => 'Backend\UserController@edit',
        'as'=>'users.edit',
        'middleware' => 'auth:admin',
    ]);


    Route::post('/users',[
        'uses' => 'Backend\UserController@store',
        'as'=>'users.store',
        'middleware' => 'auth:admin',
    ]);

    Route::put('/users/{id}',[
        'uses' => 'Backend\UserController@update',
        'as'=>'users.update',
        'middleware' => 'auth:admin',
    ]);

    Route::delete('/destroy',[
        'uses' => 'Backend\UserController@destroy',
        'as'=>'users.destroy',
        'middleware' => 'auth:admin',
    ]);



    ////////// Manage Meetings //////////
    Route::get('/meetings',[
        'uses'=>'Backend\MeetingController@index',
        'as'=>'meetings.index',
    ]);
    Route::get('/meetings/create',[
        'uses'=>'Backend\MeetingController@create',
        'as'=>'meetings.create',
        'middleware' => 'auth:admin'
    ]);

    Route::post('/meetings',[
        'uses'=>'Backend\MeetingController@store',
        'as'=>'meetings.store',
        'middleware' => 'auth:admin',
    ]);
    Route::get('/meetings/{id}',[
        'uses'=>'Backend\MeetingController@show',
        'as'=>'meetings.show',
        'middleware' => 'auth:admin',
    ]);
    Route::get('/meetings/edit/{id}',[
        'uses'=>'Backend\MeetingController@edit',
        'as'=>'meetings.edit',
        'middleware' => 'auth:admin',
    ]);
    Route::put('/meetings/{id}',[
        'uses'=>'Backend\MeetingController@update',
        'as'=>'meetings.update',
    ]);
    Route::delete('/meetings',[
        'uses' => 'Backend\MeetingController@destroy',
        'as'=>'meetings.destroy',
        'middleware' => 'auth:admin',
    ]);

    ////////// Meeting Agenda //////////
    Route::put('/meeting/agendas/{id}',[
        'uses'=>'Backend\MeetingAgendaUploadController@update',
        'as'=>'meeting.agendas.update',
        'middleware' => 'auth:admin',
    ]);
    ////////// Meeting Minute //////////
    Route::put('/meeting/minutes/{id}',[
        'uses'=>'Backend\MeetingMinuteUploadController@update',
        'as'=>'meeting.minutes.update',
        'middleware' => 'auth:admin',
    ]);
    ////////// Meeting Presentation //////////
    Route::put('/meeting/presentations/{id}',[
        'uses'=>'Backend\MeetingPresentationUploadController@update',
        'as'=>'meeting.presentations.update',
        'middleware' => 'auth:admin',
    ]);

    Route::delete('/meeting/presentations/{id}',[
        'uses'=>'Backend\MeetingPresentationUploadController@destroy',
        'as'=>'meeting.presentations.destroy',
        'middleware' => 'auth:admin',
    ]);

    ////////// Competency Area //////////
    Route::resource('competency-areas', 'Backend\CompetencyAreaController');
    ////////// Focus Area //////////
    Route::resource('focus-areas', 'Backend\FocusAreaController');
    ////////// Focus Area image Upload //////////
    Route::put('/focus-areas/images/{id}',[
        'uses'=>'Backend\FocusAreaImageUploadController@update',
        'as'=>'focus-areas.images.update',
        'middleware' => 'auth:admin',
    ]);


    ////////// Manage Test //////////

    Route::get('/tests',[
        'uses'=>'Backend\TestController@index',
        'as'=>'tests.index',
    ]);

    Route::get('/tests/create',[
        'uses'=>'Backend\TestController@create',
        'as'=>'tests.create',
    ]);

    Route::get('/tests/{id}',[
        'uses'=>'Backend\TestController@show',
        'as'=>'tests.show',
    ]);

    Route::post('/tests',[
        'uses'=>'Backend\TestController@store',
        'as'=>'tests.store',
    ]);

    Route::get('/tests/edit/{id}',[
        'uses'=>'Backend\TestController@edit',
        'as'=>'tests.edit',
    ]);

    Route::put('/tests/{id}',[
        'uses'=>'Backend\TestController@update',
        'as'=>'tests.update',
    ]);
    Route::delete('/tests/{id}',[
        'uses'=>'Backend\TestController@destroy',
        'as'=>'tests.destroy',
    ]);

    ////////// Manage Mark Test Completed //////////

    Route::put('/test/mark-test-completed/{id}',[
        'uses'=>'Backend\MarkTestCompletedController@update',
        'as'=>'test.mark-test-completed.update',
    ]);


    ////////// Manage Matrix tables //////////
    Route::get('/matrix/table',[
        'uses'=>'Backend\MatrixTableController@index',
        'as'=>'matrix.tables.index',
    ]);
});

