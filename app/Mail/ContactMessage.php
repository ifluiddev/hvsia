<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMessage extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $name,$email,$myMessage,$phone_number,$type;
    public function __construct($name,$email,$message,$phone_number,$country_email,$type)
    {
        $this->name = $name;
        $this->email = $email;
        $this->myMessage = $message;
        $this->phone_number = $phone_number;
        $this->type = $type;
        $this->country_email = $country_email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->type == 'admin'){
            return $this->from($this->email)->view('emails.admin-contact-email');
        }
        else{
            return $this->from($this->country_email)->view('emails.user-contact-email');
        }

    }
}
