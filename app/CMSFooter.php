<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CMSFooter extends Model
{

    protected $fillable = [
        'cms_id',
    ];
    protected $table = 'cms_footers';

    public function cms(){
        return $this->belongsTo('App\CMS','cms_id');
    }
}
