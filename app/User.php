<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name', 'email', 'password','phone_number','location_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getFullname(){
        return $this->first_name . " " . $this->last_name;
    }

    public function location(){
        return $this->belongsTo('App\Location','location_id');
    }

    public function tests(){
        return $this->hasMany('App\Test','user_id');
    }

    public function completedTests(){
        return $this->tests()->where('completed',true)->get();
    }

    public function inProgressTests(){
        return $this->tests()->where('completed',false)->get();
    }
}
