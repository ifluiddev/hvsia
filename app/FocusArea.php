<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FocusArea extends Model
{
    protected $fillable = [
        'name','image'
    ];

    public function competency_areas(){
        return $this->belongsToMany('App\CompetencyArea','focus_competencies','focus_area_id','competency_area_id');
    }

    public function tests(){
        return $this->hasMany('App\Test','focus_area_id');
    }

    public function getLatestTest(){
        return $this->tests()->latest()->first();
    }

    public function latestCompletedTest(){
        return $this->tests()->where('completed',true)->latest()->first();
    }

    public function numberOfTestInProgressOnACompetencyArea($competency_area_id){
        /*THis is old query. I might use it if the present does not work for all conditions
         * $count =DB::table('focus_competencies')->join('focus_areas','focus_areas.id','=','focus_competencies.focus_area_id')
            ->join('competency_areas','competency_areas.id','=','focus_competencies.competency_area_id')
            ->join('test_competencies','test_competencies.competency_area_id','=','focus_competencies.competency_area_id')
            ->join('tests','tests.id','=','test_competencies.test_id')
            ->where('focus_areas.id',$this->id)
            ->where('tests.completed',false)
            ->where('competency_areas.id',$competency_area_id)
            ->count();*/

        $count =DB::table('test_competencies')
            ->join('competency_areas','competency_areas.id','=','test_competencies.competency_area_id')
            ->join('tests','tests.id','=','test_competencies.test_id')
            ->join('focus_areas','focus_areas.id','=','tests.focus_area_id')
            ->where('focus_areas.id',$this->id)
            ->where('tests.completed',false)
            ->where('competency_areas.id',$competency_area_id)
            ->count();

        return $count;
    }

    public function numberOfTestCompletedOnACompetencyArea($competency_area_id){
        /*$count = DB::table('focus_competencies')->join('focus_areas','focus_areas.id','=','focus_competencies.focus_area_id')
            ->join('competency_areas','competency_areas.id','=','focus_competencies.competency_area_id')
            ->join('test_competencies','test_competencies.competency_area_id','=','focus_competencies.competency_area_id')
            ->join('tests','tests.id','=','test_competencies.test_id')
            ->where('focus_areas.id',$this->id)
            ->where('tests.completed',true)
            ->where('competency_areas.id',$competency_area_id)
            ->count();*/

        $count =DB::table('test_competencies')
            ->join('competency_areas','competency_areas.id','=','test_competencies.competency_area_id')
            ->join('tests','tests.id','=','test_competencies.test_id')
            ->join('focus_areas','focus_areas.id','=','tests.focus_area_id')
            ->where('focus_areas.id',$this->id)
            ->where('tests.completed',true)
            ->where('competency_areas.id',$competency_area_id)
            ->count();

        return $count;
    }

    public function numberOfCompetencyAreasDone(){

        $count =DB::table('test_competencies')
            ->join('competency_areas','competency_areas.id','=','test_competencies.competency_area_id')
            ->join('tests','tests.id','=','test_competencies.test_id')
            ->join('focus_areas','focus_areas.id','=','tests.focus_area_id')
            ->where('focus_areas.id',$this->id)
            ->distinct()
            ->count();

        return $count;
    }



    public function generateRandomName($name,$value) {
        $filename = $name.date('m-d-Y_hia') . '.' . $value->getClientOriginalExtension();
        return $filename;
    }
}
