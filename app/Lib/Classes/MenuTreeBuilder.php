<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 7/6/18
 * Time: 9:48 AM
 */

namespace App\Lib\Classes;
use App\CMS;
use App\CMSFooter;


class MenuTreeBuilder
{
    public function __construct()
    {

    }

    public function buildMainMenuTree(){
        $tree = array();
        //Create root menu
        $root = new Menu(null,'Root');
        $root->children = array();
        array_push($tree,$root);

        foreach(CMS::tree_menus() as $row) {

            $parent_menu = new Menu($row->id,$row->page_name);
            array_push($root->children,$parent_menu);

            if ($row->tree_sub_menus()) {
                $parent_menu->children = array();
                foreach ($row->tree_sub_menus() as $child){
                    $child_menu = new Menu($child->id,$child->page_name);
                    array_push($parent_menu->children,$child_menu);
                }
            }

        }

        return $tree;
    }

    public function buildFooterMenuTree(){
        $tree = array();
        //Create root menu
        $root = new Menu(null,'Root');
        $root->children = array();
        array_push($tree,$root);

        foreach(CMSFooter::orderBy('menu_order','asc')->get() as $row) {

            $parent_menu = new Menu($row->cms_id,$row->cms->page_name);
            array_push($root->children,$parent_menu);
        }

        return $tree;
    }

}