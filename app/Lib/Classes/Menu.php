<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 7/6/18
 * Time: 9:42 AM
 */

namespace App\Lib\Classes;


use App\CMS;

class Menu
{
    public $id;
    public $text;

    public function __construct($id,$page_name)
    {
        $this->id = $id;
        $this->text = $page_name;
    }

}