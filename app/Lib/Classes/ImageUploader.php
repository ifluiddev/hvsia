<?php


namespace App\Lib\Classes;
use Image;
use Storage;

class ImageUploader {

    public $img = null;

    public function create($value) {
        $this->img = Image::make($value->getRealPath());
        return $this;
    }

    public function generateRandomName($value) {
        $filename = str_random(10).date('m-d-Y_hia') . '.' . $value->getClientOriginalExtension();
        return $filename;
    }

    public function resizeImage($w = 200,$h = 200) {

        $this->img->resize($w, $h);
        return $this;
    }

    public function save($X, $filename, $value) {
        if ($X === 'profile') {
            $this->img->encode($value->getClientOriginalExtension());  //must encode it before saving with put()
            Storage::disk('public')->put(config('filesystems.storage.avatar_path'). $filename, (string) $this->img);
            //$value->move(public_path('uploads').config('filesystems.storage.avatar_path'),$filename);
        }
        if ($X === 'widget') {
            $this->img->encode($value->getClientOriginalExtension());  //must encode it before saving with put()
            Storage::disk('public')->put(config('filesystems.storage.widget_images_path'). $filename, (string) $this->img);

        }
        if ($X === 'banner') {
            $this->img->encode($value->getClientOriginalExtension());  //must encode it before saving with put()
            Storage::disk('public')->put(config('filesystems.storage.banner_path'). $filename, (string) $this->img);

        }
        if ($X === 'partner') {
            $this->img->encode($value->getClientOriginalExtension());  //must encode it before saving with put()
            Storage::disk('public')->put(config('filesystems.storage.partner_logo_path'). $filename, (string) $this->img);

        }
        if ($X === 'banner_overlay') {
            $this->img->encode($value->getClientOriginalExtension());  //must encode it before saving with put()
            Storage::disk('public')->put(config('filesystems.storage.banner_overlay_path'). $filename, (string) $this->img);

        }
        if ($X === 'challenge-image') {
            $this->img->encode($value->getClientOriginalExtension());  //must encode it before saving with put()
            Storage::disk('public')->put(config('filesystems.storage.challenge_image_path'). $filename, (string) $this->img);

        }
        if ($X === 'technology-image') {
            $this->img->encode($value->getClientOriginalExtension());  //must encode it before saving with put()
            Storage::disk('public')->put(config('filesystems.storage.technology_image_path'). $filename, (string) $this->img);

        }

        if ($X === 'product-image') {
            $this->img->encode($value->getClientOriginalExtension());  //must encode it before saving with put()
            Storage::disk('public')->put(config('filesystems.storage.product_image_path'). $filename, (string) $this->img);

        }

        if ($X === 'subscription-image') {
            $this->img->encode($value->getClientOriginalExtension());  //must encode it before saving with put()
            Storage::disk('public')->put(config('filesystems.storage.subscription_image_path'). $filename, (string) $this->img);

        }

        if ($X === 'invoice-logo') {
            $this->img->encode($value->getClientOriginalExtension());  //must encode it before saving with put()
            Storage::disk('public')->put(config('filesystems.storage.invoice_logo_path'). $filename, (string) $this->img);

        }
    }

}
