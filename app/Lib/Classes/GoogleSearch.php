<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/10/18
 * Time: 3:35 PM
 */

namespace App\Lib\Classes;

use GuzzleHttp\Client;

class GoogleSearch
{
    public $client;
    public $cx;
    public $api_key;
    public $domain;
    public function __construct()
    {
        $this->api_key = 'AIzaSyAEZVdDSGtx83A6YX4Fdp0WH76vvYbOQwY';
        $this->cx = '017246075699867113940:6k2i6zcp8lg';
        $this->domain = 'https://www.googleapis.com/customsearch';
        $this->client = new Client(['verify' => true]);
    }


    public function search($q)
    {
        //return $this->getEndpoint().'lists';
        $response = $this->client->get($this->getEndpoint().'&q='.$q, [

        ]);

        return $response;
    }

    public function nextPageSearch($q,$start)
    {
        //return $this->getEndpoint().'lists';
        $response = $this->client->get($this->getEndpoint().'&q='.$q.'&start='.$start, [

        ]);

        return $response;
    }


    public function getBase64Encode()
    {
        $string = $this->username.":".$this->api_key;
        return base64_encode($string);

    }


    public function getEndpoint()
    {
        $string = $this->domain.'/v1?key='.$this->api_key.'&cx='.$this->cx;
        return $string;

    }                                             

}