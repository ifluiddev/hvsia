<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Lib\Classes;

use DOMDocument;
/**
 * Description of ArzornSitemapGenerator
 *
 * @author root
 */
class ArzornSitemapGenerator {
    public $xmlString = '<?xml version="1.0" encoding="UTF-8"?>
    <urlset
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	xmlns:image="http://www.google.com/schemas/sitemap-image/1.1" 
	xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" 
	xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'.PHP_EOL;

    public $sitemapindexString = '<?xml version="1.0" encoding="UTF-8"?>
                                    <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
                                  '.PHP_EOL;

    public function add($url, $lastmod, $changefreg, $priority) {
        $this->xmlString.='<url>' .PHP_EOL.

            '<loc>' . $url . '</loc>' .PHP_EOL.

            '<lastmod>' . $lastmod . '</lastmod>' .PHP_EOL.

            '<changefreq>' . $changefreg . '</changefreq>' .PHP_EOL.

            '<priority>' . $priority . '</priority>' .PHP_EOL.
            '</url>'.PHP_EOL;
    }

    public function addIndex($sitemap, $lastmod) {

        $this->sitemapindexString.='<sitemap>' .PHP_EOL.

            '<loc>' . $sitemap . '</loc>' .PHP_EOL.

            '<lastmod>' . $lastmod . '</lastmod>' .PHP_EOL.

            '</sitemap>'.PHP_EOL;
    }

    public function save($xmlName) {

        $this->xmlString.='</urlset>';

        $dom = new DOMDocument;

        $dom->preserveWhiteSpace = true;

        $dom->loadXML($this->xmlString);

        //Save XML as a file
        $dom->save('/home/vagrant/Code/openix/public/'.$xmlName);
    }

    public function saveIndex($indexName) {

        $this->sitemapindexString.='</sitemapindex>';

        $dom = new DOMDocument;

        $dom->preserveWhiteSpace = true;

        $dom->loadXML($this->sitemapindexString);

        //Save XML as a file
        $dom->save('/home/vagrant/Code/openix/public/'.$indexName);
    }
}
