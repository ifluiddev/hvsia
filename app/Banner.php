<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $fillable = [
        'title', 'content','status','bg_image','icon','btn_1_name','btn_2_name','btn_1_url','btn_2_url'
    ];

    public function generateRandomName($value) {
        $filename = 'icon'.str_random(5).date('m-d-Y_hia') . '.' . $value->getClientOriginalExtension();
        return $filename;
    }
}
