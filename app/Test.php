<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $fillable = [
        'user_id','location_id','focus_area_id','start_date','completed_date','completed'
    ];

    public function location(){
        return $this->belongsTo('App\Location','location_id');
    }

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function focus_area(){
        return $this->belongsTo('App\FocusArea','focus_area_id');
    }

    public function competency_areas(){
        return $this->belongsToMany('App\CompetencyArea','test_competencies','test_id','competency_area_id');
    }

    public function publications(){
        return $this->hasMany('App\TestPublication','test_id');
    }

    public function publicationsForCompetencyArea($competency_area_id){
        return $this->publications()->where('competency_area_id',$competency_area_id)->get();
    }

    public function publicationsForCompetencyAreaCount($competency_area_id){
        return $this->publications()->where('competency_area_id',$competency_area_id)->count();
    }

    public function getStartDateAttribute($value){
        $carbon = new Carbon($value);
        return $carbon->format('Y/m/d');
    }

    public function getCompletedDateAttribute($value){
        $carbon = new Carbon($value);
        return $carbon->format('Y/m/d');
    }

    public function generateRandomName($title,$value) {
        $filename = $title.date('m-d-Y_hia') . '.' . $value->getClientOriginalExtension();
        return $filename;
    }

    public function completedTests(){
        return $this->where('completed',true)->get();
    }

    public function inProgressTests(){
        return $this->where('completed',false)->get();
    }

}
