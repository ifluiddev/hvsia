<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class CMS extends Model
{
    use Sluggable;

    protected $fillable = [
        'page_name','page_parent', 'content','template','page_title','page_meta_description','page_status','menu_order','banner_description','is_index_page','redirect_url','is_visible','external_link'
    ];

    protected $searchable = [
        'page_name','content','page_title','banner_description','page_meta_description'
    ];

    protected $table = 'cms';

    public function sluggable() {
        return [
            'slug' => [
                'source' => ['page_name']
            ]
        ];
    }

    //OnetoOne relationship with CMSFooter
    public function footers() {
        return $this->hasMany('App\CMSFooter', 'cms_id');
    }

    public static function menus() {
        return self::where('page_status',true)->where('page_parent',0)->where('is_visible',1)->orderBy('menu_order','asc')->get();
    }

    public function sub_menus() {
        return $this->where('page_status',true)->where('page_parent',$this->id)->where('is_visible',1)->orderBy('menu_order','asc')->get();
    }

    public static function tree_menus() {
        return self::where('page_parent',0)->orderBy('menu_order','asc')->get();
    }

    public function tree_sub_menus() {
        return $this->where('page_parent',$this->id)->orderBy('menu_order','asc')->get();
    }

    public function tree_has_children(){
        if($this->tree_sub_menus()->count() > 0){
            return true;
        }
        return false;
    }

    public function tree_has_parent(){
        if($this->page_parent > 0){
            return true;
        }
        return false;
    }

    public function is_visible_on_footer() {

        if($this->footers->where('cms_id',$this->id)->count() > 0){
            return true;
        }
        return false;
    }
}
