<?php

namespace App\Http\Controllers\Auth\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Lib\Interfaces\UserInterface;

class LoginController extends Controller {
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct() {
        $this->middleware('guest')->except(['logout']);
        $this->middleware('web')->only(['login']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    
     public function showLogin()
    {
        return view('backend.auth.login');
    } 
    
    protected function authenticate(array $data) {
        
        return Auth::guard('admin')->attempt(['email' => $data['email'], 'password' => $data['password']], isset($data['remember']) ? $data['remember'] : false);
    }

    public function login(Request $request) {

        if ($this->authenticate($request->all())) {
            
                return redirect()->route('admin.show.index');
        }
        return redirect()->back()->with(['message' => 'Ooops! Error while signing in. Ensure your password and email is correct']);
    }

    public function logout() {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.show.login');
    }

}