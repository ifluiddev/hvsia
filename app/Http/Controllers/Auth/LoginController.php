<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Lib\Interfaces\UserInterface;
use App\Lib\Interfaces\ProfileInterface;
use Socialite;
use App\Lib\Classes\SocialAccountService;
use App\SocialAccount;
use URL;
use App\User;

class LoginController extends Controller {

    public $userRepo;
    public $profileRepo;

    public function __construct() {
        $this->middleware('guest')->except(['logout','facebookCallback']);
        $this->middleware('web')->only('login');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    public function showLoginForm(Request $request)
    {

        return view('auth.login');
    }

    protected function authenticate(array $data) {

        return Auth::attempt(['email' => $data['email'], 'password' => $data['password']], isset($data['remember']) ? $data['remember'] : false);
    }

    public function login(Request $request) {

        if ($this->authenticate($request->all())) {

            return redirect()->route('member.show.index');
        }
        return redirect()->back()->with(['message' => 'Ooops! Error while signing in. Ensure your password and email is correct']);
    }

    public function logout() {
        Auth::logout();
        return redirect()->route('login');
    }

    public function facebookRedirect() {

        return Socialite::driver('facebook')->redirect();
    }

    public function facebookCallback(SocialAccountService $service) {

        if (auth()->guest()) {
            $providerUser = Socialite::driver('facebook')->user();
            if ($providerUser->getEmail() == null || $providerUser->getEmail() == '') {
                return redirect()->back()->with('flash_message', 'We are unable to access your email address on facebook. Please Sign up properly');
            } else {
                $segments = explode('/', URL::previous());
                $segment = array_last($segments);
                $user = $service->createOrGetUser($providerUser);
                auth()->login($user, true);
                if ($segment == "") {
                    return redirect()->route('show.profile');
                } else {

                    return redirect()->back();
                }
            }
        } else {

            $providerUser = Socialite::driver('facebook')->user();
            SocialAccount::create([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'facebook',
                'user_id' => auth()->id()
            ]);

            $verify = auth()->user()->verify;
            $verify->facebook = true;
            $verify->save();
            return redirect()->back();
        }
    }

}
