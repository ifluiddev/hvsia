<?php

namespace App\Http\Controllers\Backend;

use App\FocusArea;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class FocusAreaImageUploadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function update(Request $request,$id){

        $focus_area = FocusArea::findOrFail($id);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = $focus_area->generateRandomName($focus_area->name, $file);
            Storage::disk('public')->put(config('filesystems.storage.focus_area_image_path') . $fileName, file_get_contents($file));

            $focus_area->image = $fileName;
            $focus_area->save();

        }

        return redirect()->back();
    }
}
