<?php

namespace App\Http\Controllers\Backend;

use App\CompetencyArea;
use App\FocusArea;
use App\Meeting;
use App\Test;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class MatrixTableController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index() {
        $focus_areas = FocusArea::all();
        $competency_areas = CompetencyArea::all();
        $tests = Test::all();
        $test_competencies = DB::table('test_competencies')->get();
        return view('backend.matrix-table.index',compact('focus_areas','competency_areas','test','test_competencies'));
    }

}
