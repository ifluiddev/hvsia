<?php

namespace App\Http\Controllers\Backend;

use App\Admin;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function index()
    {
        $admins = Admin::all();
        return view('backend.admin.index',compact('admins'));
    }

    public function create()
    {
        $roles = Role::all();
        return view('backend.admin.create',compact('roles'));

    }

    public function edit($id)
    {
        //Edit Admin
        $admin =  Admin::findOrFail($id);
        $roles = Role::all();
        return view('backend.admin.edit',compact('admin','roles'));

    }

    public function store(Request $request)
    {


        $admin =  new Admin();
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'email|max:255|unique:admins',

            'password' => 'required|string|min:6',

        ]);

        $admin->name =  $request->input('name');
        $admin->email = $request->input('email');
        $admin->role_id = $request->input('role_id');


        $admin->password = bcrypt($request->input('password'));

        $admin->save();


        $data = array('name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => $request->input('password')
        );

        Mail::send('emails.admin_details', $data, function ($message) {
            $message->to(Input::get('email'), 'HVSIA')
                ->subject('Administrator Details');
            $message->from(env('MAIL_FROM_ADDRESS', 'admin@hvsia.com'), 'HVSIA');
        });


        Session::flash('message', 'Administrator successfully Created!');
        return redirect()->route('admin.admins.index');

    }

    public function destroy(Request $request){
        $admin = Admin::findOrFail($request->id);
        if($admin->delete()){
            Session::flash('message','Admin Successfully Deleted');
            return redirect()->back();
        }
    }


    public function update(Request $request,$id)
    {

        $data = $request->except(['_token','_method']);


        //Update Page
        Admin::where('id',$id)->update($data);

        return redirect()->back()->with('message','Admin successfully Updated!');

    }
}
