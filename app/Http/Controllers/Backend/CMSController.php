<?php

namespace App\Http\Controllers\Backend;

use App\Gallery;
use App\Http\Controllers\Controller;
use App\Banner;
use App\BannerWidget;
use App\Category;
use App\Challenge;
use App\CMS;
use App\CMSFooter;
use App\Footer;
use App\Lib\Classes\MenuTreeBuilder;
use App\Lib\Classes\TaskType;
use App\Partner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use DB;
use App\Lib\Classes\Menu;
use Illuminate\Support\Facades\Storage;
use stdClass;

class CMSController extends Controller
{
    public $imageUploader;


    public function __construct()
    {
        //$this->imageUploader = app()->make('ImageUploader');
    }


    public function showManageCMS()
    {
        //
        //$pages = CMS::all();
        return view('backend.content.content_manager_view');
    }


    public function index()
    {
        //Preview
        $cms = CMS::where('is_index_page',true)->first();
        if($cms){
            return view('backend.preview.cms_preview',['cms' => $cms]);
        }

    }

    public function cms($pageSlug)
    {
        //$sliders = Banner::all();
        $cms = CMS::where('slug',$pageSlug)->first();

        return view('backend.preview.cms_preview',['cms' => $cms]);

    }


    public function updatePage(Request $request)
    {

        $data = $request->except(['_token','cms_id','is_visible_on_footer','page_status']);

        //Validate that users can only choose home template once
        if(CMS::where('template',$request->template)->where('id',$request->cms_id)->count() > 0){

        }
        else{
            if(CMS::where('template','home')->count() > 0){
                if (CMS::where('template','home')->value('template') == $request->template){
                    $object = new stdClass();
                    $object->error = true;
                    return response()->json($object);
                }

            }
            else{
                $data['is_index_page'] = true;
            }
        }

        //Update Page
        CMS::where('id',$request->cms_id)->update($data);

        //insert page on footer if is_visible_on_footer is checked and the page is not on footer
        //Find the footer with max order
        $footer_max_menu_order =DB::table('cms_footers')->select(DB::raw("Max(menu_order) as max_menu_order"))->value('max_menu_order');
        if($request->is_visible_on_footer == true){
            if(CMSFooter::where('cms_id',$request->cms_id)->count() <= 0){
                CMSFooter::create([
                    'cms_id' => $request->cms_id,
                    'menu_order' =>  $footer_max_menu_order
                ]);
            }

        }
        else{
            CMSFooter::where('cms_id',$request->cms_id)->delete();
        }

        return response()->json(CMS::findOrFail($request->cms_id));
    }


    public function getPageDetail(Request $request){
        $cms = CMS::findOrFail($request->id);
        if($cms->footers->count() > 0){
            $cms->is_visible_on_footer = 1;
        }
        else{
            $cms->is_visible_on_footer = 0;
        }

        return response()->json($cms);
    }

    public function getMenus(){
        $tree= new MenuTreeBuilder();
        return response()->json($tree->buildMainMenuTree());
        //dd(\GuzzleHttp\json_encode($tree->buildTree()));
    }

    public function createMenu(Request $request){
    //dd($request->parentId);
        //Find the page with max order
        $max_menu_order =DB::table('cms')->select(DB::raw("Max(menu_order) as max_menu_order"))->value('max_menu_order');
        $data['menu_order'] = $max_menu_order + 1;//increment result and add to request
        if($request->parentId == null){
            $data['page_parent'] = 0;
        }
        else if($request->parentId == 0){
            $data['page_parent'] = 0;
        }
        else{
            $data['page_parent'] = $request->parentId;
        }

        $data['page_name'] = 'New Page';

        //Insert new Page
        $cms = CMS::create($data);

        $menu = new Menu($cms->id,$cms->page_name);

        return response()->json($menu);
    }

    public function updateMenu(Request $request){
        $cms = CMS::findOrFail($request->id);
        $cms->slug = null;
        $cms->page_name = $request->text;
        if($cms->save()){
            $object = new stdClass();
            $object->success = true;
            return response()->json($object);
        }

    }

    public function deleteMenu(Request $request){
        $cms = CMS::findOrFail($request->id);
        if($cms->delete()){
            $object = new stdClass();
            $object->success = true;
            return response()->json($object);
        }
    }

    public function dragAndDropMenu(Request $request){

        $cms = CMS::findOrFail($request->id);

        $cms->slug = null;
        /*If targetId=null, then cursor is on the root of the tree
        * Get the menu_order of the last child of the root node and inreament it by 1
        */
        if($request->targetId == null){
            $cms->page_parent = 0;
            $cms->menu_order = $cms->where('page_parent',0)->orderBy('menu_order','asc')->first()->menu_order + 1;
            if($cms->save()){
                $object = new stdClass();
                $object->success = true;
                return response()->json($object);
            }

        }
        else{
            $target_cms = CMS::findOrFail($request->targetId);
            switch ($request->point){
                case 'append' ://Append means the dragged node should be a child to the target noge
                    /*else targetId !=null and point is 'append', then cursor is on the child node of the root
                    * get the menu,if the point is append and the node has children, then find
                     * the last sibling of the  children and increament it by 1, else assign menu_order=1
                     * (because this node is the first child)
                    */
                    $cms->page_parent = $request->targetId;

                    if($target_cms->tree_has_children()){
                        $cms->menu_order = $target_cms->where('page_parent',$target_cms)->orderBy('menu_order','asc')->first()->menu_order + 1;
                        if($cms->save()){
                            $object = new stdClass();
                            $object->success = true;
                            return response()->json($object);
                        }
                    }
                    else{
                        $cms->menu_order = 1;
                        if($cms->save()){
                            $object = new stdClass();
                            $object->success = true;
                            return response()->json($object);
                        }
                    }
                case 'top' :
                    if($target_cms->tree_has_parent()){
                        $cms->page_parent = $target_cms->page_parent;
                        if($target_cms->menu_order-1<=0){
                            $target_cms->menu_order = 2;
                            $target_cms->save();

                            $cms->menu_order = 1;
                            if($cms->save()){
                                $object = new stdClass();
                                $object->success = true;
                                return response()->json($object);
                            }
                        }
                        else{
                            $cms->menu_order = $target_cms->menu_order-1;
                            if($cms->save()){
                                $object = new stdClass();
                                $object->success = true;
                                return response()->json($object);
                            }
                        }
                    }
                    else{
                        $cms->page_parent = 0;
                        if($target_cms->menu_order-1<=0){
                            $target_cms->menu_order = 2;
                            $target_cms->save();

                            $cms->menu_order = 1;
                            if($cms->save()){
                                $object = new stdClass();
                                $object->success = true;
                                return response()->json($object);
                            }
                        }
                        else{
                            $cms->menu_order = $target_cms->menu_order-1;
                            if($cms->save()){
                                $object = new stdClass();
                                $object->success = true;
                                return response()->json($object);
                            }
                        }

                    }

                case 'bottom' :
                    if($target_cms->tree_has_parent()){
                        $cms->page_parent = $target_cms->page_parent;
                        $cms->menu_order = $target_cms->menu_order+1;
                        if($cms->save()){
                            $object = new stdClass();
                            $object->success = true;
                            return response()->json($object);
                        }
                    }
                    else{
                        $cms->page_parent = 0;
                        $cms->menu_order = $target_cms->menu_order+1;
                        if($cms->save()){
                            $object = new stdClass();
                            $object->success = true;
                            return response()->json($object);
                        }
                    }


            }

        }



    }

    public function publishContent(Request $request)
    {

        $data = $request->except(['_token']);

        //Update Page
        CMS::where('id',$request->id)->update($data);

        $object = new stdClass();
        $object->success = true;
        return response()->json($object);
    }

    /*
     * Footer menu functions
     */

    public function getFooterMenus(){
        $tree= new MenuTreeBuilder();
        return response()->json($tree->buildFooterMenuTree());
    }

    public function createFooterMenu(Request $request){
        //dd($request->parentId);
        //Find the page with max order
        $max_menu_order =DB::table('cms')->select(DB::raw("Max(menu_order) as max_menu_order"))->value('max_menu_order');
        $data['menu_order'] = $max_menu_order + 1;//increment result and add to request
        if($request->parentId == null){
            $data['page_parent'] = 0;
        }
        else if($request->parentId == 0){
            $data['page_parent'] = 0;
        }
        else{
            $data['page_parent'] = $request->parentId;
        }

        $data['page_name'] = 'New Page';

        //Insert new Page
        $cms = CMS::create($data);

        //Add menu to footer
        //Find the footer with max order
        $footer_max_menu_order =DB::table('cms_footers')->select(DB::raw("Max(menu_order) as max_menu_order"))->value('max_menu_order');
        CMSFooter::create([
            'cms_id' => $cms->id,
            'menu_order' =>  $footer_max_menu_order
        ]);

        $menu = new Menu($cms->id,$cms->page_name);


        return response()->json($menu);
    }

    public function updateFooterMenu(Request $request){
        $cms = CMS::findOrFail($request->id);
        $cms->slug = null;
        $cms->page_name = $request->text;
        if($cms->save()){
            $object = new stdClass();
            $object->success = true;
            return response()->json($object);
        }

    }

    public function deleteFooterMenu(Request $request){
        $footer_menu = CMSFooter::where('cms_id',$request->id)->first();
        if( $footer_menu->delete()){
            $object = new stdClass();
            $object->success = true;
            return response()->json($object);
        }
    }

    public function dragAndDropFooterMenu(Request $request){

        $footer_menu = CMSFooter::where('cms_id',$request->id)->first();

        if($request->targetId == null){
            $footer_menu->menu_order = CMSFooter::orderBy('menu_order','desc')->first()->menu_order + 1;
            if($footer_menu->save()){
                $object = new stdClass();
                $object->success = true;
                return response()->json($object);
            }

        }
        else{
            $target_cms = CMSFooter::where('cms_id',$request->targetId)->first();
            switch ($request->point){
                case 'append' :
                    $object = new stdClass();
                    $object->success = false;
                    return response()->json($object);
                    break;
                case 'top' :
                        if($target_cms->menu_order-1<=0){
                            $target_cms->menu_order = 2;
                            $target_cms->save();

                            $footer_menu->menu_order = 1;
                            if($footer_menu->save()){
                                $object = new stdClass();
                                $object->success = true;
                                return response()->json($object);
                            }
                        }
                        else{
                            $footer_menu->menu_order = $target_cms->menu_order-1;
                            if($footer_menu->save()){
                                $object = new stdClass();
                                $object->success = true;
                                return response()->json($object);
                            }
                        }

                    break;

                case 'bottom' :
                        $footer_menu->menu_order = $target_cms->menu_order+1;
                        if($footer_menu->save()){
                            $object = new stdClass();
                            $object->success = true;
                            return response()->json($object);
                        }
                    break;

            }

        }


    }

    //Banner  actions
    public function showManageBanner()
    {
        $banners = Banner::all();
        return view('backend.content.manage-banner',compact('banners'));
    }

    public function showCreateBanner()
    {
        return view('backend.content.create-banner');

    }

    public function showEditBanner($id)
    {
        //Edit Banner
        $banner =  Banner::findOrFail($id);
        return view('backend.content.edit-banner',compact('banner'));

    }

    public function postBanner(Request $request)
    {


        $banner = new Banner();
        $this->validate($request,[
            'icon' => 'image|mimes:png',
            'title'=>'required|max:70',
            'content'=>'required|max:255',
            'bg_image'=>'required',
        ]);

        $data = $request->except(['status','icon','bg_image','_token']);

        //Admin cannot upload more than 5 Banner
       /* if(Banner::count() == 5){
            return redirect()->back()->with('message','Ooops! You cannot upload more than 5 Banners');
        }*/

       if($request->status == 'on'){
           $data['status'] = true;
       }
       else{
           $data['status'] = false;
       }


        // Add Banner Icon
        if($request->hasFile('icon')){
            $icon = $request->file('icon');
            $iconName = $banner->generateRandomName($icon);
            Storage::disk('public')->put(config('filesystems.storage.banner_icon_path'). $iconName, file_get_contents($icon));
            $data['icon'] = $iconName;

        }
        // Add Banner  Image
        if($request->hasFile('bg_image')){
            $image = $request->file('bg_image');
            $imageName = $banner->generateRandomName($image);
            Storage::disk('public')->put(config('filesystems.storage.banner_image_path'). $imageName, file_get_contents($image));
            $data['bg_image'] = $imageName;
        }


        Banner::create($data);

        Session::flash('message','Banner Successfully Uploaded');
        return redirect()->route('admin.show.manage-banner')->with('message','Banner Successfully loaded');

    }

    public function deleteBanner(Request $request){
        $cms = Banner::findOrFail($request->id);
        if($cms->delete()){
            $object = new stdClass();
            $object->success = true;
            Session::flash('message','Banner Successfully Deleted');
            return redirect()->back();
        }
    }

    public function updateStatus($id)
    {
        //Update Page
        $data = request()->except(['_token','status']);

        if(request()->status == 'on'){
            $data['status'] = true;
        }
        else{
            $data['status'] = false;
        }
        Banner::where('id',$id)->update($data);

        Session::flash('message', 'Banner Status successfully Updated!');
        return redirect()->back();

    }

    public function updateBanner(Request $request)
    {

        $data = $request->except(['_token']);


        //Update Page
        Banner::where('id',$request->id)->update($data);

        return redirect()->back()->with('message','Banner successfully Updated!');

    }

    public function updateBannerImage(Request $request)
    {

        $banner = new Banner();
        $data = $request->except(['_token','bg_image']);


        if($request->hasFile('bg_image')){
            $image = $request->file('bg_image');
            $imageName = $banner->generateRandomName($image);
            Storage::disk('public')->put(config('filesystems.storage.banner_image_path'). $imageName, file_get_contents($image));
            $data['bg_image'] = $imageName;
        }

        //Update Page
        Banner::where('id',$request->id)->update($data);

        return redirect()->back()->with('message','Banner Image successfully Updated!');

    }

    public function updateBannerIcon(Request $request)
    {

        $banner = new Banner();
        $data = $request->except(['_token','icon']);


        if($request->hasFile('icon')){
            $icon = $request->file('icon');
            $iconName = $banner->generateRandomName($icon);
            Storage::disk('public')->put(config('filesystems.storage.banner_icon_path'). $iconName, file_get_contents($icon));
            $data['icon'] = $iconName;

        }

        //Update Page
        Banner::where('id',$request->id)->update($data);

        return redirect()->back()->with('message','Banner Icon successfully Updated!');

    }

    //Gallery  actions
    public function showManageGallery()
    {
        $galleries = Gallery::all();
        $categories =  Category::all();
        return view('backend.content.manage-gallery',compact('galleries','categories'));
    }

    public function showCreateGallery()
    {
        $categories =  Category::all();
        return view('backend.content.create-gallery',compact('categories'));

    }

    public function showEditGallery($id)
    {
        //Edit Banner
        $gallery =  Gallery::findOrFail($id);
        return view('backend.content.edit-gallery',compact('gallery'));

    }

    public function postCategory(Request $request)
    {


        $category = new Category();
        $this->validate($request,[
            'name'=>'required|max:255',
        ]);

        $category->name = $request->name;
        $category->save();

        return redirect()->route('admin.show.manage-gallery')->with('message','Category Successfully loaded');

    }

    public function updateCategory(Request $request)
    {

        $category = Category::findOrFail($request->id);

        $category->slug = null;


        //Update Page
        $category->update(['name' => $request->name]);

        return redirect()->back()->with('message','Banner successfully Updated!');

    }

    public function deleteCategory(Request $request){
        $cms = Category::findOrFail($request->id);
        if($cms->delete()){
            Session::flash('message','Banner Successfully Deleted');
            return redirect()->back();
        }
    }

    public function postGallery(Request $request)
    {


        $gallery = new Gallery();
        $this->validate($request,[
            'title'=>'required|max:25',
            'content'=>'required|max:255',
            'image'=>'required',
            'category_id' => 'required',
        
        ]);

        $data = $request->except(['status','image','_token']);



        if($request->status == 'on'){
            $data['status'] = true;
        }
        else{
            $data['status'] = false;
        }


        // Add   Image
        if($request->hasFile('image')){
            $image = $request->file('image');
            $imageName = $gallery->generateRandomName($image);
            Storage::disk('public')->put(config('filesystems.storage.gallery_image_path'). $imageName, file_get_contents($image));
            $data['image'] = $imageName;
        }


        Gallery::create($data);

        Session::flash('message','Gallery Successfully Uploaded');
        return redirect()->route('admin.show.manage-gallery')->with('message','Gallery Successfully loaded');

    }

    public function deleteGallery(Request $request){
        $cms = Gallery::findOrFail($request->id);
        if($cms->delete()){

            Session::flash('message','Gallery Successfully Deleted');
            return redirect()->back();
        }
    }

    public function updateGalleryStatus($id)
    {
        //Update Page
        $data = request()->except(['_token','status']);

        if(request()->status == 'on'){
            $data['status'] = true;
        }
        else{
            $data['status'] = false;
        }
        Gallery::where('id',$id)->update($data);

        Session::flash('message', 'Gallery Status successfully Updated!');
        return redirect()->back();

    }

    public function updateGallery(Request $request)
    {

        $data = $request->except(['_token']);


        //Update Page
        Gallery::where('id',$request->id)->update($data);

        return redirect()->back()->with('message','Gallery successfully Updated!');

    }

    public function updateGalleryImage(Request $request)
    {

        $gallery = new Gallery();
        $data = $request->except(['_token','image']);


        if($request->hasFile('image')){
            $image = $request->file('image');
            $imageName = $gallery->generateRandomName($image);
            Storage::disk('public')->put(config('filesystems.storage.gallery_image_path'). $imageName, file_get_contents($image));
            $data['image'] = $imageName;
        }

        //Update Page
        Gallery::where('id',$request->id)->update($data);

        return redirect()->back()->with('message','Gallery Image successfully Updated!');

    }


    public function test()
    {
        return view('backend.content.test');
    }
}
