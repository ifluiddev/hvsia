<?php

namespace App\Http\Controllers\Backend;

use App\Location;
use App\User;
use App\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('backend.user.index',compact('users'));
    }

    public function create()
    {
        $locations = Location::all();
        return view('backend.user.create',compact('locations'));

    }

    public function edit($id)
    {
        //Edit User
        $user =  User::findOrFail($id);
        $locations = Location::all();
        return view('backend.user.edit',compact('user','locations'));

    }

    public function store(Request $request)
    {


        $user =  new User();
        $this->validate($request, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'location_id' => 'required'

        ]);

        $user->first_name =  $request->input('first_name');
        $user->last_name =  $request->input('last_name');
        $user->email = $request->input('email');
        $user->phone_number = $request->input('phone_number');
        $user->location_id = $request->input('location_id');
        $user->password = bcrypt($request->input('password'));
        $user->email_verified_at = Carbon::now()->toDateTimeString();

        $user->save();


        $data = array('name' => $user->getFullname(),
            'email' => $request->input('email'),
            'password' => $request->input('password')
        );

        Mail::send('emails.user_details', $data, function ($message) {
            $message->to(Input::get('email'), 'HVSIA')
                ->subject('User Details');
            $message->from(env('MAIL_FROM_ADDRESS', 'admin@hvsia.com'), 'HVSIA');
        });


        Session::flash('message', 'User successfully Created!');
        return redirect()->route('admin.users.index');

    }

    public function destroy(Request $request){
        $user = User::findOrFail($request->id);
        if($user->delete()){
            Session::flash('message','User Successfully Deleted');
            return redirect()->back();
        }
    }


    public function update(Request $request,$id)
    {

        $data = $request->except(['_token','_method']);


        //Update Page
        User::where('id',$id)->update($data);

        return redirect()->back()->with('message','User successfully Updated!');

    }
}
