<?php

namespace App\Http\Controllers\Backend;

use App\Meeting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class MeetingMinuteUploadController extends Controller
{
    public function update(Request $request,$id){

        $meeting = Meeting::findOrFail($id);
        if ($request->hasFile('minute')) {
            $file = $request->file('minute');
            $fileName = $meeting->generateRandomName($meeting->title, $file);
            Storage::disk('public')->put(config('filesystems.storage.meeting_minute_path') . $fileName, file_get_contents($file));

            $meeting->minute = $fileName;
            $meeting->save();

        }

        return redirect()->back();
    }
}
