<?php

namespace App\Http\Controllers\Backend;

use App\CompetencyArea;
use App\FocusArea;
use App\Location;
use App\Meeting;
use App\Test;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class MarkTestCompletedController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function update(Request $request,$id){
        //dd($id);
        $test = Test::findOrFail($id);
        $test->completed = true;
        $test->completed_date = Carbon::now();
        $test->save();

        return response()->json(['url' => route('admin.tests.index')]);
    }


}
