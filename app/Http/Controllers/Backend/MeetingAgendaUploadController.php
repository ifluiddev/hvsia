<?php

namespace App\Http\Controllers\Backend;

use App\Meeting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class MeetingAgendaUploadController extends Controller
{
    public function update(Request $request,$id){

        $meeting = Meeting::findOrFail($id);
        if ($request->hasFile('agenda')) {
            $file = $request->file('agenda');
            $fileName = $meeting->generateRandomName($meeting->title, $file);
            Storage::disk('public')->put(config('filesystems.storage.meeting_agenda_path') . $fileName, file_get_contents($file));

            $meeting->agenda = $fileName;
            $meeting->save();

        }

        return redirect()->back();
    }
}
