<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MatrixController extends Controller
{
    public function  ShowManageMatrix(){
        return view('backend.matrix.ManageMatrix');
    }
    public function  ShowCreateTest(){
        return view('backend.matrix.CreateTest');
    }
    public function  ShowFocusArea(){
        return view('backend.matrix.FocusArea');
    }
}
