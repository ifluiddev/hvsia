<?php

namespace App\Http\Controllers\Backend;

use App\Meeting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class MeetingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function  index(){
        $meeting = new Meeting();
        $upcoming_meetings = $meeting->getUpcomingMeetings();
        $past_meetings = $meeting->getPastMeetings();
        return view('backend.meeting.index',compact('upcoming_meetings','past_meetings'));
    }
    public function  create(){

        return view('backend.meeting.create');
    }

    public function  store(Request $request)
    {
        //dd($request);
        $this->validate($request, [
            'title' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'agenda' => 'required',
            'description' => 'required',
            'venue' => 'required',
        ]);


        $meeting = new Meeting();
        $data = $request->except(['_token', 'agenda','date']);

        // Add Banner Icon
        if ($request->hasFile('agenda')) {
            $file = $request->file('agenda');
            $fileName = $meeting->generateRandomName($request->title, $file);
            Storage::disk('public')->put(config('filesystems.storage.meeting_agenda_path') . $fileName, file_get_contents($file));
            $data['agenda'] = $fileName;

        }

        $data['description'] = nl2br($request->description);
        $data['start_time'] = $request->start_date . ' ' . $request->start_time;
        $data['end_time'] = $request->end_date . ' ' . $request->start_time;
        $meeting = Meeting::create($data);

        return redirect()->route('admin.meetings.index')->with('message', 'You have successfully created a meeting');
    }

    public function  show($id){
        $meeting = Meeting::findOrFail($id);
        return view('backend.meeting.show',compact('meeting'));
    }

    public function  edit($id){
        $meeting = Meeting::findOrFail($id);
        return view('backend.meeting.edit',compact('meeting'));
    }

    public function  update(Request $request,$id)
    {
        //dd($request);

        $this->validate($request, [
            'title' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'description' => 'required',
            'venue' => 'required',
        ]);


        $data = $request->except(['_token','_method','start_date','end_date']);

        $data['description'] = nl2br($request->description);
        $data['start_time'] = $request->start_date . ' ' . $request->start_time;
        $data['end_time'] = $request->end_date . ' ' . $request->end_time;
        Meeting::where('id',$id)->update($data);

        return redirect()->route('admin.meetings.index')->with('message', 'Update successful');
    }

    public function destroy(Request $request){
        $meeting = Meeting::findOrFail($request->id);
        if($meeting->delete()){
            Session::flash('message','Admin Successfully Deleted');
            return redirect()->back();
        }
    }

}
