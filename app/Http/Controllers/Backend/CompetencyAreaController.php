<?php

namespace App\Http\Controllers\Backend;

use App\CompetencyArea;
use App\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\FocusArea;

class CompetencyAreaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $competency_areas = CompetencyArea::all();
        return view('backend.competency-area.index',compact('competency_areas'));
    }

    public function create()
    {
        return view('backend.competency-area.create');

    }

    public function edit($id)
    {
        //Edit CompetencyArea
        $competency_area =  CompetencyArea::findOrFail($id);
        return view('backend.competency-area.edit',compact('competency_area'));

    }

    public function store(Request $request)
    {

        $competency_area =  new CompetencyArea();
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);

        $competency_area->name =  $request->input('name');

        $competency_area->save();


        return redirect()->route('admin.competency-areas.index');

    }

    public function show(Request $request,$id)
    {
        $competency_area = CompetencyArea::findOrFail($id);
        $focus_area = FocusArea::findOrFail($request->focus_area_id);
        return view('backend.competency-area.show',compact('competency_area','focus_area'));
    }

    public function destroy($id){
        $competency_area = CompetencyArea::findOrFail($id);
        if($competency_area->delete()){
            Session::flash('message','CompetencyArea Successfully Deleted');
            return redirect()->back();
        }
    }


    public function update(Request $request,$id)
    {

        $data = $request->except(['_token','_method']);


        //Update Page
        CompetencyArea::where('id',$id)->update($data);

        return redirect()->route('admin.competency-areas.index')->with('message','CompetencyArea successfully Updated!');

    }
}
