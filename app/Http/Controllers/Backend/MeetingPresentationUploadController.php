<?php

namespace App\Http\Controllers\Backend;

use App\Meeting;
use App\MeetingPresentation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class MeetingPresentationUploadController extends Controller
{
    public function update(Request $request,$id){

        $meeting = Meeting::findOrFail($id);
        if ($request->hasFile('presentation')) {
            $file = $request->file('presentation');
            $fileName = $meeting->generateRandomName($meeting->title, $file);
            Storage::disk('public')->put(config('filesystems.storage.meeting_presentation_path') . $fileName, file_get_contents($file));

            MeetingPresentation::create(['meeting_id' => $id,'file_name' => $fileName,'title' => $request->title,'author' => $request->author]);

        }

        return redirect()->back();
    }

    public function destroy(Request $request,$id){
        $meeting_presentation = MeetingPresentation::findOrFail($id);
        if($meeting_presentation->delete()){
            return response()->json(['success' => true]);
        }
    }
}
