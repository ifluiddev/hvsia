<?php

namespace App\Http\Controllers\Backend;

use App\Location;
use App\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class LocationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index()
    {
        $locations = Location::all();
        return view('backend.location.index',compact('locations'));
    }

    public function create()
    {
        return view('backend.location.create');

    }

    public function edit($id)
    {
        //Edit Location
        $location =  Location::findOrFail($id);
        return view('backend.location.edit',compact('location'));

    }

    public function store(Request $request)
    {

        $location =  new Location();
        $this->validate($request, [
            'country' => 'required|max:255',
            'organization' => 'required|max:255',
        ]);

        $location->country =  $request->input('country');
        $location->organization =  $request->input('organization');


        $location->save();


        return redirect()->route('admin.locations.index');

    }

    public function destroy($id){
        $location = Location::findOrFail($id);
        if($location->delete()){
            Session::flash('message','Location Successfully Deleted');
            return redirect()->back();
        }
    }


    public function update(Request $request,$id)
    {

        $data = $request->except(['_token','_method']);


        //Update Page
        Location::where('id',$id)->update($data);

        return redirect()->route('admin.locations.index')->with('message','Location successfully Updated!');

    }
}
