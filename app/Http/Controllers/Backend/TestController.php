<?php

namespace App\Http\Controllers\Backend;

use App\CompetencyArea;
use App\FocusArea;
use App\Location;
use App\Meeting;
use App\Test;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class TestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index() {
        $test = new Test();
        return view('backend.test.index',compact('test'));
    }

    public function create() {
        $competency_areas = CompetencyArea::all();
        $focus_areas = FocusArea::all();
        $locations = Location::all();
        return view('backend.test.create',compact('competency_areas','focus_areas','locations'));
    }

    public function edit($id) {

        $test = Test::findOrFail($id);
        $competency_areas = CompetencyArea::all();
        $test_competency_areas = $test->competency_areas()->pluck('competency_area_id')->toArray();
        $focus_areas = FocusArea::all();
        $locations = Location::all();
        //dd($locations);
        return view('backend.test.edit',compact('competency_areas','focus_areas','locations','test','test_competency_areas'));
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'location_id' => 'required',
            'focus_area_id' => 'required',
            'competency_area_id' => 'required',
        ]);

        $data = $request->except(['_token','competency_area_id','start_date','completed_date']);

        if($request->start_date != ''){
            $data['start_date'] = $request->start_date;
            $data['completed_date'] = $request->completed_date;
            $data['completed'] = true;

        }
        else{
            $data['start_date'] = Carbon::now();
        }

        $test = Test::create($data);

        foreach($request->competency_area_id as $id){
            $test->competency_areas()->attach($id);
        }

        return redirect()->route('admin.tests.show',['id' => $test->id]);
    }

    public function update(Request $request,$id){
        //dd($id);

        $this->validate($request, [
            'location_id' => 'required',
            'focus_area_id' => 'required',
            'competency_area_id' => 'required',
        ]);

        $test = Test::findOrFail($id);
        $test->location_id = $request->location_id;
        $test->focus_area_id = $request->focus_area_id;
        $test->start_date = $request->start_date;

        //delete all competency areas assigned to this test
        $test->competency_areas()->detach();
        //Add new competency areas
        foreach($request->competency_area_id as $id){
            $test->competency_areas()->attach($id);
        }

        //Save
        $test->save();

        return redirect()->route('admin.tests.index');
    }

    public function show($id) {
        $test = Test::findOrFail($id);
        return view('backend.test.show',compact('test'));
    }

    public function destroy($id){
        $test = Test::findOrFail($id);

        //delete all competency areas assigned to this test
        $test->competency_areas()->detach();

        $test->delete();

        return response()->json(['url' => route('admin.tests.index')]);
    }


}
