<?php

namespace App\Http\Controllers\Backend;

use App\CompetencyArea;
use App\FocusArea;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class FocusAreaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function  index(){
        $focus_areas = FocusArea::all();
        return view('backend.focus-area.index',compact('focus_areas'));
    }
    public function  create(){

        $competency_areas = CompetencyArea::all();
        return view('backend.focus-area.create',compact('competency_areas'));
    }

    public function  store(Request $request)
    {
        //dd($request);
        $this->validate($request, [
            'name' => 'required',
            'image' => 'required',
        ]);

        $focus_area = new FocusArea();
        $data = $request->except(['_token', 'image','competency_area_id']);

        // Add Image Icon
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = $focus_area->generateRandomName($request->name, $file);
            Storage::disk('public')->put(config('filesystems.storage.focus_area_image_path') . $fileName, file_get_contents($file));
            $data['image'] = $fileName;

        }

        $focus_area = FocusArea::create($data);

        foreach($request->competency_area_id as $id){
            $focus_area->competency_areas()->attach($id);
        }

        return redirect()->route('admin.focus-areas.index')->with('message', 'You have successfully created a focus area');
    }

    public function  show($id){
        $focus_area = FocusArea::findOrFail($id);
        return view('backend.focus-area.show',compact('focus_area'));
    }

    public function  edit($id){
        $focus_area = FocusArea::findOrFail($id);
        $competency_areas = CompetencyArea::all();
        $focus_competency_areas = $focus_area->competency_areas()->pluck('name')->toArray();
        return view('backend.focus-area.edit',compact('focus_area','competency_areas','focus_competency_areas'));
    }

    public function  update(Request $request,$id)
    {


        $focus_area = FocusArea::where('id',$id)->first();
        $focus_area->name = $request->name;
        $focus_area->save();

        $focus_area->competency_areas()->detach();

        foreach($request->competency_area_id as $id){
            $focus_area->competency_areas()->attach($id);
        }

        return redirect()->route('admin.focus-areas.index')->with('message', 'Update successful');
    }

    public function destroy($id){
        $focus_area = FocusArea::findOrFail($id);
        if($focus_area->delete()){
            Session::flash('message','Admin Successfully Deleted');
            return redirect()->back();
        }
    }

}
