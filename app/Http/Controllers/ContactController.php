<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Ticket;
use Illuminate\Http\Request;
use App\Events\SendContactMessage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;


class ContactController extends Controller
{


    public function store(Request $request)
    {
        $request->validate([
            
            'name' => 'required',
            'email' => 'required:email',
            'message' => 'required',
            'phone_number' => 'required',
            'country_email' => 'required'
        ]);

        // Send message
        event(new SendContactMessage($request->name,$request->email,$request->message,$request->phone_number,$request->country_email));


        return redirect()->back()->with('message','Message sent. Thanks for leaving a message');
    }

}
