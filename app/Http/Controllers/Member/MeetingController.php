<?php

namespace App\Http\Controllers\Member;

use App\Meeting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class MeetingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function  index(){
        $meeting = new Meeting();
        $upcoming_meetings = $meeting->getUpcomingMeetings();
        $past_meetings = $meeting->getPastMeetings();
        return view('member.meeting.index',compact('upcoming_meetings','past_meetings'));
    }

    public function  show($id){
        $meeting = Meeting::findOrFail($id);
        return view('member.meeting.show',compact('meeting'));
    }


}
