<?php

namespace App\Http\Controllers\Member;

use App\Test;
use App\TestPublication;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class TestPublicationController extends Controller
{

    public function index(Request $request){
        $publications = TestPublication::where('competency_area_id',$request->competency_area_id)->where('test_id',$request->test_id)->get();
        return $publications;

    }

    public function store(Request $request){

        $test = Test::findOrFail($request->test_id);
        if ($request->hasFile('publication')) {
            $file = $request->file('publication');
            $fileName = $test->generateRandomName($request->title, $file);
            Storage::disk('public')->put(config('filesystems.storage.test_publication_path') . $fileName, file_get_contents($file));

            TestPublication::create(['test_id' => $request->test_id,'file_name' => $fileName,'title' => $request->title,'author' => $request->author,'competency_area_id' => $request->competency_area_id]);

        }

        return response()->json(['link' => route('member.tests.show',['id' => $request->test_id])]);
    }

    public function destroy($id){
        $publication = TestPublication::findOrFail($id);

        $publication->delete();

        return response()->json(['success' => true]);
    }
}
