<?php

namespace App\Http\Controllers\Member;

use App\FocusArea;
use App\Meeting;
use App\Test;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class TestController extends Controller
{

    public function index() {
        return view('member.test.index');
    }

    public function store(Request $request)
    {
        $data = $request->except(['_token','competency_area_id']);

        $data['user_id'] = auth()->id();
        $data['location_id'] = auth()->user()->location->id;
        $data['start_date'] = Carbon::now();
        $test = Test::create($data);

        foreach($request->competency_area_id as $id){
            $test->competency_areas()->attach($id);
        }

        return redirect()->back()->with('message', 'You have started a test');
    }

    public function update(Request $request,$id){
        //dd($id);
        $test = Test::findOrFail($id);
        $test->completed = true;
        $test->completed_date = Carbon::now();
        $test->save();

        return response()->json(['url' => route('member.show.index')]);
    }

    public function show($id) {
        $test = Test::findOrFail($id);
        return view('member.test.show',compact('test'));
    }

    public function destroy($id){
        $test = Test::findOrFail($id);

        //delete all competency areas assigned to this test
        $test->competency_areas()->detach();

        $test->delete();

        return response()->json(['url' => route('member.show.index')]);
    }


}
