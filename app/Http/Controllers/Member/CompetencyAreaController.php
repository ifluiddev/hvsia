<?php

namespace App\Http\Controllers\Member;

use App\CompetencyArea;
use App\FocusArea;
use App\Meeting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class CompetencyAreaController extends Controller
{

    public function show(Request $request,$id)
    {
        $competency_area = CompetencyArea::findOrFail($id);
        $focus_area = FocusArea::findOrFail($request->focus_area_id);
        return view('member.competency-area.show',compact('competency_area','focus_area'));
    }


}
