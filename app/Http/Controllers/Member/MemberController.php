<?php

namespace App\Http\Controllers\Member;

use App\User;
use App\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class MemberController extends Controller
{



    public function showEditUser($id)
    {
        //Edit User
        $user =  User::findOrFail($id);
        return view('backend.user.edit-user',compact('user'));

    }

    public function updateUser(Request $request)
    {

        $data = $request->except(['_token']);


        //Update Page
        User::where('id',$request->id)->update($data);

        return redirect()->back()->with('message','User successfully Updated!');

    }
}
