<?php

namespace App\Http\Controllers;

use App\Category;
use App\Gallery;
use Illuminate\Http\Request;

class FrontEndPagesController extends Controller
{
    public function  index(){
        return view('frontend.index');
    }
    public function  constitution(){
        return view('frontend.constitution');
    }
    public function  locations(){
        return view('frontend.locations');
    }
    public function  contactus(){
        return view('frontend.contactus');
    }

    public function  meeting(){
        return view('frontend.meeting');
    }

    public function  gallery(){
        $galleries = Gallery::where('status',true)->get();
        $categories = Category::all();
        return view('frontend.gallery',compact('galleries','categories'));
    }
}
