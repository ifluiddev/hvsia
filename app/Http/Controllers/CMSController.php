<?php

namespace App\Http\Controllers;

use App\Banner;
use App\BannerWidget;
use App\Challenge;
use App\CMS;
use App\Partner;
use Illuminate\Http\Request;

class CMSController extends Controller
{
    public function index()
    {
        $banners = Banner::where('status',true)->get();

        $cms = CMS::where('is_index_page',true)->first();
        if($cms){
            return view('frontend.cms',['cms' => $cms,'banners'=>$banners]);
        }

    }

    public function cms($pageSlug)
    {
        $cms = CMS::where('slug',$pageSlug)->first();
        return view('frontend.cms',['cms' => $cms]);
    }
}
