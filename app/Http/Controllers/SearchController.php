<?php

namespace App\Http\Controllers;

use App\TestPublication;
use Illuminate\Http\Request;

class SearchController extends Controller
{

    public function  search(Request $request){
        $results = TestPublication::search($request->search)->paginate(15);

        return view('frontend.search',compact('results'));
    }
}
