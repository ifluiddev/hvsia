<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        '/admin/ajax/page/all','/admin/ajax/page/create','/admin/ajax/page/update',
        '/admin/ajax/page/delete','/admin/ajax/page/dnd','/admin/ajax/page/footer/all','/admin/ajax/page/footer/dnd',
    ];
}
