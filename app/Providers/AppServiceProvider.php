<?php

namespace App\Providers;

use App\CMS;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::if('can', function ($permission) {
            return auth()->user()->m_can($permission);
        });

        Blade::if('hasRole', function ($role) {
            return auth()->user()->hasRole($role);
        });

        Blade::if('hasAnyRole', function ($roles) {
            return auth()->user()->hasAnyRole($roles);
        });

        Blade::if('template', function ($template_name,$page_slug) {
            if(CMS::where('template',$template_name)->where('slug',$page_slug)->count() > 0)
            {
                return true;
            }
            return false;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
