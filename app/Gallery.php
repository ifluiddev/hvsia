<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable = [
        'title', 'content','category_id','image','status','external_link'
    ];

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function generateRandomName($value) {
        $filename = 'image'.str_random(5).date('m-d-Y_hia') . '.' . $value->getClientOriginalExtension();
        return $filename;
    }
}
