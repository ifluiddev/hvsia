<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeetingPresentation extends Model
{
    protected $fillable = [
        'meeting_id','file_name','title','author'
    ];

    public function meeting(){
        return $this->belongsTo('App\Meeting','meeting_id');
    }
}
