<?php

namespace App\Listeners;

use App\Events\SendContactMessage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\ContactMessage;
use Illuminate\Support\Facades\Mail;

class SendContactMessageListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendContactMessage  $event
     * @return void
     */
    public function handle(SendContactMessage $event)
    {
        Mail::to($event->country_email)->send(new ContactMessage($event->name,$event->email,$event->message,$event->phone_number,$event->country_email,'admin'));
        Mail::to($event->email)->send(new ContactMessage($event->name,$event->email,$event->message,$event->phone_number,$event->country_email,'user'));

    }
}
