<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable {

    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guard='admin';
    protected $fillable = [
        'name', 'email', 'password', 'job_title','dept',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //Relationship starts here

    public function role(){
        return $this->belongsTo('App\Role');
    }


    //These functions checks either a user has a certain role or not
    public function hasAnyRole($roles) {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                $d_roles = Role::all();
                if(Role::whereIn($role, $d_roles)){
                    return true;
                }
                return false;
            }

        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }

    public function hasRole($role) {

        if (Role::where('role_name', $role)->first()) {
            return true;
        }
        return false;
    }

}
