<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = [
        'country','organization',
    ];

    public function users(){
        return $this->hasMany('App\Location','location_id');
    }

    public function tests(){
        return $this->hasMany('App\Test','location_id');
    }

    public function getLocationAttribute(){
        return $this->country . ':' . $this->organization;
    }

    public function completedTests(){
        return $this->tests()->where('completed',true)->get();
    }

    public function inProgressTests(){
        return $this->tests()->where('completed',false)->get();
    }
}
