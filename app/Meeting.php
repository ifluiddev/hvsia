<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Meeting extends Model
{
    protected $fillable = [
        'title','description','start_time','end_time','agenda','minute','venue'
    ];

    public function presentations() {
        return $this->hasMany('App\MeetingPresentation', 'meeting_id');
    }

    public function generateRandomName($title,$value) {
        $filename = $title.date('m-d-Y_hia') . '.' . $value->getClientOriginalExtension();
        return $filename;
    }

    public function getUpcomingMeetings() {
       // dd(DB::select('SELECT TIME(start_time) as time_start, TIME(NOW()) as time_current FROM meetings WHERE start_time >= NOW() OR (TIMESTAMPDIFF(DAY, start_time,NOW()) = 0  AND TIME(start_time) > CURRENT_TIME())'));
        return $this->whereRaw('end_time >= NOW()')->get();
    }

    public function getPastMeetings() {
        /*$now = Carbon::now();
        return $this->where('end_time','<',$now->format('Y-m-d H:i:s'))->get();*/
        return $this->whereRaw('end_time < NOW()')->get();
    }

    public function getDateAttribute(){
        $carbon = new Carbon($this->start_time);
        return $carbon->format('Y/m/d');
    }

    public function getEndDateAttribute(){
        $carbon = new Carbon($this->end_time);
        return $carbon->format('Y/m/d');
    }

    public function getTimeStartAttribute(){
        $carbon = new Carbon($this->start_time);
        return $carbon->format('H:i:s');
    }

    public function getTimeEndAttribute(){
        $carbon = new Carbon($this->end_time);
        return $carbon->format('H:i:s');
    }


}
