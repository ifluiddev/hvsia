<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompetencyArea extends Model
{
    protected $fillable = [
        'name',
    ];

    public function focus_areas(){
        return $this->belongsToMany('App\FocusArea','focus_competencies','competency_area_id','focus_area_id');
    }

    public function tests(){
        return $this->belongsToMany('App\Test','test_competencies','competency_area_id','test_id');
    }

    public function latestTestInProgress(){
        return $this->tests()->where('completed',false)->latest()->first();
    }

    public function latestTestCompleted(){
        return $this->tests()->where('completed',true)->latest()->first();
    }

    public function testsInprogress(){
        return $this->tests()->where('completed',false)->get();
    }

    public function testsCompleted(){
        return $this->tests()->where('completed',true)->get();
    }
}
