<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Sluggable;

    protected $fillable = [
        'name'
    ];

    protected $table = 'categories';

    public function sluggable() {
        return [
            'slug' => [
                'source' => ['name']
            ]
        ];
    }

    public function gallery(){
        return $this->hasOne('App\Gallery', 'category_id');
    }
}
