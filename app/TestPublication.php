<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class TestPublication extends Model
{
    use Searchable;
    protected $fillable = [
        'test_id','file_name','title','author','competency_area_id'
    ];

    public function test(){
        return $this->belongsTo('App\Test','test_id');
    }
}
