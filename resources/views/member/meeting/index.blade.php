@extends('layouts.member.main')

@section('css')
    {{-- @include('layouts.admin_css.faq_css')--}}

@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Meetings</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Members</a></li>
                <li class="breadcrumb-item active">Meetings</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <!-- begin panel -->
                <div class="card" data-sortable-id="tree-view-4">
                    <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="card">
                                <div class="card-header">
                                    <b> Upcoming Meetings</b>
                                </div>
                                <div class="card-body" style="padding: 0px">
                                    <div class="table-responsive">
                                        @if($upcoming_meetings->count() > 0)
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Date</th>
                                                <th>Venue</th>
                                                <th class="text-nowrap" width="3%"></th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($upcoming_meetings as $row)
                                                <tr>
                                                    <td>{{str_limit($row->title,10)}}</td>
                                                    <td>{{$row->date}}</td>
                                                    <td>{{$row->venue}}</td>
                                                    <td class="text-nowrap">
                                                        <a href="{{route('member.meetings.show',['id' => $row->id])}}" data-toggle="tooltip" data-original-title="Details"> <i class="fa fa-eye text-inverse m-r-10"></i> </a>
                                                    </td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                        @else
                                        <blockquote>No Upcoming Meetings Scheduled yet.</blockquote>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="card">
                                <div class="card-header">
                                    <b>Concluded Meetings</b>
                                </div>
                                <div class="card-body" style="margin-top: 20px">
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Date</th>
                                            <th>Venue</th>
                                            <th width="6%"></th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($past_meetings as $row)
                                            <tr>
                                                <td>{{str_limit($row->title,10)}}</td>
                                                <td>{{$row->date}}</td>
                                                <td>{{$row->venue}}</td>
                                                <td class="text-nowrap">
                                                    <a href="{{route('member.meetings.show',['id' => $row->id])}}" data-toggle="tooltip" data-original-title="Details"> <i class="fa fa-eye text-inverse m-r-10"></i> </a>
                                                </td>
                                            </tr>
                                        @endforeach


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    </div>



                </div>
                <!-- end panel -->
            </div>
            <!-- end col-6 -->
        </div>
        <!-- end row -->
    </div>

@endsection

@section('script')

    <script src="{{asset('Back-Assets/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script>

        $('#example23').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    </script>

@endsection

