@extends('layouts.member.main')

@section('css')

@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Meeting Overview</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('member.meetings.index')}}">Meetings</a></li>
                <li class="breadcrumb-item active">Meeting Overview</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-4">
                <!-- begin panel -->
                <div class="card" data-sortable-id="tree-view-4">
                    <div class="card-body">
                        <h2>Meeting Details</h2>
                        <hr />
                        <div class="row">
                            <div class="col-4">
                                <b>Title:</b>
                            </div>
                            <div class="col-8">
                                {{$meeting->title}}
                            </div>
                            <div class="col-4">
                                <b> Date:</b>
                            </div>
                            <div class="col-8">
                                {{$meeting->date}}
                            </div>
                            <div class="col-4">
                                <b>Venue:</b>
                            </div>
                            <div class="col-8">
                                <b> {{$meeting->venue}}</b>
                            </div>
                        </div>
                    </div>



                </div>
                <!-- end panel -->
            </div>
            <!-- end col-4 -->
            <div class="col-md-8">
                <!-- begin panel -->
                <div class="card" data-sortable-id="tree-view-4">
                    <div class="card-body">
                        <ul class="nav nav-tabs customtab" role="tablist">
                            <li class="nav-item"> <a class="nav-link show active" data-toggle="tab" href="#home2" role="tab" aria-selected="true"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Description</span></a> </li>
                            <li class="nav-item"> <a class="nav-link show" data-toggle="tab" href="#profile2" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Agenda</span></a> </li>
                            <li class="nav-item"> <a class="nav-link show" data-toggle="tab" href="#messages2" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Minutes</span></a> </li>
                            <li class="nav-item"> <a class="nav-link show" data-toggle="tab" href="#messages4" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Presentation</span></a> </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane show active" id="home2" role="tabpanel">
                                <div class="p-20">
                                    {!! $meeting->description !!}
                                </div>
                            </div>
                            <div class="tab-pane p-20 show" id="profile2" role="tabpanel">
                                <table class="table table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Date Uploaded</th>
                                        <th width="6%">Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>{{$meeting->created_at}}</td>
                                        <td><a target="_blank" href="{{'/uploads'.config('filesystems.storage.meeting_agenda_path').$meeting->agenda}}">
                                                <i class="fa fa-download fa-2x" aria-hidden="true"></i>
                                            </a></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane p-20 show" id="messages2" role="tabpanel">
                                @if($meeting->minute)
                                    <table class="table table-hover table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Date Uploaded</th>
                                            <th width="6%">Action</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{$meeting->updated_at}}</td>
                                            <td>
                                                <a target="_blank" href="{{'/uploads'.config('filesystems.storage.meeting_minute_path').$meeting->minute}}">
                                                    <i class="fa fa-download fa-2x" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                @else
                                    <blockquote>No Minutes have been uploaded by the system administrator, check back later</blockquote>
                                @endif
                            </div>
                            <div class="tab-pane p-20 show" id="messages4" role="tabpanel">
                                @if($meeting->presentations->count() > 0)

                                    <table class="table table-hover table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Author</th>
                                            <th>Date Uploaded</th>
                                            <th width="6%">Action</th>

                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($meeting->presentations as $row)
                                            <tr>
                                                <td>{{$row->title}}</td>
                                                <td>{{$row->author}}</td>
                                                <td>{{$row->created_at}}</td>
                                                <td>
                                                    <a target="_blank" href="{{'/uploads'.config('filesystems.storage.meeting_presentation_path').$row->file_name}}">
                                                        <i class="fa fa-download fa-2x" aria-hidden="true"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>

                                @else
                                    <blockquote>No Presentations have been uploaded by the system administrator, check back later</blockquote>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
    </div>

@endsection

@section('script')

@endsection

