@extends('layouts.member.main')

@section('css')

@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Your Tests</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Matrix</a></li>
                <li class="breadcrumb-item active">Your Tests</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- begin row -->
        <div class="row">
            <input type="hidden" id="test-id" value="" >
            <!-- begin col-12 -->
            <div class="col-md-12">
                <!-- begin panel -->
                <div class="card" data-sortable-id="tree-view-4">
                    <div class="card-body">
                        <div class="vtabs">
                            <ul class="nav nav-tabs tabs-vertical" role="tablist">
                                <li class="nav-item"> <a class="nav-link active show" data-toggle="tab" href="#home4" role="tab" aria-selected="true"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Completed</span> </a> </li>
                                <li class="nav-item"> <a class="nav-link show" data-toggle="tab" href="#profile4" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">In Progress</span></a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active show" id="home4" role="tabpanel" style="padding-top: 20px">
                                    <table id="CompletedTableView" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>Test ID</th>
                                                <th>Focus Area</th>
                                                <th>Competencies</th>
                                                <th>Date Started</th>
                                                <th>Date Completed</th>
                                                <th>View</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @foreach(auth()->user()->location->completedTests() as $row)
                                                <tr>
                                                    <td>#{{$row->id}}</td>
                                                    <td>{{$row->focus_area->name}}</td>
                                                    <td>
                                                        @if($row->competency_areas()->count() > 1)
                                                            <label class="label label-info" data-toggle="tooltip" data-html="true" data-original-title="@foreach($row->competency_areas as $competency_area){{$competency_area->name}}<br>@endforeach"> {{$row->competency_areas()->count()}} Competencies tested</label>
                                                            @else
                                                            <label class="label label-info" data-toggle="tooltip" data-html="true" data-original-title=""> {{$row->competency_areas()->first()->name}}</label>
                                                        @endif

                                                    </td>
                                                    <td>{{$row->start_date}}</td>
                                                    <td>{{$row->completed_date}}</td>
                                                    <td>
                                                        <a href="{{route('member.tests.show',['id' => $row->id])}}" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-eye text-inverse m-r-10"></i> </a>
                                                    </td>
                                                </tr>
                                                @endforeach

                                            </tbody>
                                        </table>
                                </div>
                                <div class="tab-pane p-20 show" id="profile4" role="tabpanel">
                                    <div class="col-12">
                                        <h4 align="center"> Your In Progress Tests <i class="fa fa-spinner fa-spin"></i></h4>
                                        <hr>
                                        <div class="row">
                                            @foreach(auth()->user()->location->inProgressTests() as $row)
                                                <div class="col-lg-4 col-md-4">
                                                    <!-- Card -->
                                                    <div class="card">
                                                        <img class="card-img-top img-responsive" src="{{'/uploads'.config('filesystems.storage.focus_area_image_path').$row->focus_area->image}}" alt="Card image cap">
                                                        <div class="card-body">


                                                            <div class="row" style="margin-bottom: 4%">
                                                                <div class="col-12">
                                                                    <h4 class="card-title" align="center"><i class="fa fa-spinner fa-spin"></i> In Progress</h4>
                                                                </div>
                                                                <div class="col-8">
                                                                    Started Date:
                                                                </div>
                                                                <div class="col-4">
                                                                    <strong class="pull-right">{{$row->start_date}}</strong>
                                                                </div>
                                                                <hr>
                                                                <div class="col-12" align="center">
                                                                    <strong >{{$row->focus_area->name}}</strong>
                                                                </div>
                                                                <div class="col-12" align="center">
                                                                    <strong>
                                                                        @if($row->competency_areas()->count() > 1)
                                                                            {{$row->competency_areas()->count()}} Competency Areas Tested
                                                                        @else
                                                                            {{$row->competency_areas()->first()->name}}
                                                                        @endif
                                                                    </strong>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <a href="#" class="btn btn-block btn-danger btn-sm cancel-test" data-id="{{$row->id}}"  >Cancel</a>
                                                                </div>
                                                                <div class="col-6">
                                                                    <a href="#" class="btn btn-block btn-info btn-sm complete-test" data-id="{{$row->id}}" >Complete Test</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Card -->
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>



                </div>
                <!-- end panel -->
            </div>
            <!-- end col-6 -->
        </div>
        <!-- end row -->
    </div>

    <!-- MODAL Complete test -->
    <div id="verticalcenter" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="vcenter" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="vcenter">Complete a Test</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <h4>You are about to complete a <strong>Test</strong>, Would you like to continue</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal" aria-hidden="true" >No, Cancel</button>
                    <button type="button" class="btn btn-success waves-effect" id="complete-test-btn">Yes, Complete Test</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- MODAL Cancel -->
    <div id="cancel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="vcenter" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title" id="vcenter">Cancel TEST</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <h4>You are about to cancel a test</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success waves-effect" data-dismiss="modal" aria-hidden="true">No, Don't</button>
                    <button type="button" class="btn btn-danger waves-effect" id="cancel-test-btn">Yes, Cancel</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


@endsection

@section('script')

    <!-- This is data table -->
    <script src="{{asset('Back-Assets/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script>
        $('#CompletedTableView').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        //complete
        $('.complete-test').on('click',function(){
            $('#test-id').val($(this).data('id'));
            $('#verticalcenter').modal('show');
        });
        $('#complete-test-btn').on('click',function(){
            $.ajax({
                type : 'POST',
                url : "{{url('member/tests/')}}/" + $('#test-id').val(),
                data : "_token={{csrf_token()}}&_method=PUT",
                success : function (data) {
                    window.location = data.url;
                }
            });
        });

        //cancel
        $('.cancel-test').on('click',function(){
            $('#test-id').val($(this).data('id'));
            $('#cancel').modal('show');
        });
        $('#cancel-test-btn').on('click',function(){
            $.ajax({
                type : 'POST',
                url : "{{url('member/tests/')}}/" + $('#test-id').val(),
                data : "_token={{csrf_token()}}&_method=DELETE",
                success : function (data) {
                    window.location = data.url;
                }
            });
        });
    </script>

@endsection

