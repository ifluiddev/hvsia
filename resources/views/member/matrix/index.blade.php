@extends('layouts.member.main')

@section('css')
    {{-- @include('layouts.admin_css.faq_css')--}}

    <link href="{{asset('Back-Assets/assets/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('Back-Assets/css/custom.css')}}" type="text/css" media="screen">

@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Activity Matrix</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Matrix</a></li>
                <li class="breadcrumb-item active">Activity Matrix</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <!-- begin panel -->
                <div class="card" data-sortable-id="tree-view-4">
                    <div class="card-body">
                        <div class="row">
                            @foreach($focus_areas as $row)
                                <div class="col-lg-3 col-md-4">
                                    <!-- Card -->
                                    <div class="card">
                                        <img class="card-img-top img-responsive" src="{{'/uploads'.config('filesystems.storage.focus_area_image_path').$row->image}}" alt="Card image cap">
                                        <div class="card-body">
                                            @if($row->getLatestTest())
                                                <div class="row" style="margin-bottom: 4%">
                                                    <div class="col-12">
                                                        <h5 align="center"><strong>Latest Test:</strong></h5>
                                                    </div>

                                                    <div class="col-8">
                                                        Started Date:
                                                    </div>
                                                    <div class="col-4">
                                                        <strong class="pull-right">{{$row->getLatestTest()->start_date}}</strong>
                                                    </div>
                                                    <div class="col-5">
                                                        Done By:
                                                    </div>
                                                    <div class="col-7">
                                                        <strong class="pull-right">{{$row->getLatestTest()->location->country}}</strong>
                                                    </div>
                                                    <div class="col-6">
                                                        Status:
                                                    </div>
                                                    <div class="col-6">
                                                        <strong class="pull-right">
                                                            @if($row->getLatestTest()->completed)
                                                                <label class="label label-success"><i class="fa fa-check"></i>  Completed</label>
                                                            @else
                                                                <label class="label label-warning"><i class="fa fa-spinner fa-spin"></i>  Pending</label>
                                                            @endif
                                                        </strong>
                                                    </div>
                                                    <div class="col-12" style="margin-top: 13px"></div>
                                                    <div class="col-12" align="center">
                                                        <a href="#" class="btn btn-block waves-effect waves-light btn-rounded btn-primary btn-sm" data-toggle="tooltip" data-html="true" data-original-title="
                                                        @foreach($row->getLatestTest()->competency_areas as $competency_area){{$competency_area->name}}<br>@endforeach
                                                        " style="margin-bottom: 10px">

                                                            @if($row->getLatestTest()->competency_areas()->count() > 1)
                                                                {{$row->getLatestTest()->competency_areas()->count()}} Competency areas in Test
                                                                @else
                                                                {{$row->getLatestTest()->competency_areas()->first()->name}}
                                                            @endif

                                                        </a>
                                                    </div>

                                                </div>
                                                <hr>
                                                <div class="container" align="center">
                                                    <label class="label label-info">{{$row->numberOfCompetencyAreasDone()}} / {{$row->competency_areas_count}} Competency Areas done</label>
                                                </div>
                                                <hr>
                                                <a href="{{route('member.matrices.show',['id' => $row->id])}}" class="btn btn-block btn-outline-success btn-sm" style="color: #0A0A0A">View {{$row->name}} <i class="fa fa-arrow-right"></i></a>
                                                @else
                                                <div class="row" style="margin-bottom: 4%">
                                                    <div class="col-12">
                                                        <h5 align="center"><strong>Latest Test:</strong></h5>
                                                    </div>
                                                    <div class="col-12">
                                                        <p align="center">No test has been added under</p>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="container" align="center">
                                                    <label class="label label-info">0 / {{$row->competency_areas_count}} Competency Areas done</label>
                                                </div>
                                                <hr>
                                                <a href="{{route('member.matrices.show',['id' => $row->id])}}" class="btn btn-block btn-outline-success btn-sm" style="color: #0A0A0A">View {{$row->name}} <i class="fa fa-arrow-right"></i></a>
                                            @endif

                                        </div>
                                        <!-- Card -->
                                    </div>
                                </div>
                            @endforeach


                        </div>



                    </div>
                    <!-- end panel -->
                </div>
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>



@endsection

@section('script')
    {{--@include('layouts.admin_script.faq_script')--}}

    <script src="{{asset('Back-Assets/assets/plugins/jquerytree/jquery.tree.min.js')}}"></script>
    <script src="{{asset('Back-Assets/js/custom.min.js')}}"></script>
    <script src="{{asset('Back-Assets/js/jasny-bootstrap.js')}}"></script>

@endsection

