@extends('layouts.member.main')

@section('css')
    {{-- @include('layouts.admin_css.faq_css')--}}

    <link href="{{asset('Back-Assets/assets/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('Back-Assets/css/custom.css')}}" type="text/css" media="screen">

@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Focus Area: {{$focus_area->name}}</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Activity Matrix</a></li>
                <li class="breadcrumb-item active">Focus Area</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

    @include('layouts.frontend.inc.response')

        <!-- begin row -->


                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-lg-7">
                                <div class="card">
                                    <div class="card-header">
                                        Overview
                                    </div>
                                    <div class="card-body">
                                        <a href="#" class="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#verticalcenter"><i class="fa fa-plus"></i> Start A New Test </a>
                                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>Type</th>
                                                <th>Complete</th>
                                                <th>In Progress</th>
                                                <th width="6%"></th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($focus_area->competency_areas as $row)
                                                <tr>
                                                    <td>{{$row->name}}</td>
                                                    <td>{{$focus_area->numberOfTestCompletedOnACompetencyArea($row->id)}}</td>
                                                    <td>{{$focus_area->numberOfTestInProgressOnACompetencyArea($row->id)}}</td>
                                                    <th>


                                                        <a href="{{route('member.competency-areas.show',['id' => $row->id])}}?focus_area_id={{$focus_area->id}}" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-eye text-inverse m-r-10"></i> </a>
                                                    </th>

                                                </tr>
                                                @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        Recently Completed
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-responsive table-striped">
                                            <thead>
                                            <tr>
                                                <th>Test Type</th>
                                                <th>Done By</th>
                                                <th class="text-nowrap">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($focus_area->latestCompletedTest())
                                                @foreach($focus_area->latestCompletedTest()->competency_areas as $row)
                                                    <tr>
                                                        <td>{{$row->name}}</td>
                                                        <td>
                                                            {{$focus_area->latestCompletedTest()->location->location}}
                                                        </td>

                                                        <td class="text-nowrap">
                                                            <a href="{{route('member.competency-areas.show',['id' => $row->id])}}?focus_area_id={{$focus_area->id}}" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-eye text-inverse m-r-10"></i> </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                @endif

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
        <!-- end panel -->
            </div>
            <!-- end col-6 -->






    <!-- Modal -->
    <div id="verticalcenter" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="vcenter" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="vcenter">Start a New Test</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <h4>Please Select the Test you are about to start.</h4>
                    <hr>
                    <form id="start-test-form" action="{{route('member.tests.store')}}" method="POST">
                        @csrf
                        <input type="hidden" name="focus_area_id" value="{{$focus_area->id}}">
                        <div class="form-group row">
                            <div class="col-md-12">
                                <select name="competency_area_id[]" class="form-control {{ $errors->has('competency_area_id') ? ' is-invalid' : '' }}" style="height: 140px;" multiple required>
                                    @foreach($focus_area->competency_areas as $row)
                                        <option value="{{$row->id}}">{{$row->name}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <a href="javascript:;"  class="btn btn-success waves-effect" id="start-test-btn">Start test now</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



@endsection

@section('script')
    {{--@include('layouts.admin_script.faq_script')--}}

    <script src="{{asset('Back-Assets/assets/plugins/jquerytree/jquery.tree.min.js')}}"></script>
    <script src="{{asset('Back-Assets/js/custom.min.js')}}"></script>
    <script src="{{asset('Back-Assets/js/jasny-bootstrap.js')}}"></script>
    <!-- This is data table -->
    <script src="{{asset('Back-Assets/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script>
        $('#example23').DataTable({
            responsive: true,
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('#start-test-btn').on('click',function(){
            $('#start-test-form').submit();
        });
    </script>
@endsection

