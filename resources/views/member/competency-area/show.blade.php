@extends('layouts.member.main')

@section('css')
    {{-- @include('layouts.admin_css.faq_css')--}}

    <link href="{{asset('Back-Assets/assets/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('Back-Assets/css/custom.css')}}" type="text/css" media="screen">

@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">{{$competency_area->name}} Overview</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$focus_area->name}}</a></li>
                <li class="breadcrumb-item active">{{$competency_area->name}}</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="card-group">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="m-b-0"><i class="mdi mdi-briefcase-check text-info"></i></h2>
                            <h3 class="">{{$competency_area->latestTestCompleted() ? $competency_area->latestTestCompleted()->start_date : 'None'}}</h3>
                            <h6 class="card-subtitle">Latest Completed Test</h6></div>

                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="m-b-0"><i class="mdi mdi-alert-circle text-success"></i></h2>
                            <h3 class="">{{$competency_area->latestTestInProgress() ? $competency_area->latestTestInProgress()->start_date : 'None'}}</h3>
                            <h6 class="card-subtitle">Latest test Started</h6></div>

                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="m-b-0"><i class="mdi mdi-wallet text-purple"></i></h2>
                            <h3 class="">{{$competency_area->testsCompleted()->count()}}</h3>
                            <h6 class="card-subtitle">Tests Completed</h6></div>

                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="m-b-0"><i class="fa fa-spinner fa-spin text-warning"></i></h2>
                            <h3 class="">{{$competency_area->testsInProgress()->count()}}</h3>
                            <h6 class="card-subtitle">In Progress</h6></div>

                    </div>
                </div>
            </div>
        </div>
        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-9">
                <div class="card">
                    <div class="card-header">
                        Completed
                    </div>
                    <div class="card-body">
                        <a href="#" class="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#verticalcenter"><i class="fa fa-plus"></i> Start A New Test </a>
                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Done By</th>
                                <th><i class="fa  fa-clock-o"></i> Started Date</th>
                                <th><i class="fa fa-check"></i> Completed Date</th>
                                <th width="10%"></th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($competency_area->tests()->where('completed',true)->get() as $row)
                                <tr>
                                    <td>{{str_limit($row->location->location,20)}}</td>
                                    <td>{{$row->start_date}}</td>
                                    <td>{{$row->completed_date}}</td>
                                    <th>
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            {{--<a type="button" class="btn btn-secondary" data-toggle="modal" data-target="#ViewDocuements"><i class="fa fa-download"></i> Documents</a>--}}
                                            <button type="button" class="btn btn-sm btn-outline-info view-doc" data-test-id="{{$row->id}}"> <i class="fa fa-eye"></i> View Documents</button>

                                        </div>
                                    </th>

                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="card">
                    <div class="card-header">
                        Testing In Progress
                    </div>
                    <div class="card-body" style="padding: 0px!important;">
                        <div class="table-responsive table-striped">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>In Progress By</th>
                                    <th>Started Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($competency_area->testsInProgress() as $row)
                                    <tr>
                                        <td>{{$row->location->country}}</td>
                                        <td>{{$row->start_date}}</td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end col-6 -->
        </div>
        <!-- end row -->
    </div>

    <!-- Modal Start Test -->
    <div id="verticalcenter" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="vcenter" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="vcenter">Start a New Test</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <h4>You are about to start a <strong>(Test Type)</strong> On <strong>({{$competency_area->name}})</strong>, Would you like to continue</h4>
                    <form id="start-test-form" action="{{route('member.tests.store')}}" method="POST">
                        @csrf
                        <input type="hidden" name="focus_area_id" value="{{$focus_area->id}}">
                        <input type="hidden" name="competency_area_id[]" value="{{$competency_area->id}}">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal" aria-hidden="true">No, Cancel</button>
                    <button type="button" class="btn btn-success waves-effect" id="start-test-btn">Yes, Start Test</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- View Documents Modal -->
    <div id="ViewDocModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Viewing {{$competency_area->name}} Publications</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Author</th>
                            <th class="text-nowrap">Action</th>
                        </tr>
                        </thead>
                        <tbody id="view-doc-content">

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>



@endsection

@section('script')
    {{--@include('layouts.admin_script.faq_script')--}}

    <script src="{{asset('Back-Assets/assets/plugins/jquerytree/jquery.tree.min.js')}}"></script>
    <script src="{{asset('Back-Assets/js/custom.min.js')}}"></script>
    <script src="{{asset('Back-Assets/js/jasny-bootstrap.js')}}"></script>

    <!-- This is data table -->
    <script src="{{asset('Back-Assets/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#myTable').DataTable();
            $(document).ready(function() {
                var table = $('#example').DataTable({
                    "columnDefs": [{
                        "visible": false,
                        "targets": 2
                    }],
                    "order": [
                        [2, 'asc']
                    ],
                    "displayLength": 25,
                    "drawCallback": function(settings) {
                        var api = this.api();
                        var rows = api.rows({
                            page: 'current'
                        }).nodes();
                        var last = null;
                        api.column(2, {
                            page: 'current'
                        }).data().each(function(group, i) {
                            if (last !== group) {
                                $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                                last = group;
                            }
                        });
                    }
                });
                // Order by the grouping
                $('#example tbody').on('click', 'tr.group', function() {
                    var currentOrder = table.order()[0];
                    if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                        table.order([2, 'desc']).draw();
                    } else {
                        table.order([2, 'asc']).draw();
                    }
                });
            });
        });
        $('#example23').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('#start-test-btn').on('click',function(){
            $('#start-test-form').submit();
        });

        //view doc
        $('.view-doc').on('click',function(){
            $.ajax({
                type : 'GET',
                url : "{{route('member.test.publications.index')}}",
                data : "_token={{csrf_token()}}&competency_area_id={{$competency_area->id}}" + "&test_id=" + $(this).data('test-id'),
                success : function (data) {
                    var content = $('#view-doc-content');
                    content.empty();
                    $.each(data,function(index,value){
                        var downloadLink = "{{url('uploads')}}/test/publication/" + value.file_name;
                        var el = $('<tr>\n' +
                            '            <td>' + value.title + '</td>\n' +
                            '            <td>\n' + value.author + '</td>\n' +
                            '            <td class="text-nowrap">\n' +
                            '                <a href="' + downloadLink + '" data-toggle="tooltip" data-original-title="Download" target="_blank"> <i class="fa fa-download text-inverse m-r-10"></i> </a>\n' +
                            '            </td>\n' +
                            '            </tr>');

                        content.append(el);

                    });

                }
            });
            $('#ViewDocModal').modal('show');
        });
    </script>
@endsection

