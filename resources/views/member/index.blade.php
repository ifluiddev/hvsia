@extends('layouts.member.main')

@section('css')
    {{-- @include('layouts.admin_css.faq_css')--}}
    <!-- Dropzone css -->
    <link href="{{asset('Back-Assets/assets/plugins/dropzone-master/dist/dropzone.css')}}" rel="stylesheet" type="text/css" />

@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Dashboard</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>
        <div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="card-group">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="m-b-0"><i class="mdi mdi-account-check text-info"></i></h2>
                            <h3 class="">{{auth()->user()->location->tests()->where('completed',true)->count()}}</h3>
                            <h6 class="card-subtitle"># Your Completed Tests</h6></div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="m-b-0"><i class="mdi mdi-account-convert text-success"></i></h2>
                            <h3 class="">{{auth()->user()->location->tests()->where('completed',false)->count()}}</h3>
                            <h6 class="card-subtitle">Your in Progress Tests</h6></div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="m-b-0"><i class="mdi mdi-chemical-weapon text-purple"></i></h2>
                            <h3 class="">{{\App\Test::where('completed',true)->count()}}</h3>
                            <h6 class="card-subtitle">Overall Tests Completed</h6></div>

                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="m-b-0"><i class="mdi mdi-map-marker-radius text-warning"></i></h2>
                            <h3 class="">{{\App\Test::where('completed',false)->count()}}</h3>
                            <h6 class="card-subtitle">Overall Tests In Progress</h6></div>
                    </div>
                </div>
            </div>
        </div>
        <h4 class="m-b-15">Your Active Tests</h4>
        <input type="hidden" id="test-id" value="" >
        <div class="row">
            <div class="col-12">

                       <div class="row">
                           @foreach(auth()->user()->location->inProgressTests() as $row)
                               <div class="col-lg-3 col-md-3">
                                   <!-- Card -->
                                   <div class="card">
                                       <img class="card-img-top img-responsive" src="{{'/uploads'.config('filesystems.storage.focus_area_image_path').$row->focus_area->image}}" alt="Card image cap">
                                       <div class="card-body">


                                           <div class="row" style="margin-bottom: 4%">
                                               <div class="col-12">
                                                   <h4 class="card-title" align="center"><i class="fa fa-spinner fa-spin"></i> In Progress</h4>
                                               </div>
                                               <div class="col-8">
                                                   Started Date:
                                               </div>
                                               <div class="col-4">
                                                   <strong class="pull-right">{{$row->start_date}}</strong>
                                               </div>
                                               <hr>
                                               <div class="col-12" align="center">
                                                   <strong >{{$row->focus_area->name}}</strong>
                                               </div>
                                               <div class="col-12" align="center">
                                                   <strong>
                                                       @if($row->competency_areas()->count() > 1)
                                                           {{$row->competency_areas()->count()}} Competency Areas Tested
                                                       @else
                                                           {{$row->competency_areas()->first()->name}}
                                                       @endif
                                                   </strong>
                                               </div>
                                           </div>
                                           <hr>
                                           <div class="row">
                                               <div class="col-6">
                                                   <a href="#" class="btn btn-block btn-danger btn-sm cancel-test" data-id="{{$row->id}}"  >Cancel</a>
                                               </div>
                                               <div class="col-6">
                                                   <a href="#" class="btn btn-block btn-info btn-sm complete-test" data-id="{{$row->id}}" >Complete Test</a>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                                   <!-- Card -->
                               </div>
                               @endforeach

                       </div>
            </div>
        </div>
    </div>

<!-- MODAL Complete test -->
    <div id="verticalcenter" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="vcenter" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="vcenter">Complete a Test</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <h4>You are about to complete a <strong>Test</strong>, Would you like to continue</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal" aria-hidden="true" >No, Cancel</button>
                    <button type="button" class="btn btn-success waves-effect" id="complete-test-btn">Yes, Complete Test</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- MODAL Cancel -->
    <div id="cancel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="vcenter" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title" id="vcenter">Cancel TEST</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <h4>You are about to cancel a test</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success waves-effect" data-dismiss="modal" aria-hidden="true">No, Don't</button>
                    <button type="button" class="btn btn-danger waves-effect" id="cancel-test-btn">Yes, Cancel</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


@endsection
@section('script')
    <!-- Dropzone Plugin JavaScript -->
    <script src="{{asset('Back-Assets/assets/plugins/dropzone-master/dist/dropzone.js')}}"></script>
    <script>
        //complete
        $('.complete-test').on('click',function(){
            $('#test-id').val($(this).data('id'));
            $('#verticalcenter').modal('show');
        });
        $('#complete-test-btn').on('click',function(){
            $.ajax({
                type : 'POST',
                url : "{{url('member/tests/')}}/" + $('#test-id').val(),
                data : "_token={{csrf_token()}}&_method=PUT",
                success : function (data) {
                    window.location = data.url;
                }
            });
        });

        //cancel
        $('.cancel-test').on('click',function(){
            $('#test-id').val($(this).data('id'));
            $('#cancel').modal('show');
        });
        $('#cancel-test-btn').on('click',function(){
            $.ajax({
                type : 'POST',
                url : "{{url('member/tests/')}}/" + $('#test-id').val(),
                data : "_token={{csrf_token()}}&_method=PUT",
                success : function (data) {
                    window.location = data.url;
                }
            });
        });
    </script>

@endsection