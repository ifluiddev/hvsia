@extends('layouts.backend.main')

@section('css')
    {{-- @include('layouts.admin_css.faq_css')--}}

    <link href="{{asset('Back-Assets/assets/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('Back-Assets/css/custom.css')}}" type="text/css" media="screen">

@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Manage Banner</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Content</a></li>
                <li class="breadcrumb-item active">Manage Banner</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <!-- begin panel -->
                <div class="card" data-sortable-id="tree-view-4">
                    <div class="card-body">

                        <h4> Fill in the form </h4>
                    </div>
                    <div class="card-body">
                        <form action="{{route('admin.post.banner')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label class="control-label">Banner Title <span class="text-danger">*</span></label>
                                <input type="text" id="title" name="title" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" required value="{{old('title')}}">
                                @if ($errors->has('title'))
                                    <span class="invalid-feedback">
                                              <strong>{{ $errors->first('title') }} </strong>
                                            </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">Banner Text <span class="text-danger">*</span></label>
                                <textarea type="text" id="firstName" name="content" class="form-control {{ $errors->has('content') ? ' is-invalid' : '' }}" required>{{old('name')}}</textarea>
                                @if ($errors->has('content'))
                                    <span class="invalid-feedback">
                                              <strong>{{ $errors->first('content') }} </strong>
                                            </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Background Image <span class="text-danger">*</span></label>
                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                    <div class="form-control" data-trigger="fileinput">
                                        <i class="fa fa-file fileinput-exists"></i>
                                        <span class="fileinput-filename"></span>
                                    </div>
                                    <span class="input-group-addon btn btn-secondary btn-file">
                                            <span class="fileinput-new">Select file</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" name="bg_image" required>
                                        </span>
                                    <a href="#" class="input-group-addon btn btn-secondary fileinput-exists" data-dismiss="fileinput">Remove</a> </div>
                                <small class="form-control-feedback"><b>Recommended Size:</b> <span style="color: red">1920px</span> X <span style="color: red">520px</span> </small>
                                @if ($errors->has('bg_image'))
                                    <span class="invalid-feedback">
                                              <strong>{{ $errors->first('bg_image') }} </strong>
                                            </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Icon (PNG) <span class="text-danger">*</span></label>
                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                    <div class="form-control" data-trigger="fileinput">
                                        <i class="fa fa-file fileinput-exists"></i>
                                        <span class="fileinput-filename"></span>
                                    </div>
                                    <span class="input-group-addon btn btn-secondary btn-file">
                                            <span class="fileinput-new">Select file</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" name="icon" required>
                                        </span>
                                    <a href="#" class="input-group-addon btn btn-secondary fileinput-exists" data-dismiss="fileinput">Remove</a> </div>
                                <small class="form-control-feedback"><b>Recommended Size:</b> <span style="color: red">50px</span> X <span style="color: red">50px</span> *Must be PNG</small>
                                @if ($errors->has('icon'))
                                    <span class="invalid-feedback">
                                              <strong>{{ $errors->first('icon') }} </strong>
                                            </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Set status as:</label>
                                <div class="switch">
                                    <label>Inactive
                                        <input type="checkbox" name="status" checked=""><span class="lever"></span>Active</label>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="control-label">Button 1 Name</label>
                                <input type="text" id="title" name="btn_1_name" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label class="control-label">Button 1 URL</label>
                                <input type="text" id="title" name="btn_1_url" class="form-control" >
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="control-label">Button 2 Name</label>
                                <input type="text" id="title" name="btn_2_name" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label class="control-label">Button 2 URL</label>
                                <input type="text" id="title" name="btn_2_url" class="form-control" >
                            </div>
                            <button type="submit" class="btn waves-effect btn-block waves-light btn-rounded btn-info pull-right"><i class="fa fa-plus"></i> Create New Banner</button>
                            <br/>
                        </form>

                    </div>



                </div>
                <!-- end panel -->
            </div>
            <!-- end col-6 -->
        </div>
        <!-- end row -->


        @endsection

        @section('script')
            {{--@include('layouts.admin_script.faq_script')--}}

            <script src="{{asset('Back-Assets/assets/plugins/jquerytree/jquery.tree.min.js')}}"></script>
            <script src="{{asset('Back-Assets/js/custom.min.js')}}"></script>
            <script src="{{asset('Back-Assets/js/jasny-bootstrap.js')}}"></script>

@endsection
