@extends('layouts.admin')

@section('css')
    @include('layouts.admin_css.form_plugin_css')
@endsection

@section('content')

    <!-- begin #content -->
    <div id="content" class="content">
        <!-- begin breadcrumb -->
    @include('layouts.alert.response')

    <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">Create Footer <small>...</small></h1>
        <!-- end page-header -->

        <!-- begin row -->
        <div class="row">
            <!-- begin col-6 -->
            <div class="col-lg-12">
                <!-- begin panel -->
                <div class="panel panel-inverse" data-sortable-id="form-plugins-1">
                    <!-- begin panel-heading -->
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>

                        </div>
                        <h4 class="panel-title">All Page Footer</h4>
                    </div>
                    <!-- end panel-heading -->
                    <!-- begin panel-body -->
                    <div class="panel-body panel-form">
                        <form class="form-horizontal form-bordered" method="post" action="{{route('post_footer')}}" enctype="multipart/form-data">
                            {{csrf_field()}}

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label">Footer Address</label>
                                <div class="col-md-8">
                                    <div class="input-group ">
                                        <textarea name="address" class="form-control" ></textarea>

                                        @if ($errors->has('logo'))
                                            <span class="invalid-feedback">
                                              <strong>{{ $errors->first('logo') }} </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label">Footer About</label>
                                <div class="col-md-8">
                                    <div class="input-group ">
                                        <textarea name="about" class="form-control" ></textarea>

                                        @if ($errors->has('about'))
                                            <span class="invalid-feedback">
                                              <strong>{{ $errors->first('about') }} </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label">Footer Extra</label>
                                <div class="col-md-8">
                                    <div class="input-group ">
                                        <textarea name="extra" class="form-control" ></textarea>

                                        @if ($errors->has('extra'))
                                            <span class="invalid-feedback">
                                              <strong>{{ $errors->first('extra') }} </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-8">
                                    <input type="submit" value="Save" class="btn btn-success">
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- end panel-body -->
                </div>
                <!-- end panel -->


            </div>
            <!-- end col-6 -->

        </div>
        <!-- end row -->
    </div>
    <!-- end #content -->

@endsection

@section('script')
    @include('layouts.admin_script.form_plugin_script')
@endsection