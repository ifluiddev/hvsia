@extends('layouts.backend.main')

@section('css')
   {{-- @include('layouts.admin_css.faq_css')--}}

    <link href="{{asset('Back-Assets/assets/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" />
   <link rel="stylesheet" href="{{asset('Back-Assets/css/custom.css')}}" type="text/css" media="screen">
@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Manage CMS</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Content</a></li>
                <li class="breadcrumb-item active">Manage Content</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- begin row -->
        <div class="row">
            <!-- begin col-6 -->
            <div class="col-md-3">
                <!-- begin panel for Main menu structure -->
                <div class="card" data-sortable-id="tree-view-3">
                    <div class="card-body">
                        <h4 class="card-title">CMS Main Menu Structure</h4>
                    </div>
                    <div class="card-body">

                        <div class="toolbar" align="center">
                            <div class="btn-group" style="width: 100%">
                                <a class="btn btn-white btn-primary btn-sm " id="btn-create"  href="#" style="width: 33.33%; border-radius: 0px"><i class="fa fa-plus"></i> Create  </a>
                                <a class="btn btn-white btn-warning btn-sm " id="btn-rename"  href="#" style="width: 33.33%"><i class="fa fa-edit"></i> Rename</a>
                                <a class="btn btn-white btn-danger  btn-sm" id="btn-delete"  href="#" style="width: 33.33%; border-radius: 0px"><i class="fa fa-trash"></i> Delete</a>
                            </div>

                            <hr>
                        </div>

                        <ul id="tt-main-menu" >

                        </ul>


                    </div>
                </div>

                <!-- end panel -->
                <!-- begin panel for footer nav structure -->
                <div class="card" data-sortable-id="tree-view-3">
                    <div class="card-body">

                        <h4 class="card-title">CMS Footer Menu Structure</h4>
                    </div>
                    <div class="card-body">
                        <div class="toolbar" align="center">
                            <div class="btn-group" style="width: 100%">
                                <a class="btn btn-white btn-primary btn-sm " id="btn-create-f"  href="#" style="width: 33.33%; border-radius: 0px"><i class="fa fa-plus"></i> Create  </a>
                                <a class="btn btn-white btn-warning btn-sm " id="btn-rename-f"  href="#" style="width: 33.33%"><i class="fa fa-edit"></i> Rename</a>
                                <a class="btn btn-white btn-danger  btn-sm" id="btn-delete-f"  href="#" style="width: 33.33%; border-radius: 0px"><i class="fa fa-trash"></i> Delete</a>
                            </div>
                        </div>
                        <hr>
                        <ul id="tt-footer-menu"></ul>


                    </div>
                </div>
                <!-- end panel -->
            </div>
            <!-- end col-3 -->
            <!-- begin col-9 -->
            <div class="col-md-9">
                <!-- begin panel -->
                <div class="card" data-sortable-id="tree-view-4">
                    <div class="card-body">

                        <h4 class="card-title">Create CMS</h4>
                    </div>
                    <div class="card-body">
                        <form id="page-form" action="{{route('admin.ajax.update.page-detail')}}" method="post">
                            {{csrf_field()}}
                            <div id="page-form-container" style="display: none;">
                                <div class="toolbar" style="margin-bottom: 20px;">

                                    <div class="row">
                                        <div class="col-sm-6" align="center">
                                            <div class="row">
                                                <div class="col-md-6" >
                                                    <div id="unpublish-div">
                                                        <span style="color: green"><i class="fa fa-circle"></i> Published</span>
                                                        <form id="unpublish-form" action="" method="post">
                                                            {{csrf_field()}}
                                                            <input type="hidden" value="0" name="page_status">
                                                            <input class="page-id" type="hidden"  name="id">
                                                            <button id="btn-unpublish" type="button" class="btn btn-danger btn-sm" style="width: 100%; margin-top: 10px">Unpublish <i class="fa fa-globe"></i></button>
                                                        </form>
                                                    </div>
                                                    <div id="publish-div">
                                                        <span style="color: orangered"><i class="fa fa-circle"></i> Draft</span>
                                                        <form id="publish-form" action="" method="post">
                                                            {{csrf_field()}}
                                                            <input type="hidden" value="1" name="page_status">
                                                            <input class="page-id" type="hidden"  name="id">
                                                            <button id="btn-publish" type="button"  class="btn btn-success btn-sm" style="width: 100%; margin-top: 10px">Publish <i class="fa fa-globe"></i></button>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <span style="color: green"><i class="fa fa-globe"></i> Preview</span>
                                                    <a  href="#" id="preview-btn" class="btn btn-dark btn-sm" style="width: 100%; margin-top: 10px">Preview Page</a>

                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-sm-6" align="center" style="border-left: 1px dotted black">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label>Appear In Main Menu</label><br />
                                                    <div class="switcher">
                                                        <input type="checkbox" class="is-valid"  id="is-visible" value="0">
                                                        <label for="is-visible"></label>
                                                        <input type="hidden" id="is-visible-h" name="is_visible" value="0">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label>Appear In Footer</label><br />
                                                    <div class="switcher">
                                                        <input type="checkbox" class="is-valid"  id="is-visible-on-footer" value="1">
                                                        <label for="is-visible-on-footer"></label>
                                                        <input type="hidden" id="is-visible-on-footer-h" name="is_visible_on_footer" value="0">
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>


                                    <div class="clear"></div>
                                </div>
                                <!-- Page ID -->
                                <input type="hidden" id="page-id" class="page-id" name="cms_id">

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Banner Title</label>
                                            <input  type="text" id="banner-title" class="form-control" placeholder="Banner Title" name="banner_description" >
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Page Template</label>
                                            <select id="template" name="template" class="form-control">
                                                <option readonly="readonly" selected>Please Select Template</option>
                                                <option value="home">Home</option>
                                                <option value="cms">CMS</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <label>Content</label>
                                    <textarea  id="content-cms"  name="content"></textarea>

                                </div>

                                <div class="form-group">
                                    <label>Page Redirect</label>
                                    <input type="text" id="redirect-url" class="form-control" placeholder="e.g http://www.example.com/news" name="redirect_url" value="" >
                                    <small class="f-s-12 text-grey-darker">This is only required if page will be redirected to a different web page</small>
                                </div>
                                <div class="form-group">
                                    <label>SEO Title</label>
                                    <input type="text" id="seo-title" class="form-control" placeholder="SEO Title" name="page_title" >
                                </div>
                                <div class="form-group">
                                    <label>SEO Description</label>
                                    <textarea name="page_meta_description"  id="page-meta-description" class="form-control" rows="4"></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="button" id="save" class="btn btn-success">Save</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
                <!-- end panel -->
            </div>
            <!-- end col-6 -->
        </div>
        <!-- end row -->

@endsection

@section('script')
    {{--@include('layouts.admin_script.faq_script')--}}

    <script src="{{asset('Back-Assets/assets/plugins/jquerytree/jquery.tree.min.js')}}"></script>
    <script src="{{asset('Back-Assets/assets/plugins/jquerytree/jquery.draggable.min.js')}}"></script>
    <script src="{{asset('Back-Assets/assets/plugins/jquerytree/jquery.droppable.min.js')}}"></script>
    <script src="{{asset('Back-Assets/assets/plugins/jquerytree/jquery.etree.js')}}"></script>
    <script src="{{asset('Back-Assets/assets/plugins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('Back-Assets/assets/plugins/gritter/js/jquery.gritter.js')}}"></script>

    <script>

        $(document).ready(function()
        {
            $('#page-form-container').hide();

            $('#tt-main-menu').etree(
                {
                    url: "{{route('admin.ajax.get.pages')}}",
                    createUrl: "{{route('admin.ajax.create.page')}}",
                    updateUrl: "{{route('admin.ajax.update.page')}}",
                    destroyUrl: "{{route('admin.ajax.delete.page')}}",
                    dndUrl: "{{route('admin.ajax.dnd.page')}}",
                    onSelect: function (node)
                    {
                        if(node.id == null){
                            //do nothing
                        }
                        getPageDetail(node.id);
                    }
                });

            $('#btn-create')
                .button({
                    icons: {
                        primary: 'ui-icon-plus'
                    }
                })
                .click(function (e) {

                    e.preventDefault();

                    $('#tt-main-menu').etree('create');

                });

            $('#btn-rename')
                .button({
                    icons: {
                        primary: 'ui-icon-refresh'
                    }
                })
                .click(function (e)
                {
                    e.preventDefault();

                    if ($('#tt-main-menu > li > div.tree-node-selected').attr('node-id') == 'null')
                    {
                        alert('Please select a page from the list first!');
                        return false;
                    }

                    $('#tt-main-menu').etree('edit');

                });

            $('#btn-delete')
                .button({
                    icons: {
                        primary: 'ui-icon-close'
                    }
                })
                .click(function (e) {

                    e.preventDefault();

                    $('#tt-main-menu').etree('destroy');

                });

            //submit form
            $('#save').on('click',function(){
                updatePage();
            });

            //on clicking publish button
            $('#btn-publish').on('click',function(e){
                $('#publish-div').hide();
                $('#unpublish-div').show();
                e.preventDefault();
                publishPage();
            });
            //on clicking unpublish button
            $('#btn-unpublish').on('click',function(e){
                $('#publish-div').show();
                $('#unpublish-div').hide();
                e.preventDefault();
                unpublishPage();
            });

            /**
             *
             * Footer Menu Structure
             *
             *
             */

            $('#tt-footer-menu').etree(
                {
                    url: "{{route('admin.ajax.get.footer-menus')}}",
                    createUrl: "{{route('admin.ajax.create.footer-menu')}}",
                    updateUrl: "{{route('admin.ajax.update.footer-menu')}}",
                    destroyUrl: "{{route('admin.ajax.delete.footer-menu')}}",
                    dndUrl: "{{route('admin.ajax.dnd.footer-menu')}}",
                    onSelect: function (node)
                    {
                        if(node.id == null){
                            //do nothing
                        }
                        getPageDetail(node.id);
                    }
                });
            $('#btn-create-f')
                .button({
                    icons: {
                        primary: 'ui-icon-plus'
                    }
                })
                .click(function (e) {

                    e.preventDefault();

                    $('#tt-footer-menu').etree('create');

                });

            $('#btn-rename-f')
                .button({
                    icons: {
                        primary: 'ui-icon-refresh'
                    }
                })
                .click(function (e)
                {
                    e.preventDefault();

                    if ($('#tt-footer-menu > li > div.tree-node-selected').attr('node-id') == 'null')
                    {
                        alert('Please select a page from the list first!');
                        return false;
                    }

                    $('#tt-footer-menu').etree('edit');

                });

            $('#btn-delete-f')
                .button({
                    icons: {
                        primary: 'ui-icon-close'
                    }
                })
                .click(function (e) {

                    e.preventDefault();

                    $('#tt-footer-menu').etree('destroy');

                });

            //OTHER FUNCTIONS

            var getPageDetail = function(id)
            {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $('#content-cms').html('');
                $.ajax({
                    type: "POST",
                    url: '{{route('admin.ajax.get.page-detail')}}',
                    data: {'_token' : CSRF_TOKEN, 'id' : id},
                    success: function (data) {
                        console.log(data);
                        //Apear in main menu
                        $('#is-visible').prop('checked',data.is_visible);
                        $('#is-visible-h').val(data.is_visible);
                        //Apear in footer
                        $('#is-visible-on-footer').prop('checked',data.is_visible_on_footer);
                        $('#is-visible-on-footer-h').val(data.is_visible_on_footer);
                        //Template name
                        $('#template > option').each(function() {

                            if ( $(this).val() == data.template){
                                $(this).attr('selected', 'selected');
                            }
                        });

                        //Others
                        $('.page-id').each(function () {
                            $(this).val(data.id);
                        });

                        $('#banner-title').val(data.banner_description);
                        $('#content-cms').val(data.content);
                        tinyMCE.activeEditor.setContent(data.content==null?'<span>some</span> html':data.content);
                        $('#redirect-url').val(data.redirect_url);
                        $('#seo-title').val(data.page_title);
                        $('#page-meta-description').val(data.page_meta_description);
                        $('#page-form-container').show();

                        //hide and show publish and unpublish button
                        if(data.page_status == 0){
                            $('#publish-div').show();
                            $('#unpublish-div').hide();
                        }
                        else {
                            $('#publish-div').hide();
                            $('#unpublish-div').show();
                        }

                        //Preview button
                        $('#preview-btn').prop('href','{{url('admin/page-preview')}}/'+data.slug);

                    }
                });

                //Tiny mce

                var editor_config = {
                    setup: function (editor) {
                        editor.on('change', function () {
                            editor.save();
                        });
                    },
                    path_absolute : "/",
                    selector: "textarea[id='content-cms']",
                    theme: 'modern',
                    width: '100%',
                    height: 500,
                    subfolder: '',
                    content_css: '/Back-Assets/css/style.css, /Back-Assets/assets/plugins/bootstrap/css/bootstrap.min.css',
                    relative_urls: false,
                    convert_urls: false,
                    link_list: [],
                    external_plugins: {'nanospell': '/Back-Assets/assets/plugins/tinymce/plugins/spellchecker/plugin.js',
                        'bootstrap': '/Back-Assets/assets/plugins/tinymce/plugins/bootstrap/plugin.js'},
                    nanospell_server: 'php', // choose "php" "asp" "asp.net" or "java"
                    nanospell_dictionary: "en",
                    nanospell_autostart: true,
                    nanospell_ignore_words_with_numerals: true,
                    nanospell_ignore_block_caps: false,
                    nanospell_compact_menu: false,
                    toolbar: "nanospell",
                    // replace with your images folder path
                    plugins:
                        [
                            'advlist autolink link image lists charmap print preview hr anchor pagebreak',
                            'searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking',
                            'table contextmenu directionality emoticons paste textcolor fullscreen'

                        ],
                    bootstrapConfig: {
                        'bootstrapElements': {
                            'btn': true,
                            'icon': true,

                            'snippet': true,
                            'table': true,
                            'template': true,
                            'label': true,
                            'alert':true,
                            'panel': true
                        },
                        'bootstrapCssPath': '/Back-Assets/assets/plugins/tinymce/plugins/bootstrap/css/bootstrap.min.css',
                        'imagesPath': '/tinymce-bootstrap-plugin/examples/img/',
                    },
                    image_advtab: true,
                    toolbar1: 'undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect',
                    toolbar2: 'forecolor backcolor | link unlink anchor | image media | preview code fullscreen | bootstrap',
                    style_formats: [
                        {title: 'Headers', items: [
                            {title: 'h1', block: 'h1'},
                            {title: 'h2', block: 'h2'},
                            {title: 'h3', block: 'h3'},
                            {title: 'h4', block: 'h4'},
                            {title: 'h5', block: 'h5'},
                            {title: 'h6', block: 'h6'}
                        ]},
                        {title: 'Blocks', items: [
                            {title: 'p', block: 'p'},
                            {title: 'div', block: 'div'},
                            {title: 'pre', block: 'pre'}
                        ]},
                        {title: 'Containers', items: [
                            {title: 'Info Block', block: 'div', classes: 'info-block'}
                        ]}
                    ],

                    file_browser_callback : function(field_name, url, type, win) {
                        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                        if (type == 'image') {
                            cmsURL = cmsURL + "&type=Images";
                        } else {
                            cmsURL = cmsURL + "&type=Files";
                        }

                        tinyMCE.activeEditor.windowManager.open({
                            file : cmsURL,
                            title : 'Filemanager',
                            width : x * 0.8,
                            height : y * 0.8,
                            resizable : "yes",
                            close_previous : "no"
                        });
                    }
                };

                tinymce.init(editor_config);
                tinyMCE.activeEditor.setContent('<span>some</span> html');
            }

            var updatePage = function()
            {
                console.log($('#page-form').serialize());
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    type: "POST",
                    url: "{{route('admin.ajax.update.page-detail')}}",
                    data: $('#page-form').serialize(),
                    success: function (data) {
                        console.log(data);
                        if(data.error == true){
                            displayNotification('You cannot select home page twice');
                        }
                        //Apear in main menu
                        $('#is-visible').prop('checked',data.is_visible);
                        $('#is-visible-h').val(data.is_visible);
                        //Apear in footer
                        $('#is-visible-on-footer').prop('checked',data.is_visible_on_footer);
                        $('#is-visible-on-footer-h').val(data.is_visible_on_footer);

                        //Template name
                        $('#template > option').each(function() {

                            if ( $(this).val() == data.template){
                                $(this).attr('selected', 'selected');
                            }
                        });

                        //Others
                        $('#banner-title').val(data.banner_description);
                        $('#editor1').val(data.content);
                        $('#redirect-url').val(data.redirect_url);
                        $('#seo-title').val(data.page_title);
                        $('#page-meta-description').val(data.page_meta_description);
                        $('#page-form-container').show();

                        displayNotification('Your update is successful');

                        //window.location.reload(true);
                    }
                });
            }

            var publishPage = function()
            {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    type: "POST",
                    url: "{{route('admin.ajax.publish.page')}}",
                    data: $('#publish-form').serialize(),
                    success: function (data) {
                        displayNotification('You successfully publish a page');
                    }
                });
            }
            var unpublishPage = function()
            {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    type: "POST",
                    url: "{{route('admin.ajax.publish.page')}}",
                    data: $('#unpublish-form').serialize(),
                    success: function (data) {
                        displayNotification('You successfully unpublish a page');
                    }
                });
            }

        });

    </script>

    <script>


        $(document).ready(function () {

            //Is visible
            $('#is-visible').change(function () {
                if ($('#is-visible').prop('checked')) {
                    $('#is-visible-h').val('1');
                }
                else {
                    $('#is-visible-h').val('0');
                }
            });
            if ($('#is-visible').prop('checked')) {
                $('#is-visible-h').val('1');
            }
            else {
                $('#is-visible-h').val('0');
            }

            // Visible on footer

            $('#is-visible-on-footer').change(function () {
                if ($('#is-visible-on-footer').prop('checked')) {
                    $('#is-visible-on-footer-h').val('1');
                }
                else {
                    $('#is-visible-on-footer-h').val('0');
                }
            });

            if ($('#is-visible-on-footer').prop('checked')) {
                $('#is-visible-on-footer-h').val('1');
            }
            else {
                $('#is-visible-on-footer-h').val('0');
            }
        });

        //Handle Gritter Notification
        var displayNotification = function(text) {
            setTimeout(function() {
                $.gritter.add({
                    title: 'Alert',
                    text: text,
                    sticky: false,
                    time: '',
                    class_name: 'my-sticky-class'
                });
            }, 1000);
        };



    </script>

@endsection
