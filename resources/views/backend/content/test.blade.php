

@extends('layouts.admin')

@section('css')
    @include('layouts.admin_css.faq_css')
@endsection

@section('content')

    <!-- begin #content -->
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <div class="alert">

            @if(Session::has('message'))
                <div class="alert alert-warning">
                    {{ Session::get('message') }}
                </div>
            @endif
        </div>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">Create CMS Page<small>...</small></h1>
        <!-- end page-header -->



        <!-- begin row -->
        <div class="row">
            <!-- begin col-6 -->
            <div class="col-lg-4">
                <!-- begin panel for Main menu structure -->
                <div class="panel panel-inverse" data-sortable-id="tree-view-3">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                        <h4 class="panel-title">CMS Main Menu Structure</h4>
                    </div>
                    <div class="panel-body">


                        <div class="cf nestable-lists">

                            <div class="dd " id="nestable">
                                <ol class="dd-list">
                                    <!--=========== Single Menu ===============-->
                                    @foreach(\App\CMS::menus() as $row)
                                        <li class="dd-item" data-id="{{$row->id}}">
                                            <div class="dd-handle">{{$row->page_name}}</div>
                                            @if($row->sub_menus())
                                                @foreach($row->sub_menus() as $child)
                                                    <ol class="dd-list">
                                                        <li class="dd-item" data-id="{{$child->id}}">
                                                            <a href="" class="dd-handle">{{$child->page_name}}</a>
                                                        </li>

                                                    </ol>
                                                @endforeach
                                            @endif

                                        </li>
                                    @endforeach
                                </ol>
                            </div>

                        </div>

                        {{--<p><strong>Serialised Output (per list)</strong></p>

                        <textarea id="nestable-output"></textarea>--}}


                    </div>
                </div>
                <!-- end panel -->
                <!-- begin panel for footer nav structure -->
                <div class="panel panel-inverse" data-sortable-id="tree-view-3">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                        <h4 class="panel-title">CMS Footer Menu Structure</h4>
                    </div>
                    <div class="panel-body">
                        {{--<menu id="nestable-menu">
                            <button type="button" class="btn btn-dark" data-action="expand-all">Expand All</button>
                            <button type="button" class="btn btn-dark" data-action="collapse-all">Collapse All</button>
                        </menu>--}}

                        <div class="cf nestable-lists">

                            <div class="dd " id="nestable-footer">
                                <ol class="dd-list">
                                    <!--=========== Single Menu ===============-->
                                    @foreach(\App\CMSFooter::orderBy('menu_order','asc')->get() as $row)
                                        <li class="dd-item" data-id="{{$row->id}}">
<table class="table table-resposive">
    <tr>
        <td>
            <div class="btn  btn-dark pull-right dd-handle">{{$row->cms->page_name}}</div>
        </td>
        <td>
            <a class="btn bt-xs btn-dark pull-left" href="{{route('create_faq')}}"> <i class="fa fa-plus"></i></a>
        </td>
    </tr>
</table>


                                        </li>
                                    @endforeach
                                </ol>
                            </div>

                        </div>

                        {{--<p><strong>Serialised Output (per list)</strong></p>

                        <textarea id="nestable-output"></textarea>--}}


                    </div>
                </div>
                <!-- end panel -->
            </div>
            <!-- end col-6 -->
            <!-- begin col-6 -->
            <div class="col-lg-8">
                <!-- begin panel -->
                <div class="panel panel-inverse" data-sortable-id="tree-view-4">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                        <h4 class="panel-title">Create CMS</h4>
                    </div>
                    <div class="panel-body">
                        <form action="{{route('add_new_page')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="page_status" value="0">
                            <div class="form-group">
                                <label>Appear In Main Menu</label>
                                <input type="checkbox" class="checkbox checkbox-css is-valid"  id="is-visible"  >
                                <input type="hidden" id="is-visible-h" name="is_visible" value="0">
                            </div>
                            <div class="form-group">
                                <label>Appear In Footer</label>
                                <input type="checkbox" class="checkbox checkbox-css is-valid"  id="is-visible-on-footer"  >
                                <input type="hidden" id="is-visible-on-footer-h" name="is_visible_on_footer" value="0">
                            </div>
                            <div class="form-group">
                                <label>Make Index Page</label>
                                <input type="checkbox" class="checkbox checkbox-css is-valid" id="is-index-page"  >
                                <br><small class="f-s-12 text-grey-darker">Note: Tick only for home page</small>
                                <input type="hidden" id="is-index-page-h" name="is_index_page" value="0">
                            </div>
                            <div class="form-group">
                                <label>Page Name</label>
                                <input  type="text" class="form-control" placeholder="Page Name" name="page_name" >
                            </div>
                            <div class="form-group">
                                <label>Banner Title</label>
                                <input  type="text" class="form-control" placeholder="Banner Title" name="banner_description" >
                            </div>
                            <div class="form-group">
                                <label>Page Slug</label>
                                <input type="text" class="form-control" placeholder="Page Slug" name="slug" >
                            </div>
                            <div class="form-group">
                                <label>Page Template</label>
                                <select name="template" class="form-control">
                                    <option readonly="readonly" selected>Please Select Template</option>
                                    <option value="home">Home</option>
                                    <option value="cms">CMS</option>

                                </select>
                            </div>
                            <div class="form-group">
                                <label>Content</label>
                                <textarea class="ckeditor" id="editor1" required name="content" rows="80"></textarea>

                            </div>

                            <div class="form-group">
                                <label>Page Redirect</label>
                                <input type="text" class="form-control" placeholder="Redirect URL" name="redirect_url" value="" >
                                <small class="f-s-12 text-grey-darker">This is only required if page will be redirected to a different web page</small>
                            </div>
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" class="form-control" placeholder="Page Title" name="page_title" >
                            </div>
                            <div class="form-group">
                                <label>SEO Description</label>
                                <textarea name="page_meta_description" class="form-control" rows="4"></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">Create New <i class="fa fa-plus"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- end panel -->
            </div>
            <!-- end col-6 -->
        </div>
        <!-- end row -->
    </div>


@endsection

@section('script')
    @include('layouts.admin_script.faq_script')

    <script src="/admin/assets/js/jquery.nestable.js"></script>

    <script>

        $(document).ready(function()
        {

            var updateMainMenu = function(e)
            {
                var list   = e.length ? e : $(e.target),
                    CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    type: "POST",
                    url: '{{route('cms.menu.update')}}',
                    data: {_token: CSRF_TOKEN, data: list.nestable('serialize')},
                    success: function (data) {
                        /* $.each(data, function (index,element) {
                             skillIDs.push(parseInt(element));
                         });

                         setTimeout(function(){
                             that.val('Save');
                         },2000);*/
                    }
                });
            };

            var updateFooter = function(e)
            {
                var list   = e.length ? e : $(e.target),
                    CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    type: "POST",
                    url: '{{route('cms.footer-menu.update')}}',
                    data: {_token: CSRF_TOKEN,data:list.nestable('serialize')},
                    success: function(data) {
                        $('#nestable-footer').nestable('reset');
                    }
                });
            };

            // activate Nestable for list 1
            $('#nestable').nestable({
                group: 1
            }).on('change', updateMainMenu);

            // activate Nestable for list 1
            $('#nestable-footer').nestable({
                group: 1
            }).on('change', updateFooter);


            /* // output initial serialised data
             updateOutput($('#nestable').data('output', $('#nestable-output')));*/



        });
    </script>

    <script>


        $(document).ready(function () {

            //Is visible
            $('#is-visible').change(function () {
                if ($('#is-visible').prop('checked')) {
                    $('#is-visible-h').val('1');
                }
                else {
                    $('#is-visible-h').val('0');
                }
            });
            /* if ($('#is-visible').prop('checked')) {
                 $(this).val('1');
             }
             else {
                 $(this).val('0');
             }*/

            //Is index page

            $('#is-index-page').change(function () {
                if ($('#is-index-page').prop('checked')) {
                    $('#is-index-page-h').val('1');
                }
                else {
                    $('#is-index-page-h').val('0');
                }
            });


            // Visible on footer

            $('#is-visible-on-footer').change(function () {
                if ($('#is-visible-on-footer').prop('checked')) {
                    $('#is-visible-on-footer-h').val('1');
                }
                else {
                    $('#is-visible-on-footer-h').val('0');
                }
            });
        });



    </script>

@endsection




