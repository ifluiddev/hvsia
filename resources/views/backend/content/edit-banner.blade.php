@extends('layouts.backend.main')

@section('css')
    {{-- @include('layouts.admin_css.faq_css')--}}

    <link href="{{asset('Back-Assets/assets/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('Back-Assets/css/custom.css')}}" type="text/css" media="screen">

@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Manage Banner</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Content</a></li>
                <li class="breadcrumb-item active">Manage Banner</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <!-- begin panel -->
                <div class="card" data-sortable-id="tree-view-4">
                    <div class="card-body">

                        <h4> Fill in the form </h4>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="card-body">
                        <form action="{{route('admin.update.banner')}}" method="post">
                            @csrf
                            <input type="hidden"  name="id"  value="{{$banner->id}}" required >
                            <div class="form-group">
                                <label class="control-label">Banner Title <span class="text-danger">*</span></label>
                                <input type="text" id="title" name="title" class="form-control" value="{{$banner->title}}" required >
                            </div>
                            <div class="form-group">
                                <label class="control-label">Banner Text <span class="text-danger">*</span></label>
                                <textarea type="text" id="firstName" name="content" class="form-control" required>{{$banner->content}}</textarea>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="control-label">Button 1 Name</label>
                                <input type="text" id="title" name="btn_1_name" class="form-control" value="{{$banner->btn_1_name}}">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Button 1 URL</label>
                                <input type="text" id="title" name="btn_1_url" class="form-control" value="{{$banner->btn_1_url}}">
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="control-label">Button 2 Name</label>
                                <input type="text" id="title" name="btn_2_name" class="form-control" value="{{$banner->btn_2_name}}">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Button 2 URL</label>
                                <input type="text" id="title" name="btn_2_url" class="form-control" value="{{$banner->btn_2_url}}">
                            </div>
                            <button type="submit" class="btn waves-effect btn-block waves-light btn-rounded btn-info pull-right"> Save</button>
                            <br/>
                        </form>

                    </div>

                    <div class="card-body">
                        <table id="data-table-combine" class="table table-striped table-bordered">
                            <thead>

                            <tr>
                                <th class="text-nowrap">Field</th>
                                <th class="text-nowrap">Image</th>
                                <th class="text-nowrap">Action</th>
                            </tr>

                            </thead>
                            <tbody>
                            <tr>
                                <td>Image</td>
                                <td width="1%" class="with-img">
                                    <img src="/uploads/banner_image/{{$banner->bg_image}}" width="180" height="80" />
                                </td>
                                <td>
                                    <form enctype="multipart/form-data" id="form-edit-electronic-image" action="{{route('admin.update.banner-image')}}" method="POST">
                                        <input type="hidden" name="id" value="{{$banner->id}}">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <input type="file" name="bg_image" class="form-control" />
                                            <span class="text-danger">
                                                    {{$errors->first('bg_image')}}
                                                </span>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" value="Save" class="btn btn-success">
                                        </div>

                                    </form>
                                </td>
                            </tr>
                            <tr>
                                <td>Banner Icon</td>
                                <td width="1%" class="with-img">
                                    <img src="/uploads/banner_icon/{{$banner->icon}}" width="180" height="80" />
                                </td>
                                <td>
                                    <form enctype="multipart/form-data" action="{{route('admin.update.banner-icon')}}" method="POST">
                                        <input type="hidden" name="id" value="{{$banner->id}}">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <input type="file" name="icon" class="form-control" />
                                            <span class="text-danger">
                                                    {{$errors->first('icon')}}
                                                </span>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" value="Save" class="btn btn-success">
                                        </div>

                                    </form>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>



                </div>
                <!-- end panel -->
            </div>
            <!-- end col-6 -->
        </div>
        <!-- end row -->


        @endsection

        @section('script')
            {{--@include('layouts.admin_script.faq_script')--}}

            <script src="{{asset('Back-Assets/assets/plugins/jquerytree/jquery.tree.min.js')}}"></script>
            <script src="{{asset('Back-Assets/js/custom.min.js')}}"></script>
            <script src="{{asset('Back-Assets/js/jasny-bootstrap.js')}}"></script>

@endsection
