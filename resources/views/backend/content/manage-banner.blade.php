@extends('layouts.backend.main')

@section('css')
   {{-- @include('layouts.admin_css.faq_css')--}}

    <link href="{{asset('Back-Assets/assets/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" />
   <link rel="stylesheet" href="{{asset('Back-Assets/css/custom.css')}}" type="text/css" media="screen">

@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Manage Banner</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Content</a></li>
                <li class="breadcrumb-item active">Manage Banner</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Banners</h4>
                        <h6 class="card-subtitle pull-right"><a href="{{route('admin.show.create-banner')}}" type="button" class="btn waves-effect waves-light btn-sm btn-info"><i class="fa fa-plus"></i> Upload New Banner</a></h6>
                        @include('layouts.frontend.inc.response')
                        <div class="table-responsive">
                            <table class="table table-striped ">
                                <thead>
                                <tr>
                                    <th>ID #</th>
                                    <th>Banner Title</th>
                                    <th>Status</th>
                                    <th class="text-nowrap">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($banners as $row)
                                    <tr>
                                        <td>{{$row->id}}</td>
                                        <td>
                                            {{$row->title}}
                                        </td>
                                        <td>
                                                @if($row->status == true)

                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <span class="label label-success">Active</span>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <form action="{{route('admin.post.publish-banner',['id'=>$row->id])}}" method="post" enctype="multipart/form-data">
                                                                {{csrf_field()}}
                                                                <input type="hidden" value="off" name="status">
                                                                <button  class="btn btn-danger btn-xs">Inactivate</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <span class="label label-danger">Inactive</span>
                                                        </div>
                                                        <div class="col-md-8">

                                                            <form action="{{route('admin.post.publish-banner',['id'=>$row->id])}}" method="post" enctype="multipart/form-data">
                                                                {{csrf_field()}}
                                                                <input type="hidden" value="on" name="status"></button>
                                                                <button  class="btn btn-success btn-xs">Activate</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                @endif
                                        </td>
                                        <td class="text-nowrap">
                                            <a href="{{route('admin.show.edit-banner',['id' => $row->id])}}" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                            <a href="#modal-dialog"  data-id = "{{$row->id}}"  data-toggle="modal" style="width:100%" title="Delete"  id="delete-banner" class="trigger-modal"> <i class="fa fa-close text-danger"></i> </a>
                                        </td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>



        </div>
        <!-- end row -->

        <!--====================================== #modal-dialog ========================================-->
        <div class="modal fade" id="modal-dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Banner</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <p>
                            Are you sure you want to delete this Banner
                        </p>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="btn btn-white"  data-dismiss="modal">Close</a>
                        <form action="{{route('admin.delete.banner')}}" method="post">
                            {{csrf_field()}}
                            <input type="hidden"  name="id" id="banner-id" value="1">
                            <input type="submit" class="btn btn-danger" id="delete" value="Delete">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #modal-without-animation -->


@endsection

@section('script')
    {{--@include('layouts.admin_script.faq_script')--}}

    <script>

        $('.trigger-modal').on('click', function () {
            var banner_id = $(this).attr('data-id');
            $('#banner-id').val(banner_id);
            $('#modal-dialog').modal('show');

        });

    </script>

@endsection
