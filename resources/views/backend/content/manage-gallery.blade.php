@extends('layouts.backend.main')

@section('css')
   {{-- @include('layouts.admin_css.faq_css')--}}

    <link href="{{asset('Back-Assets/assets/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" />
   <link rel="stylesheet" href="{{asset('Back-Assets/css/custom.css')}}" type="text/css" media="screen">

@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Manage Gallery</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Content</a></li>
                <li class="breadcrumb-item active">Manage Gallery</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">

                        <div class="col-sm-9" style="border-right: 0.5px dotted lightgrey">
                            @include('layouts.frontend.inc.response')
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                            <blockquote><b>Manage Gallery Items</b></blockquote>
                            <div class="row">
                                <div class="col-sm-4">
                                    <a href="{{route('admin.show.create-gallery')}}" type="button" class="btn btn-block btn-outline-info">Add New Image</a>
                                </div>
                            </div>
                            <hr>
                            <table class="table table-striped" style="vertical-align: center">
                                <thead>
                                <tr align="center">
                                    <th>ID #</th>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Status</th>
                                    <th class="text-nowrap">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($galleries as $row)
                                    <tr>
                                        <td>{{$row->id}}</td>
                                        <td>
                                            <img width="50px" src="/uploads/gallery/{{$row->image}}" alt="HVSIA Gallery" />
                                        </td>
                                        <td>
                                            {{$row->title}}
                                        </td>
                                        <td>
                                            @if($row->status == true)

                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <span class="label label-success">Active</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <form action="{{route('admin.post.publish-gallery',['id'=>$row->id])}}" method="post" >
                                                            {{csrf_field()}}
                                                            <input type="hidden" value="off" name="status">
                                                            <button  class="btn btn-danger btn-xs">Inactivate</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <span class="label label-danger">Inactive</span>
                                                    </div>
                                                    <div class="col-md-8">

                                                        <form action="{{route('admin.post.publish-gallery',['id'=>$row->id])}}" method="post" enctype="multipart/form-data">
                                                            {{csrf_field()}}
                                                            <input type="hidden" value="on" name="status"></button>
                                                            <button  class="btn btn-success btn-xs">Activate</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            @endif
                                        </td>
                                        <td class="text-nowrap">
                                            <a href="{{route('admin.show.edit-gallery',['id' => $row->id])}}" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                            <a href="#modal-dialog"  data-id = "{{$row->id}}"  data-toggle="modal" style="width:100%" title="Delete"  class="delete-gallery"> <i class="fa fa-close text-danger"></i> </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                            <div class="col-sm-3">
                                <blockquote>Manage Categories</blockquote>
                                <div class="row">
                                    <div class="col-sm-8">
                                        <button type="button" class="btn btn-block btn-outline-success" data-toggle="modal" data-target="#myModal">Create Category</button>
                                    </div>
                                </div>
                                <hr>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th class="text-nowrap">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($categories as $cat)
                                        <tr>
                                            <td>{{$cat->id}}</td>

                                            <td>{{$cat->name}}</td>
                                            <td class="text-nowrap">
                                                <a href="#edit-category-dialog"  data-id="{{$cat->id}}" data-name="{{$cat->name}}" data-toggle="modal" style="width:100%" title="Edit"   class="edit-category"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                                <a href="#delete-category-dialog"  data-id = "{{$cat->id}}"  data-toggle="modal" style="width:100%" title="Delete"  class="delete-category"> <i class="fa fa-close text-danger"></i> </a>
                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>



        </div>
        <!-- end row -->

        <!--====================================== #modal-dialog ========================================-->
        <div class="modal fade" id="delete-gallery-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Gallery</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <p>
                            Are you sure you want to delete this Gallery
                        </p>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="btn btn-white"  data-dismiss="modal">Close</a>
                        <form action="{{route('admin.delete.gallery')}}" method="post">
                            {{csrf_field()}}
                            <input type="hidden"  name="id" id="gallery-id" value="1">
                            <input type="submit" class="btn btn-danger" id="delete-gallery" value="Delete">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #modal-without-animation -->


        <!--====================================== #modal-Create Cata ========================================-->
        <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Create a Category</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form id="create-category-form" method="post" action="{{route('admin.post.category')}}">
                            @csrf
                            <div class="form-group">
                                <label class="control-label">Category Name</label>
                                <input type="text" id="firstName" name="name" class="form-control" placeholder="Design">
                                <small class="form-control-feedback"> Enter new Category Name </small>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success waves-effect" id="create-category" data-dismiss="modal">Create</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <!--====================================== #modal-edit Cata ========================================-->
        <div id="edit-category-modal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Edit a Category</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form id="edit-category-form" method="post" action="{{route('admin.update.category')}}">
                            @csrf
                            <div class="form-group">
                                <label class="control-label">Category Name</label>
                                <input type="text" id="edit-name" name="name" class="form-control" >
                                <input type="hidden" id="edit-category-id" name="id" >
                                <small class="form-control-feedback"> Enter new Category Name </small>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success waves-effect" id="edit-category" data-dismiss="modal">Update</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <!--====================================== #modal-dialog ========================================-->
        <div class="modal fade" id="delete-category-dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Category</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <p>
                            Are you sure you want to delete this Category
                        </p>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="btn btn-white"  data-dismiss="modal">Close</a>
                        <form action="{{route('admin.delete.category')}}" method="post">
                            {{csrf_field()}}
                            <input type="hidden"  name="id" id="category-id" value="1">
                            <input type="submit" class="btn btn-danger" id="delete" value="Delete">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #modal-without-animation -->

@endsection

@section('script')
    {{--@include('layouts.admin_script.faq_script')--}}

    <script>

        $('#create-category').on('click', function () {

            $('#create-category-form').submit();
        });

        $('.edit-category').on('click', function () {
            var category_id = $(this).attr('data-id');
            var category_name = $(this).attr('data-name');

            $('#edit-category-id').val(category_id);
            $('#edit-name').val(category_name);
            $('#edit-category-modal').modal('show');
        });

        $('#edit-category').on('click', function () {

            $('#edit-category-form').submit();
        });

        $('.delete-category').on('click', function () {
            var category_id = $(this).attr('data-id');
            $('#category-id').val(category_id);
            $('#delete-category-dialog').modal('show');

        });

        $('.delete-gallery').on('click', function () {
            var gallery_id = $(this).attr('data-id');
            $('#gallery-id').val(gallery_id);
            $('#delete-gallery-modal').modal('show');

        });

    </script>

@endsection
