@extends('layouts.backend.main')

@section('css')
    {{-- @include('layouts.admin_css.faq_css')--}}

    <link href="{{asset('Back-Assets/assets/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('Back-Assets/css/custom.css')}}" type="text/css" media="screen">

@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Manage Gallery</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Content</a></li>
                <li class="breadcrumb-item active">Manage Gallery</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <!-- begin panel -->
                <div class="card" data-sortable-id="tree-view-4">
                    <div class="card-body">

                        <h4> Fill in the form </h4>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="card-body">
                        <form action="{{route('admin.update.gallery')}}" method="post">
                            @csrf
                            <input type="hidden"  name="id"  value="{{$gallery->id}}" required >
                            <div class="form-group">
                                <label class="control-label">Gallery Title <span class="text-danger">*</span></label>
                                <input type="text" id="title" name="title" class="form-control" value="{{$gallery->title}}" required >
                            </div>
                            <div class="form-group">
                                <label class="control-label">Gallery Text <span class="text-danger">*</span></label>
                                <textarea type="text" id="firstName" name="content" class="form-control" required>{{$gallery->content}}</textarea>
                            </div>
                            <div class="form-group">
                                <label class="control-label">External Link</label>
                                <input type="text" id="external_link" name="external_link" class="form-control" value="{{$gallery->external_link}}" >
                            </div>
                            <hr>
                            <button type="submit" class="btn waves-effect btn-block waves-light btn-rounded btn-info pull-right"> Save</button>
                            <br/>
                        </form>

                    </div>

                    <div class="card-body">
                        <table id="data-table-combine" class="table table-striped table-bordered">
                            <thead>

                            <tr>
                                <th class="text-nowrap">Field</th>
                                <th class="text-nowrap">Image</th>
                                <th class="text-nowrap">Action</th>
                            </tr>

                            </thead>
                            <tbody>
                            <tr>
                                <td>Image</td>
                                <td width="1%" class="with-img">
                                    <img src="/uploads/gallery/{{$gallery->image}}" width="180" height="80" />
                                </td>
                                <td>
                                    <form enctype="multipart/form-data" id="form-edit-electronic-image" action="{{route('admin.update.gallery-image')}}" method="POST">
                                        <input type="hidden" name="id" value="{{$gallery->id}}">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <input type="file" name="image" class="form-control" />
                                            <span class="text-danger">
                                                    {{$errors->first('image')}}
                                                </span>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" value="Save" class="btn btn-success">
                                        </div>

                                    </form>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>



                </div>
                <!-- end panel -->
            </div>
            <!-- end col-6 -->
        </div>
        <!-- end row -->


        @endsection

        @section('script')
            {{--@include('layouts.admin_script.faq_script')--}}

            <script src="{{asset('Back-Assets/assets/plugins/jquerytree/jquery.tree.min.js')}}"></script>
            <script src="{{asset('Back-Assets/js/custom.min.js')}}"></script>
            <script src="{{asset('Back-Assets/js/jasny-bootstrap.js')}}"></script>

@endsection
