@extends('layouts.admin')

@section('css')
    @include('layouts.admin_css.home_css')
@endsection

@section('content')

    <!-- begin #content -->
    <div id="content" class="content">
    @include('layouts.alert.response')
    <!-- begin breadcrumb -->

        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header"> <small> <a href="{{route('view_footer')}}" class="btn btn-dark"><i class="fa fa-plus"></i> Create Footer</a> </small></h1>
        <!-- end page-header -->

        <!-- begin row -->
        <div class="row">
            <!-- begin col-2 -->

            <!-- end col-2 -->
            <!-- begin col-10 -->
            <div class="col-lg-12">
                <div class="panel panel-inverse">
                    <!-- begin panel-heading -->
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        </div>
                        <h4 class="panel-title">Manage Footer</h4>
                    </div>
                    <!-- end panel-heading -->
                    <!-- begin panel-body -->
                    <div class="panel-body">
                        <table id="data-table-combine" class="table table-striped table-bordered">
                            <thead>

                            <tr>
                                <th width="1%">Address</th>
                                <th width="1%" data-orderable="false">About</th>
                                <th class="text-nowrap">Extra</th>

                                <th class="text-nowrap">Action</th>
                            </tr>

                            </thead>
                            <tbody>
                            @foreach($content_footer as $content)
                                <tr class="odd gradeX">


                                    <td>  {!! $content->address !!} </td>
                                    <td>
                                         {!! $content->about !!}

                                    </td>
                                    <td>
                                        @if($content->status==0)

                                            <div class="row">
                                                <div class="col-md-6">
                                                    Draft <span style="color:orange"><i class="fa fa-circle"></i></span>
                                                </div>
                                                <div class="col-md-6">
                                                    <form action="{{route('publish_footer',['id'=>$content->id])}}" method="post" enctype="multipart/form-data">
                                                        {{csrf_field()}}
                                                        <input type="hidden" value="1" name="status">
                                                        <button  class="btn btn-success btn-xs">Publish <i class="fa fa-globe"></i></button>
                                                    </form>
                                                </div>
                                            </div>
                                        @else
                                            <div class="row">
                                                <div class="col-md-6">
                                                    Published <span style="color:green"><i class="fa fa-circle"></i></span>
                                                </div>
                                                <div class="col-md-6">
                                                    <form action="{{route('publish_footer',['id'=>$content->id])}}" method="post" enctype="multipart/form-data">
                                                        {{csrf_field()}}
                                                        <input type="hidden" value="0" name="status">
                                                        <button  class="btn btn-danger btn-xs">Unpublish <i class="fa fa-globe"></i></button>
                                                    </form>
                                                </div>
                                            </div>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <a href="{{route('update_footer',['id'=>$content->id])}}" style="width:100%" title="Edit"   class="btn btn-dark btn-xs"><i class="fa fa-pencil-square-o"></i></a>
                                            </div>
                                            <div class="col-md-6">
                                                <a href="#modal-dialog"  data-user-id = "{{$content->id}}"  data-toggle="modal" style="width:100%" title="Remove"  class="btn btn-danger btn-xs trigger-modal"><i class="fa fa-remove"></i></a>

                                            </div>

                                        </div>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- end panel-body -->
                </div>
            </div>
            <!-- end col-10 -->
        </div>
        <!-- end row -->
    </div>
    <!-- end #content -->


    <!--====================================== #modal-dialog ========================================-->
    <div class="modal fade" id="modal-dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Remove Footer</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure you want to remove the Footer
                    </p>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-white" id="delete-user" data-dismiss="modal">Close</a>
                    <form action="{{route('delete_footer')}}" method="post">
                        {{csrf_field()}}
                        <input type="hidden"  name="id" id="admin-id" value="1">
                        <input type="submit" class="btn btn-danger" id="remove" value="Remove">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- #modal-without-animation -->


@endsection

@section('script')
    @include('layouts.admin_script.table_script')
    <script>

        $('.trigger-modal').on('click', function () {
            var user_id = $(this).attr('data-user-id');
            $('#admin-id').val(user_id);
            $('#modal-dialog').modal('show');

        });



    </script>
@endsection