@extends('layouts.backend.preview')

@section('content')
    @if(isset($cms))

        @template('home',$cms->slug)
        @include('frontend.cms_template.home')
    @endif
    @template('cms',$cms->slug)
    @include('frontend.cms_template.cms')
    @endif
    @endif

@endsection
