@extends('layouts.backend.main')

@section('css')
    {{-- @include('layouts.admin_css.faq_css')--}}

    <link href="{{asset('Back-Assets/assets/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('Back-Assets/css/custom.css')}}" type="text/css" media="screen">

@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">{{$competency_area->name}} Overview</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{$focus_area->name}}</a></li>
                <li class="breadcrumb-item active">{{$competency_area->name}}</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="card-group">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="m-b-0"><i class="mdi mdi-briefcase-check text-info"></i></h2>
                            <h3 class="">{{$competency_area->latestTestCompleted() ? $competency_area->latestTestCompleted()->start_date : 'None'}}</h3>
                            <h6 class="card-subtitle">Latest Completed Test</h6></div>

                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="m-b-0"><i class="mdi mdi-alert-circle text-success"></i></h2>
                            <h3 class="">{{$competency_area->latestTestInProgress() ? $competency_area->latestTestInProgress()->start_date : 'None'}}</h3>
                            <h6 class="card-subtitle">Latest test Started</h6></div>

                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="m-b-0"><i class="mdi mdi-wallet text-purple"></i></h2>
                            <h3 class="">{{$competency_area->testsCompleted()->count()}}</h3>
                            <h6 class="card-subtitle">Tests Completed</h6></div>

                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="m-b-0"><i class="fa fa-spinner fa-spin text-warning"></i></h2>
                            <h3 class="">{{$competency_area->testsInProgress()->count()}}</h3>
                            <h6 class="card-subtitle">In Progress</h6></div>

                    </div>
                </div>
            </div>
        </div>
        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-9">
                <div class="card">
                    <div class="card-header">
                        Completed
                    </div>
                    <div class="card-body">
                        <a href="#" class="btn btn-sm btn-outline-info" id="start-test-btn" data-toggle="modal" data-target="#verticalcenter"><i class="fa fa-plus"></i> Start A New Test </a>
                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Done By</th>
                                <th><i class="fa  fa-clock-o"></i> Started Date</th>
                                <th><i class="fa fa-check"></i> Completed Date</th>
                                <th width="10%"></th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($competency_area->tests()->where('completed',true)->get() as $row)
                                <tr>
                                    <td>{{str_limit($row->location->location,20)}}</td>
                                    <td>{{$row->start_date}}</td>
                                    <td>{{$row->completed_date}}</td>
                                    <th>
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            {{--<a type="button" class="btn btn-secondary" data-toggle="modal" data-target="#ViewDocuements"><i class="fa fa-download"></i> Documents</a>--}}
                                            <a href="{{route('admin.tests.show',['id' => $row->id])}}" data-toggle="tooltip" data-original-title="View Test"> <i class="fa fa-eye text-inverse m-r-10"></i> </a>

                                        </div>
                                    </th>

                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="card">
                    <div class="card-header">
                        Testing In Progress
                    </div>
                    <div class="card-body" style="padding: 0px!important;">
                        <div class="table-responsive table-striped">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>In Progress By</th>
                                    <th>Started Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($competency_area->testsInProgress() as $row)
                                    <tr>
                                        <td>{{$row->location->country}}</td>
                                        <td>{{$row->start_date}}</td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end col-6 -->
        </div>
        <!-- end row -->
    </div>


@endsection

@section('script')
    {{--@include('layouts.admin_script.faq_script')--}}

    <script src="{{asset('Back-Assets/assets/plugins/jquerytree/jquery.tree.min.js')}}"></script>
    <script src="{{asset('Back-Assets/js/custom.min.js')}}"></script>
    <script src="{{asset('Back-Assets/js/jasny-bootstrap.js')}}"></script>

    <!-- This is data table -->
    <script src="{{asset('Back-Assets/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#myTable').DataTable();
            $(document).ready(function() {
                var table = $('#example').DataTable({
                    "columnDefs": [{
                        "visible": false,
                        "targets": 2
                    }],
                    "order": [
                        [2, 'asc']
                    ],
                    "displayLength": 25,
                    "drawCallback": function(settings) {
                        var api = this.api();
                        var rows = api.rows({
                            page: 'current'
                        }).nodes();
                        var last = null;
                        api.column(2, {
                            page: 'current'
                        }).data().each(function(group, i) {
                            if (last !== group) {
                                $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                                last = group;
                            }
                        });
                    }
                });
                // Order by the grouping
                $('#example tbody').on('click', 'tr.group', function() {
                    var currentOrder = table.order()[0];
                    if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                        table.order([2, 'desc']).draw();
                    } else {
                        table.order([2, 'asc']).draw();
                    }
                });
            });
        });
        $('#example23').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('#start-test-btn').on('click',function(){
            window.location = "{{route('admin.tests.create')}}";
        });

    </script>
@endsection

