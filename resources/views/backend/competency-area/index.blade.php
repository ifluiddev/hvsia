@extends('layouts.backend.main')

@section('css')
   {{-- @include('layouts.competency_area_css.faq_css')--}}

    <link href="{{asset('Back-Assets/assets/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" />
   <link rel="stylesheet" href="{{asset('Back-Assets/css/custom.css')}}" type="text/css" media="screen">

@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Manage Competency Area</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Content</a></li>
                <li class="breadcrumb-item active">Manage Competency Area</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Competency Areas</h4>
                        <h6 class="card-subtitle pull-right"><a href="{{route('admin.competency-areas.create')}}" type="button" class="btn waves-effect waves-light btn-sm btn-info"><i class="fa fa-plus"></i> Create New Competency Area</a></h6>
                        @include('layouts.frontend.inc.response')
                        <div class="table-responsive">
                            <table class="table table-striped ">
                                <thead>
                                <tr>
                                    <th>ID #</th>
                                    <th>Name</th>
                                    <th class="text-nowrap">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($competency_areas as $row)
                                    <tr>
                                        <td>{{$row->id}}</td>
                                        <td>
                                            {{$row->name}}
                                        </td>
                                        <td class="text-nowrap">
                                            <a href="{{route('admin.competency-areas.edit',['id' => $row->id])}}" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                            <a href="#modal-dialog"  data-id = "{{$row->id}}"  data-toggle="modal" style="width:100%" title="Delete"  id="delete-competency-area" class="trigger-modal"> <i class="fa fa-close text-danger"></i> </a>
                                        </td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>



        </div>
        <!-- end row -->

        <!--====================================== #modal-dialog ========================================-->
        <div class="modal fade" id="modal-dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Competency Area</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <p>
                            Are you sure you want to delete this Competency Area
                        </p>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="btn btn-white"  data-dismiss="modal">Close</a>
                        <form id="delete-form" action="" method="post">
                            {{csrf_field()}}
                            @method('DELETE')
                            <input type="hidden"  name="id" id="competency-area-id" value="1">
                            <input type="submit" class="btn btn-danger" id="delete" value="Delete">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #modal-without-animation -->


@endsection

@section('script')
    {{--@include('layouts.competency_area_script.faq_script')--}}

    <script>

        $('.trigger-modal').on('click', function () {
            var id = $(this).attr('data-id');
            $('#competency-area-id').val(id);

            //set form action
            $('#delete-form').attr('action',"{{url('/admin/competency-areas')}}/"+id)
            $('#modal-dialog').modal('show');

        });

    </script>

@endsection
