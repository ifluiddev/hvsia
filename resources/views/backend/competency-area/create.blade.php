@extends('layouts.backend.main')

@section('css')
    {{-- @include('layouts.admin_css.faq_css')--}}

    <link href="{{asset('Back-Assets/assets/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('Back-Assets/css/custom.css')}}" type="text/css" media="screen">

@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Manage Competency Area</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Content</a></li>
                <li class="breadcrumb-item active">Manage Competency Area</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <!-- begin panel -->
                <div class="card" data-sortable-id="tree-view-4">
                    <div class="card-body">

                        <h4> Fill in the form </h4>
                        {{--@if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif--}}
                    </div>
                    <div class="card-body">
                        <form action="{{route('admin.competency-areas.store')}}" method="post" >
                            @csrf
                            <div class="form-group">
                                <label class="control-label">Name<span class="text-danger">*</span></label>
                                <input type="text" id="name" name="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" required value="{{old('name')}}">
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                              <strong>{{ $errors->first('name') }} </strong>
                                            </span>
                                @endif
                            </div>
                            <button type="submit" class="btn waves-effect btn-block waves-light btn-rounded btn-info pull-right"><i class="fa fa-plus"></i> Create New Competency Area</button>
                            <br/>
                        </form>

                    </div>



                </div>
                <!-- end panel -->
            </div>
            <!-- end col-6 -->
        </div>
        <!-- end row -->


        @endsection

        @section('script')
            {{--@include('layouts.admin_script.faq_script')--}}

            <script src="{{asset('Back-Assets/assets/plugins/jquerytree/jquery.tree.min.js')}}"></script>
            <script src="{{asset('Back-Assets/js/custom.min.js')}}"></script>
            <script src="{{asset('Back-Assets/js/jasny-bootstrap.js')}}"></script>

@endsection
