@extends('layouts.backend.main')

@section('css')
    <!--
    PLEASE NOTE THAT THIS PAGE IS COMPLEX, LOTS OF NESTED ITERATIONS AND CONDITIONS
    BAD PRACTICE BUT GETS THE JOB DONE. CODE ON THIS PAGE CAN BE IMPROVED.

    I TRIED AS MUCH AS POSSIBLE TO COMMENT EVERY LOOP. ENJOYYYYY!!!!!

-->
@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Matrix Table</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Matrix</a></li>
                <li class="breadcrumb-item active">Table</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <!-- begin panel -->
                <div class="card" data-sortable-id="tree-view-4">
                    {{--<div class="card-body">
                        <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>

                                <th class="text-nowrap"></th>
                                <th class="text-nowrap">Structural Design</th>
                                <th class="text-nowrap">Materials</th>
                                <th class="text-nowrap">Functional Performance</th>
                                <th class="text-nowrap">Structural Performance</th>
                                <th class="text-nowrap">Rehabilitation Maintenance</th>
                                <th class="text-nowrap">Construction</th>
                                <th class="text-nowrap">Information Systems</th>
                                <th class="text-nowrap">Vehicle Pavement-Interaction</th>
                                <th class="text-nowrap">Instrumentation</th>
                                <th class="text-nowrap">Environ. Impact Assess</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="text-nowrap">Asphalt </td>
                                <td></td>
                                <td></td>
                                <td class="bg-success" align="center"> <i class="fa fa-check"></i> </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>
                            <tr>
                                <td class="text-nowrap">Cemented layers</td>
                                <td></td>
                                <td></td>
                                <td class="bg-success" align="center"> <i class="fa fa-check"></i> </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>
                            <tr>
                                <td class="text-nowrap">Concrete</td>
                                <td></td>
                                <td class="bg-success" align="center"> <i class="fa fa-check"></i> </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="bg-success" align="center"> <i class="fa fa-check"></i> </td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>
                            <tr>
                                <td  class="text-nowrap">Granular</td>
                                <td></td>
                                <td></td>
                                <td class="bg-success" align="center"> <i class="fa fa-check"></i> </td>
                                <td></td>
                                <td></td>
                                <td class="bg-success" align="center"> <i class="fa fa-check"></i> </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>

                            </tbody>
                        </table>
                        </div>
                    </div>
--}}

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th class="text-nowrap"></th>
                                    <!--Loop through competency area and display-->
                                    @foreach($competency_areas as $row)
                                        <th class="text-nowrap">{{$row->name}}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                <!--Loop through focus area and display-->
                                @foreach($focus_areas as $focus_area)
                                    <tr>
                                        <td class="text-nowrap">{{$focus_area->name}} </td><!--first loop must display focus area name-->
                                        <!--loop through competency area again to get each competency area-->
                                    @foreach($competency_areas as $competency_area)

                                        @if($focus_area->tests()->count() > 0)

                                            @php
                                                $test_array = [];
                                                $duplicate_array = [];
                                            @endphp

                                            <!-- loop tests under each focus area, one competency area can appear once in each test. I need to find a way to eliminate duplicate (Read on to see the logic used for elimination)-->
                                                @foreach($focus_area->tests as $test)
                                                    @php
                                                        //loop test competency areas,pluck only the name and append to test_array
                                                        foreach($test->competency_areas()->pluck('name')->toArray() as $n){
                                                            array_push($test_array,$n);
                                                        }

                                                    @endphp


                                                @endforeach

                                                <!--Now use array_count_values() to count existence of each name-->
                                                @php
                                                    $duplicate_array = array_count_values($test_array);
                                                @endphp


                                             <!--If competency area name exists as a key in duplicate_array AND exists 2 or more times in the duplicate_array,
                                             then check(meaning a test has been done or ongoing under the focus area and competency area)-->
                                                @if(array_key_exists($competency_area->name,$duplicate_array) && $duplicate_array[$competency_area->name] >= 2)
                                                    <td class="bg-success" align="center"> <a href="{{route('admin.competency-areas.show',['id' => $competency_area->id])}}?focus_area_id={{$focus_area->id}}"><i class="fa btn btn-block fa-check"></i></a></td>
                                                    <!--Else If competency area name exists as a key in duplicate_array AND exists once in the duplicate_array,
                                             then check(meaning a test has been done or ongoing under the focus area and competency area)-->
                                                @elseif(array_key_exists($competency_area->name,$duplicate_array) && $duplicate_array[$competency_area->name] == 1 )
                                                    <td class="bg-success" align="center"> <a href="{{route('admin.competency-areas.show',['id' => $competency_area->id])}}?focus_area_id={{$focus_area->id}}"><i class="fa btn btn-block fa-check"></i></a></td>
                                                    <!--Else competency area name  does not exist, therefore don't check -->
                                                @else
                                                    <td>  </td>
                                                @endif

                                            @else
                                                <td>  </td>
                                            @endif

                                    @endforeach

                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>



                </div>
                <!-- end panel -->
            </div>
            <!-- end col-6 -->
        </div>
        <!-- end row -->
    </div>

@endsection

@section('script')
    {{--@include('layouts.admin_script.faq_script')--}}

@endsection

