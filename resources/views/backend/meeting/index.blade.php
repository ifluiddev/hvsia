@extends('layouts.backend.main')

@section('css')
    {{-- @include('layouts.admin_css.faq_css')--}}

    <link href="{{asset('Back-Assets/assets/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('Back-Assets/css/custom.css')}}" type="text/css" media="screen">

@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Manage Meetings</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Meetings</a></li>
                <li class="breadcrumb-item active">Manage</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
    @include('layouts.frontend.inc.response')
        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-6" >
                <!-- begin panel -->
                <div class="card" data-sortable-id="tree-view-4">
                    <div class="card-body" style="padding: 0px!important;">
                        <ul class="nav nav-tabs customtab" role="tablist">
                            <li class="nav-item"> <a class="nav-link active show" data-toggle="tab" href="#home2" role="tab" aria-selected="true"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Upcoming Meetings</span></a> </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active show" id="home2" role="tabpanel">
                                <div class="p-20" style="margin-top: 5%">
                                    <div class="table-responsive">
                                        <table  class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Date</th>
                                                <th>Time</th>
                                                <th class="text-nowrap">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($upcoming_meetings as $row)
                                                <tr>
                                                    <td>{{str_limit($row->title,10)}}</td>
                                                    <td>{{$row->date}}</td>
                                                    <td>{{$row->time_start}}</td>
                                                    <td class="text-nowrap">
                                                        <a href="{{route('admin.meetings.edit',['id' => $row->id])}}" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                                        <a href="#modal-dialog"  data-id = "{{$row->id}}"  data-toggle="modal" style="width:100%" title="Delete"  id="delete-admin" class="trigger-modal"> <i class="fa fa-close text-danger"></i> </a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>



                </div>
                <!-- end panel -->
            </div >
            <div class="col-md-6">
                <!-- begin panel -->
                <div class="card" data-sortable-id="tree-view-4">
                    <div class="card-body"  style="padding: 0px!important">
                        <ul class="nav nav-tabs customtab" role="tablist">
                            <li class="nav-item"> <a class="nav-link active show" data-toggle="tab" href="#home4" role="tab" aria-selected="true"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Concluded Meetings</span></a> </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane active show" id="home4" role="tabpanel">
                                <div class="p-20" style="margin-top: 5%" >
                                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Concluded Date</th>

                                            <th width="6%"></th>

                                        </tr>
                                        </thead>
                                        <tbody>

                                            @foreach($past_meetings as $row)
                                                <tr>
                                                    <td>{{str_limit($row->title,10)}}</td>
                                                    <td>{{$row->date}}</td>

                                                    <td>
                                                        <div class="btn-group" role="group" aria-label="Basic example">
                                                            <a href="{{route('admin.meetings.show',['id' => $row->id])}}" type="button" class="btn btn-secondary">View</a>
                                                            <a href="{{route('admin.meetings.edit',['id' => $row->id])}}" data-toggle="tooltip" data-original-title="Edit" class="btn btn-outline-success"><i class="fa fa-pencil text-inverse"></i> </a>
                                                            <a href="#modal-dialog"  data-id = "{{$row->id}}"  data-toggle="modal"  data-original-title="Delete" style="width:100%" title="Delete"  id="delete-admin" class="trigger-modal btn btn-outline-danger"> <i class="fa fa-close text-danger"></i> </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>



                </div>
                <!-- end panel -->
            </div>
            <!-- end col-6 -->
        </div>
        <!-- end row -->
    </div>

    <!--====================================== #modal-dialog ========================================-->
    <div class="modal fade" id="modal-dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Delete Meeting</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure you want to delete this Meeting
                    </p>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-white"  data-dismiss="modal">Close</a>
                    <form action="{{route('admin.meetings.destroy')}}" method="post">
                        {{csrf_field()}}
                        @method('DELETE')
                        <input type="hidden"  name="id" id="admin-id" value="1">
                        <input type="submit" class="btn btn-danger" id="delete" value="Delete">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- #modal-without-animation -->

@endsection

@section('script')
    {{--@include('layouts.admin_script.faq_script')--}}

    <script src="{{asset('Back-Assets/assets/plugins/jquerytree/jquery.tree.min.js')}}"></script>
    <script src="{{asset('Back-Assets/js/custom.min.js')}}"></script>
    <script src="{{asset('Back-Assets/js/jasny-bootstrap.js')}}"></script>

    <script src="{{asset('Back-Assets/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#myTable').DataTable();
            $(document).ready(function() {
                var table = $('#example').DataTable({
                    "columnDefs": [{
                        "visible": false,
                        "targets": 2
                    }],
                    "order": [
                        [2, 'asc']
                    ],
                    "displayLength": 25,
                    "drawCallback": function(settings) {
                        var api = this.api();
                        var rows = api.rows({
                            page: 'current'
                        }).nodes();
                        var last = null;
                        api.column(2, {
                            page: 'current'
                        }).data().each(function(group, i) {
                            if (last !== group) {
                                $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                                last = group;
                            }
                        });
                    }
                });
                // Order by the grouping
                $('#example tbody').on('click', 'tr.group', function() {
                    var currentOrder = table.order()[0];
                    if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                        table.order([2, 'desc']).draw();
                    } else {
                        table.order([2, 'asc']).draw();
                    }
                });
            });
        });
        $('#example23').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
        $('#example233').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $('.trigger-modal').on('click', function () {
            var id = $(this).attr('data-id');
            $('#admin-id').val(id);
            $('#modal-dialog').modal('show');

        });
    </script>
@endsection

