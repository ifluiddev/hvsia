@extends('layouts.backend.main')

@section('css')
    {{-- @include('layouts.admin_css.faq_css')--}}

    <link href="{{asset('Back-Assets/assets/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('Back-Assets/css/custom.css')}}" type="text/css" media="screen">

    <!-- Date picker plugins css -->
    <link href="{{asset('Back-Assets/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('Back-Assets/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet">
    <!-- Daterange picker plugins css -->
    <link href="{{asset('Back-Assets/assets/plugins/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('Back-Assets/assets/plugins/clockpicker/dist/jquery-clockpicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('Back-Assets/assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
    <!-- editor -->
    <link rel="stylesheet" href="{{asset('Back-Assets/assets/plugins/html5-editor/bootstrap-wysihtml5.css')}}" />
    <!-- Dropzone css -->
    <link href="{{asset('Back-Assets/assets/plugins/dropzone-master/dist/dropzone.css')}}" rel="stylesheet" type="text/css" />

@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Create Meeting</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.meetings.index')}}">Meetings</a></li>
                <li class="breadcrumb-item active">Create New</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <!-- begin panel -->
                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="{{route('admin.meetings.store')}}" enctype="multipart/form-data">
                            @if($errors->any())
                                @foreach ($errors->all() as $error)
                                    <div class="text-danger">{{ $error }}</div>
                                @endforeach
                            @endif
                            @csrf
                            <div class="row">
                                <div class="col-12">

                                    <div class="form-group">
                                        <label>Meeting Title*</label>
                                        <input type="text" name="title" class="form-control form-control-line {{ $errors->has('title') ? ' is-invalid' : '' }}" placeholder="Meeting Title" style="cursor: auto;" required value="{{old('title')}}">
                                        @if ($errors->has('title'))
                                            <span class="invalid-feedback">
                                              <strong>{{ $errors->first('title') }} </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Venue*</label>
                                        <input type="text" name="venue" class="form-control form-control-line {{ $errors->has('venue') ? ' is-invalid' : '' }}" placeholder="Meeting Venue" style="cursor: auto;" required value="{{old('venue')}}">
                                        @if ($errors->has('venue'))
                                            <span class="invalid-feedback">
                                              <strong>{{ $errors->first('venue') }} </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="example">
                                        <div class="form-group">
                                            <label>Start Date*</label>
                                            <div class="input-group">
                                                <input type="text" name="start_date" class="form-control {{ $errors->has('start_date') ? ' is-invalid' : '' }}" id="datepicker-start" placeholder="yyyy-mm-dd" value="{{old('start_date')}}" required>
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="icon-calender"></i></span>
                                                </div>

                                            </div>
                                            @if ($errors->has('start_date'))
                                                <span class="invalid-feedback">
                                              <strong>{{ $errors->first('start_date') }} </strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <label>Start Time</label>
                                    <div class="input-group clockpicker " data-placement="bottom" data-align="top" data-autoclose="true">
                                        <input type="text" name="start_time" class="form-control {{ $errors->has('start_time') ? ' is-invalid' : '' }}" placeholder="13:14" value="{{old('start_time')}}" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                        @if ($errors->has('start_time'))
                                            <span class="invalid-feedback">
                                              <strong>{{ $errors->first('start_time') }} </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="example">
                                        <div class="form-group">
                                            <label>End Date*</label>
                                            <div class="input-group">
                                                <input type="text" name="end_date" class="form-control {{ $errors->has('end_date') ? ' is-invalid' : '' }}" id="datepicker-end" placeholder="yyyy-mm-dd" value="{{old('end_date')}}" required>
                                                <div class="input-group-append">
                                                    <span class="input-group-text"><i class="icon-calender"></i></span>
                                                </div>

                                            </div>
                                            @if ($errors->has('end_date'))
                                                <span class="invalid-feedback">
                                              <strong>{{ $errors->first('end_date') }} </strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <label> End Time</label>
                                    <div class="input-group clockpicker " data-placement="bottom" data-align="top" data-autoclose="true">
                                        <input type="text" name="end_time" class="form-control {{ $errors->has('end_time') ? ' is-invalid' : '' }}" placeholder="13:14" value="{{old('end_time')}}" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                                        </div>
                                        @if ($errors->has('end_time'))
                                            <span class="invalid-feedback">
                                              <strong>{{ $errors->first('end_time') }} </strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-12">
                                    <label>Description</label>
                                    <textarea id="mymce" name="description">{{old('description')}}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="invalid-feedback">
                                              <strong>{{ $errors->first('description') }} </strong>
                                            </span>
                                    @endif
                                </div>
                                <div class="col-md-6" style="border-right: 1px silver solid">
                                    <div class="form-group" style="margin-top: 3%;">
                                        <label>File Upload</label>
                                        <input name="agenda" class="form-control" type="file" required  />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body" align="center">
                                            <button class="btn btn-block btn-info"><i class="fa fa-calendar"></i> Create Meeting</button>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-body" align="center">
                                            <a href="{{route('admin.meetings.index')}}" class="btn btn-block btn-danger">Cancel</a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
                <!-- end panel -->
            </div>

            <!-- end col-6 -->
        </div>
        <!-- end row -->
    </div>

@endsection

@section('script')
    {{--@include('layouts.admin_script.faq_script')--}}

    <script src="{{asset('Back-Assets/assets/plugins/jquerytree/jquery.tree.min.js')}}"></script>
    <script src="{{asset('Back-Assets/js/custom.min.js')}}"></script>
    <script src="{{asset('Back-Assets/js/jasny-bootstrap.js')}}"></script>

    <!-- Clock Plugin JavaScript -->
    <script src="{{asset('Back-Assets/assets/plugins/clockpicker/dist/jquery-clockpicker.min.js')}}"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="{{asset('Back-Assets/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="{{asset('Back-Assets/assets/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
    <script src="{{asset('Back-Assets/assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('Back-Assets/assets/plugins/moment/moment.js')}}"></script>
    <script src="{{asset('Back-Assets/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
    <!-- Dropzone Plugin JavaScript -->
    <script src="{{asset('Back-Assets/assets/plugins/dropzone-master/dist/dropzone.js')}}"></script>

    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{asset('Back-Assets/assets/plugins/tinymce/tinymce.min.js')}}"></script>
    <script>
        $(document).ready(function() {

            if ($("#mymce").length > 0) {
                tinymce.init({
                    selector: "textarea#mymce",
                    theme: "modern",
                    height: 300,
                    plugins: [
                        "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                        "save table contextmenu directionality emoticons template paste textcolor"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",

                });
            }
        });
    </script>

    <script>
        // MAterial Date picker
        $('#mdate').bootstrapMaterialDatePicker({ weekStart: 0, time: false });
        $('#timepicker').bootstrapMaterialDatePicker({ format: 'HH:mm', time: true, date: false });
        $('#date-format').bootstrapMaterialDatePicker({ format: 'dddd DD MMMM YYYY - HH:mm' });

        $('#min-date').bootstrapMaterialDatePicker({ format: 'DD/MM/YYYY HH:mm', minDate: new Date() });
        // Clock pickers
        $('#single-input').clockpicker({
            placement: 'bottom',
            align: 'left',
            autoclose: true,
            'default': 'now'
        });
        $('.clockpicker').clockpicker({
            donetext: 'Done',
        }).find('input').change(function() {
            console.log(this.value);
        });
        $('#check-minutes').click(function(e) {
            // Have to stop propagation here
            e.stopPropagation();
            input.clockpicker('show').clockpicker('toggleView', 'minutes');
        });
        if (/mobile/i.test(navigator.userAgent)) {
            $('input').prop('readOnly', true);
        }

        // Date Picker
        jQuery('#datepicker-start').datepicker({
            autoclose: true,
            todayHighlight: true,
            format : 'yyyy-mm-dd'
        });
        jQuery('#datepicker-end').datepicker({
            autoclose: true,
            todayHighlight: true,
            format : 'yyyy-mm-dd'
        });

        // Daterange picker

    </script>


@endsection

