@extends('layouts.backend.main')

@section('css')
    {{-- @include('layouts.admin_css.faq_css')--}}

    <link href="{{asset('Back-Assets/assets/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('Back-Assets/css/custom.css')}}" type="text/css" media="screen">
    <link href="{{asset('Back-Assets/assets/plugins/dropzone-master/dist/dropzone.css')}}" rel="stylesheet" type="text/css" />

@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Meeting Overview</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('admin.meetings.index')}}">Meetings</a></li>
                <li class="breadcrumb-item active">Meeting Overview</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-4">
                <!-- begin panel -->
                <div class="card" data-sortable-id="tree-view-4">
                    <div class="card-body">
                        <h2>Meeting Details</h2>
                        <hr />
                        <div class="row">
                            <div class="col-4">
                                <b>Title:</b>
                            </div>
                            <div class="col-8">
                                {{$meeting->title}}
                            </div>
                            <div class="col-4">
                                <b> Date:</b>
                            </div>
                            <div class="col-8">
                                {{$meeting->date}}
                            </div>
                            <div class="col-4">
                                <b>Venue:</b>
                            </div>
                            <div class="col-8">
                                <b> {{$meeting->venue}}</b>
                            </div>
                        </div>
                    </div>



                </div>
                <!-- end panel -->
            </div>
            <!-- end col-4 -->
            <div class="col-md-8">
                <!-- begin panel -->
                <div class="card" data-sortable-id="tree-view-4">
                    <div class="card-body">
                        <ul class="nav nav-tabs customtab" role="tablist">
                            <li class="nav-item"> <a class="nav-link show active" data-toggle="tab" href="#home2" role="tab" aria-selected="true"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Description</span></a> </li>
                            <li class="nav-item"> <a class="nav-link show" data-toggle="tab" href="#profile2" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Agenda</span></a> </li>
                            <li class="nav-item"> <a class="nav-link show" data-toggle="tab" href="#messages2" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Minutes</span></a> </li>
                            <li class="nav-item"> <a class="nav-link show" data-toggle="tab" href="#messages4" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span> <span class="hidden-xs-down">Presentation</span></a> </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane show active" id="home2" role="tabpanel">
                                <div class="p-20">
                                    {!! $meeting->description !!}
                                </div>
                            </div>
                            <div class="tab-pane p-20 show" id="profile2" role="tabpanel">
                                <table class="table table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Date Uploaded</th>
                                        <th width="6%">Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>{{$meeting->created_at}}</td>
                                        <td><a target="_blank" href="{{'/uploads'.config('filesystems.storage.meeting_agenda_path').$meeting->agenda}}">
                                                <i class="fa fa-download fa-2x" aria-hidden="true"></i>
                                            </a></td>
                                    </tr>
                                    </tbody>
                                </table>

                            </div>
                            <div class="tab-pane p-20 show" id="messages2" role="tabpanel">
                                @if($meeting->minute)

                                    <table class="table table-hover table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Date Uploaded</th>
                                            <th width="6%">Action</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{$meeting->updated_at}}</td>
                                            <td>
                                                <a target="_blank" href="{{'/uploads'.config('filesystems.storage.meeting_minute_path').$meeting->minute}}">
                                                    <i class="fa fa-download fa-2x" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    @else
                                    <form action="{{route('admin.meeting.minutes.update',['id' => $meeting->id])}}"  enctype="multipart/form-data" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <div class="form-group">
                                            <input name="minute" type="file" required/>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-block btn-info"><i class="fa fa-calendar"></i> Upload</button>
                                        </div>
                                    </form>
                                @endif
                            </div>
                            <div class="tab-pane p-20 show" id="messages4" role="tabpanel">
                                @if($meeting->presentations->count() > 0)

                                    <table class="table table-hover table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Author</th>
                                            <th>Date Uploaded</th>
                                            <th width="6%">Action</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($meeting->presentations as $row)
                                            <tr>
                                                <td>{{$row->title}}</td>
                                                <td>{{$row->author}}</td>
                                                <td>{{$row->created_at}}</td>
                                                <td>
                                                    <a target="_blank" href="{{'/uploads'.config('filesystems.storage.meeting_presentation_path').$row->file_name}}">
                                                        <i class="fa fa-download fa-2x" aria-hidden="true"></i>
                                                    </a>
                                                    <a href="#delete-presentation-dialog"  data-id = "{{$row->id}}"  data-toggle="modal" style="width:100%" title="Delete"  class="delete-presentation"> <i class="fa fa-close fa-2x text-danger"></i> </a>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                    <div class="form-group">
                                        <a href="#modal-dialog"  data-toggle="modal" class="btn btn-block btn-info"><i class="fa fa-plus"></i> Upload New Presentation</a>
                                    </div>
                                @else
                                    <div class="form-group">
                                        <a href="#modal-dialog"  data-toggle="modal" class="btn btn-block btn-info"><i class="fa fa-plus"></i> Upload New Presentation</a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
    </div>

    <!--====================================== #modal-dialog ========================================-->
    <div class="modal fade" id="modal-dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Upload New Presentation</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <form id="presentation-form" action="{{route('admin.meeting.presentations.update',['id' => $meeting->id])}}"  enctype="multipart/form-data" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <input name="title" class="form-control" type="text" placeholder="Enter Presentation Title" required/>
                        </div>
                        <div class="form-group">
                            <input name="author" type="text" class="form-control" placeholder="Enter Author Name" required/>
                        </div>
                        <div class="form-group">
                            <input name="presentation" type="file" class="form-control" required />
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-white"  data-dismiss="modal">Close</a>
                    <a type="submit" class="btn btn-blue" id="upload-presentation">Save</a>
                </div>
            </div>
        </div>
    </div>
    <!-- #modal-without-animation -->

    <!-- MODAL Cancel -->
    <div id="delete-presentation-dialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="vcenter" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title" id="vcenter">DELETE PRESENTATION</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <h4>You are about to delete a presentation</h4>
                    <input type="hidden" id="presentation-id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success waves-effect" data-dismiss="modal" aria-hidden="true">No, Don't</button>
                    <button type="button" class="btn btn-danger waves-effect" id="delete-presentation-btn">Yes, Delete</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('script')
    {{--@include('layouts.admin_script.faq_script')--}}

    <script src="{{asset('Back-Assets/assets/plugins/jquerytree/jquery.tree.min.js')}}"></script>
    <script src="{{asset('Back-Assets/js/custom.min.js')}}"></script>
    <script src="{{asset('Back-Assets/js/jasny-bootstrap.js')}}"></script>
    <script src="{{asset('Back-Assets/assets/plugins/dropzone-master/dist/dropzone.js')}}"></script>
    <script>
        $('#upload-presentation').on('click',function(){
            $('#presentation-form').submit();
        });

        //cancel
        $('.delete-presentation').on('click',function(){
            $('#presentation-id').val($(this).data('id'));
            $('#cancel').modal('show');
        });
        $('#delete-presentation-btn').on('click',function(){

            $.ajax({
                type : 'POST',
                url : "{{url('admin/meeting/presentations')}}/" + $('#presentation-id').val(),
                data : "_token={{csrf_token()}}&_method=DELETE",
                success : function (data) {
                    document.location.reload(true);
                }
            });
        });

    </script>

@endsection

