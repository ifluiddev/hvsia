@extends('layouts.backend.main')

@section('css')
    <!-- Multi Select CSS -->
    <link href="{{asset('Back-Assets/assets/plugins/multiselect/css/multi-select.css')}}" rel="stylesheet" type="text/css" xmlns=""/>
    <!-- Date picker plugins css -->
    <link href="{{asset('Back-Assets/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('Back-Assets/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet">
    <!-- Daterange picker plugins css -->
    <link href="{{asset('Back-Assets/assets/plugins/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('Back-Assets/assets/plugins/clockpicker/dist/jquery-clockpicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('Back-Assets/assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">

@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Create New Test</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Matrix</a></li>
                <li class="breadcrumb-item active">Create Test</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <!-- begin panel -->
                <div class="card" data-sortable-id="tree-view-4">
                    <div class="card-body">
                        <form id="create-test-form" method="POST" action="{{route('admin.tests.store')}}">
                            @csrf
                            <div class="form-group">
                                <label class="control-label">Location <span class="text-danger">*</span></label>
                                <select name="location_id" class="form-control {{ $errors->has('location_id') ? ' is-invalid' : '' }}" required>
                                    <option selected disabled hidden>--- Select Location ---</option>
                                    @foreach($locations as $row)
                                        <option value="{{$row->id}}" {{$row->id==old('location_id')?'selected' : ''}}>{{$row->location}}</option>
                                    @endforeach

                                </select>
                                @if ($errors->has('location_id'))
                                    <span class="invalid-feedback">
                                              <strong>{{ $errors->first('location_id') }} </strong>
                                            </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">Select Focus Area <span class="text-danger">*</span></label>
                                <select name="focus_area_id" id="public_methods" class="form-control {{ $errors->has('focus_area_id') ? ' is-invalid' : '' }}" required>
                                    <option selected disabled hidden>--- Select Focus Area ---</option>
                                    @foreach($focus_areas as $row)
                                        <option value="{{$row->id}}" {{$row->id==old('focus_area_id')?'selected' : ''}}>{{$row->name}}</option>
                                    @endforeach

                                </select>
                                @if ($errors->has('focus_area_id'))
                                    <span class="invalid-feedback">
                                              <strong>{{ $errors->first('focus_area_id') }} </strong>
                                            </span>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-xlg-12">
                                    <h5 class="box-title">Select your Competencies</h5>
                                    @if ($errors->has('competency_area_id'))
                                        <span class="invalid-feedback">
                                              <strong>{{ $errors->first('competency_area_id') }} </strong>
                                            </span>
                                    @endif
                                    <select id="public-methods" name="competency_area_id[]"   multiple required>
                                        <option selected disabled hidden>--- Select Focus Area ---</option>
                                        @foreach($competency_areas as $row)
                                            <option value="{{$row->id}}" {{$row->id==old('competency_area_id')?'selected' : ''}}>{{$row->name}}</option>
                                        @endforeach

                                    </select>

                                    <div class="button-box m-t-20">
                                        <a id="select-all" class="btn btn-danger" href="#">select all</a>
                                        <a id="deselect-all" class="btn btn-info" href="#">deselect all</a>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-xlg-12">

                                </div>
                            </div>
                            <hr>
                            <blockquote class="blockquote bg-warning">Only fill out the following section if you are back dating a old test else ignore and create</blockquote>
                            <div class="row">
                                <div class="col-lg-6 col-xlg-6">
                                    <label>Select a Started Date</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="start_date" id="datepicker-autoclose" placeholder="mm/dd/yyyy">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="icon-calender"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-xlg-6">
                                    <label>Select a Completed Date</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="completed_date" id="datepicker-autoclose2" placeholder="mm/dd/yyyy">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="icon-calender"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                    <div class="card-footer">
                        <button type="button" id="create-test-btn" class="btn waves-effect waves-light pull-right  btn-success">Create & Submit</button>
                    </div>

                </div>
                <!-- end panel -->

            </div>
            <!-- end col-6 -->

        </div>
        <!-- end row -->
    </div>

@endsection

@section('script')
    {{--@include('layouts.admin_script.faq_script')--}}


    <script src="{{asset('Back-Assets/assets/plugins/jquerytree/jquery.tree.min.js')}}"></script>
    <script src="{{asset('Back-Assets/js/custom.min.js')}}"></script>
    <script src="{{asset('Back-Assets/js/jasny-bootstrap.js')}}"></script>

    <script src="{{asset('Back-Assets/assets/plugins/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('Back-Assets/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
    <script src="{{asset('Back-Assets/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js')}}" type="text/javascript"></script>
    <script src="{{asset('Back-Assets/assets/plugins/dff/dff.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('Back-Assets/assets/plugins/multiselect/js/jquery.multi-select.js')}}"></script>

    <!-- Clock Plugin JavaScript -->
    <script src="{{asset('Back-Assets/assets/plugins/clockpicker/dist/jquery-clockpicker.min.js')}}"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="{{asset('Back-Assets/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="{{asset('Back-Assets/assets/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
    <script src="{{asset('Back-Assets/assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('Back-Assets/assets/plugins/moment/moment.js')}}"></script>
    <script src="{{asset('Back-Assets/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>


    <script type="text/javascript">
        jQuery(document).ready(function() {

            $('#public-methods').multiSelect();
            $('#select-all').click(function() {
                $('#public-methods').multiSelect('select_all');
                return false;
            });
            $('#deselect-all').click(function() {
                $('#public-methods').multiSelect('deselect_all');
                return false;
            });

            $('#add-option').on('click', function() {
                $('#public-methods').multiSelect('addOption', {
                    value: 42,
                    text: 'test 42',
                    index: 0
                });
                return false;
            });

        });

     // Date Picker
        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true,
            format : 'yyyy-mm-dd'
        });
        jQuery('#datepicker-autoclose2').datepicker({
            autoclose: true,
            todayHighlight: true,
            format : 'yyyy-mm-dd'
        });

        //create test
        $('#create-test-btn').on('click',function(){
            $('#create-test-form').submit();
        });
    </script>

@endsection

