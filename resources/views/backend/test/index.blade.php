@extends('layouts.backend.main')
@section('css')
    {{-- @include('layouts.admin_css.faq_css')--}}
    <!-- Dropzone css -->
    <link href="{{asset('Back-Assets/assets/plugins/dropzone-master/dist/dropzone.css')}}" rel="stylesheet" type="text/css" />

@endsection
@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Manage Matrix</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Matrix</a></li>
                <li class="breadcrumb-item active">Manage</li>
            </ol>
        </div>
        <div>
        </div>
    </div>

    <!-- ============================================================== -->
    <!-- Main Content Starts here -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <!-- begin panel -->
                <div class="card" data-sortable-id="tree-view-4">
                    <input type="hidden" id="test-id" value="" >
                    <div class="card-body">
                        <!-- TAB NAV Starts here -->
                        <ul class="nav nav-tabs customtab" role="tablist">
                            <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home2" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">In Progress</span></a> </li>
                            <li class="nav-item"> <a class="nav-link show" data-toggle="tab" href="#profile2" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Completed</span></a> </li>
                        </ul>
                        <!-- TAB Content Starts here -->
                        <div class="tab-content">
                            <div class="tab-pane p-20 active show" id="home2" role="tabpanel">
                                    <h3>In Progress</h3>
                                    <table id="InProgressTable" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Focus Area</th>
                                            <th>Competency Area</th>
                                            <th>Tested By</th>
                                            <th>Started Date</th>
                                            <th></th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($test->InProgressTests() as $row)
                                            <tr>
                                                <td>{{$row->focus_area->name}}</td>
                                                <td>
                                                    @if($row->competency_areas()->count() > 1)
                                                        <label class="label label-info" data-toggle="tooltip" data-html="true" data-original-title="@foreach($row->competency_areas as $competency_area){{$competency_area->name}}<br>@endforeach"> {{$row->competency_areas()->count()}} Competencies tested</label>
                                                    @else
                                                        <label class="label label-info" data-toggle="tooltip" data-html="true" data-original-title=""> {{$row->competency_areas()->first()->name}}</label>
                                                    @endif

                                                </td>
                                                <td>{{$row->location->location}}</td>
                                                <td>{{$row->start_date}}</td>
                                                <th>
                                                    <div class="btn-group" role="group" aria-label="Basic example">
                                                        <div class="btn-group dropleft">
                                                            <button type="button" class="btn btn-sm btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                Action
                                                            </button>
                                                            <div class="dropdown-menu" x-placement="left-start" style="position: absolute; transform: translate3d(-2px, 0px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                                <!-- Dropdown menu links -->
                                                                <a class="dropdown-item complete-test" data-toggle="modal"  data-id="{{$row->id}}"><i class="fa fa-check" ></i> Mark as Complete</a>
                                                                <div class="dropdown-divider"></div>
                                                                <a href="{{route('admin.tests.edit',['id' => $row->id])}}" class="dropdown-item" ><i class="fa fa-pencil" ></i> Edit</a>
                                                                <div class="dropdown-divider"></div>
                                                                <a class="dropdown-item bg-danger cancel-test" style="color: white" data-toggle="modal"  data-id="{{$row->id}}"><i class="fa fa-trash"></i> Delete</a>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </th>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                            </div>
                            <div class="tab-pane p-20 show" id="profile2" role="tabpanel">
                                    <h3>Completed</h3>
                                <table id="CompletedTable" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Focus Area</th>
                                        <th>Competency Area</th>
                                        <th>Tested By</th>
                                        <th>Completed Date</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($test->completedTests() as $row)
                                        <tr>
                                            <td>{{$row->focus_area->name}}</td>
                                            <td>
                                                @if($row->competency_areas()->count() > 1)
                                                    <label class="label label-info" data-toggle="tooltip" data-html="true" data-original-title="@foreach($row->competency_areas as $competency_area){{$competency_area->name}}<br>@endforeach"> {{$row->competency_areas()->count()}} Competencies tested</label>
                                                @else
                                                    <label class="label label-info" data-toggle="tooltip" data-html="true" data-original-title=""> {{$row->competency_areas()->first()->name}}</label>
                                                @endif

                                            </td>
                                            <td>{{$row->location->location}}</td>
                                            <td>{{$row->completed_date}}</td>
                                            <td>
                                                <a href="{{route('admin.tests.show',['id' => $row->id])}}" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-eye text-inverse m-r-10"></i> </a>
                                                <a href="#" data-toggle="tooltip" data-original-title="Delete" class="cancel-test" data-id="{{$row->id}}"> <i class="fa fa-close text-danger"></i> </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>



                </div>
                <!-- end panel -->
            </div>
            <!-- end col-6 -->
        </div>
        <!-- end row -->
    </div>
    <!-- MODAL Complete test -->
    <div id="verticalcenter" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="vcenter" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="vcenter">Complete a Test</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <h4>You are about to complete a <strong>Test</strong>, Would you like to continue</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal" aria-hidden="true" >No, Cancel</button>
                    <button type="button" class="btn btn-success waves-effect" id="complete-test-btn">Yes, Complete Test</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- MODAL Cancel -->
    <div id="cancel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="vcenter" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title" id="vcenter">Cancel TEST</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <h4>You are about to cancel a test</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success waves-effect" data-dismiss="modal" aria-hidden="true">No, Don't</button>
                    <button type="button" class="btn btn-danger waves-effect" id="cancel-test-btn">Yes, Cancel</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection
@section('script')
    <!-- Dropzone Plugin JavaScript -->
    <script src="{{asset('Back-Assets/assets/plugins/dropzone-master/dist/dropzone.js')}}"></script>
    <!-- This is data table -->
    <script src="{{asset('Back-Assets/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script>

        $('#InProgressTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "order": [ 3, 'dec' ],
        });
        $('#CompletedTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "order": [ 3, 'dec' ],

        });

        //complete
        $('.complete-test').on('click',function(){
            $('#test-id').val($(this).data('id'));
            $('#verticalcenter').modal('show');
        });
        $('#complete-test-btn').on('click',function(){
            $.ajax({
                type : 'POST',
                url : "{{url('admin/test/mark-test-completed')}}/" + $('#test-id').val(),
                data : "_token={{csrf_token()}}&_method=PUT",
                success : function (data) {
                    window.location = data.url;
                }
            });
        });

        //cancel
        $('.cancel-test').on('click',function(){
            $('#test-id').val($(this).data('id'));
            $('#cancel').modal('show');
        });
        $('#cancel-test-btn').on('click',function(){
            $.ajax({
                type : 'POST',
                url : "{{url('admin/tests/')}}/" + $('#test-id').val(),
                data : "_token={{csrf_token()}}&_method=DELETE",
                success : function (data) {
                    window.location = data.url;
                }
            });
        });
    </script>
@endsection