@extends('layouts.backend.main')

@section('css')
    {{-- @include('layouts.admin_css.faq_css')--}}

    <link href="{{asset('Back-Assets/assets/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('Back-Assets/css/custom.css')}}" type="text/css" media="screen">

@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">#{{$test->id}} Overview</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('member.tests.index')}}">My Tests</a></li>
                <li class="breadcrumb-item">{{$test->id}}</li>
                <li class="breadcrumb-item active">Overview</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <!-- begin panel -->
                <div class="row">
                    <div class="col-4">
                        <div class="card">
                            <div class="card-header">
                                Test Details
                                <input type="hidden" id="competency-area-id" value="" >
                            </div>
                            <div class="card-body">
                               <div class="row">
                                   <div class="col-md-4">
                                       <strong>Focus Area:</strong>
                                   </div>
                                   <div class="col-8"  align="right">
                                       {{$test->focus_area->name}}
                                   </div>
                                   <div class="col-md-4">
                                       <strong>Created By:</strong>
                                   </div>
                                   <div class="col-md-8"  align="right">
                                       {{$test->location->location}}
                                   </div>
                                   <div class="col-md-4">
                                       <strong>Started Date:</strong>
                                   </div>
                                   <div class="col-md-8"  align="right">
                                       {{$test->start_date}}
                                   </div>
                                   <div class="col-md-5">
                                       <strong>Completed Date:</strong>
                                   </div>
                                   <div class="col-md-7"  align="right">
                                       {{$test->completed_date}}
                                   </div>
                               </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12" align="center">
                                        <h3>Competencies Tested</h3>
                                    </div>
                                    <div class="col-md-12">
                                        <ul>
                                            @foreach($test->competency_areas as $row)
                                                <li>{{$row->name}}</li>
                                                @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card" data-sortable-id="tree-view-4">
                            <div class="card-body">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Competency:</th>
                                        <th># Of Documents</th>
                                        <th class="text-nowrap">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($test->competency_areas as $row)
                                        <tr>
                                            <td>{{$row->name}}</td>
                                            <td>
                                                <label class="label label-success">{{$test->publicationsForCompetencyAreaCount($row->id)}}</label>
                                            </td>
                                            <td class="text-nowrap">
                                                <button type="button" class="btn btn-sm btn-outline-primary upload-doc"  data-id="{{$row->id}}" data-name="{{$row->name}}"  ><i class="fa fa-upload"></i> Upload Document</button>
                                                <button type="button" class="btn btn-sm btn-outline-info view-doc" data-id="{{$row->id}}" data-name="{{$row->name}}"> <i class="fa fa-eye"></i> View Documents</button>

                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>

                            </div>



                        </div>
                    </div>
                </div>


                <!-- end panel -->
            </div>
            <!-- end col-6 -->
        </div>
        <!-- end row -->
    </div>
<!-- View Documents Modal -->
    <div id="ViewDocModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Viewing <span id="competency-area-name"></span> Publications</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Author</th>
                            <th class="text-nowrap">Action</th>
                        </tr>
                        </thead>
                        <tbody id="view-doc-content">

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Upload Documents Modal -->
    <div id="UploadDocModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Document</h4>
                </div>
                <div class="modal-body">
                    <blockquote>You are about to add a Document to: {{$test->focus_area->name}} / <span id="competency-area-name"></span> Under your Test {{$test->id}}</blockquote>
                    <hr>
                    <form ID="upload-form">
                        <input type="hidden" name="test_id" value="{{$test->id}}">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Title:</label>
                            <input type="text" class="form-control" id="title" name="title">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Author:</label>
                            <input type="text" class="form-control" id="author" name="author">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Document::</label>
                            <div class="custom-file mb-3">
                                <input type="file" class="custom-file-input" id="customFile" name="publication">
                                <label class="custom-file-label form-control" for="customFile">Choose file</label>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger waves-effect waves-light" id="save-btn">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    {{--@include('layouts.admin_script.faq_script')--}}

    <script src="{{asset('Back-Assets/assets/plugins/jquerytree/jquery.tree.min.js')}}"></script>
    <script src="{{asset('Back-Assets/js/custom.min.js')}}"></script>
    <script src="{{asset('Back-Assets/js/jasny-bootstrap.js')}}"></script>
    <script>
        //upload doc
        $('.upload-doc').on('click',function(){
            $('#competency-area-id').val($(this).data('id'));
            $('#competency-area-name').text($(this).data('name'));
            $('#UploadDocModal').modal('show');
        });
        $('#save-btn').on('click',function(){

            var data = new FormData();

            //Form data
            var form_data = $('#upload-form').serializeArray();
            $.each(form_data, function (key, input) {
                data.append(input.name, input.value);
            });

            //File data
            console.log($('input:file').get(0).files[0]);
            console.log('yes');
            var file_data = $('input:file');

            data.append("publication", file_data.get(0).files[0]);

            //Custom data
            data.append('competency_area_id', $('#competency-area-id').val());
            data.append('_token', "{{csrf_token()}}");

            console.log(data);


            $.ajax({
                type : 'POST',
                url : "{{route('member.test.publications.store')}}",
                mimeType:"multipart/form-data",
                contentType: false,
                cache: false,
                processData:false,
                data : data,
                success : function (res) {
                    //window.location = res.link;
                    location.reload();

                }
            });
        });

        //view doc
        $('.view-doc').on('click',function(){
            $('#competency-area-id').val($(this).data('id'));
            $('#competency-area-name').text($(this).data('name'));

            $.ajax({
                type : 'GET',
                url : "{{route('member.test.publications.index')}}",
                data : "_token={{csrf_token()}}&competency_area_id=" + $('#competency-area-id').val() + "&test_id=" + "{{$test->id}}",
                success : function (data) {
                    var content = $('#view-doc-content');
                    content.empty();
                    $.each(data,function(index,value){
                        var downloadLink = "{{url('uploads')}}/test/publication/" + value.file_name;
                        var el = $('<tr>\n' +
                            '            <td>' + value.title + '</td>\n' +
                            '            <td>\n' + value.author + '</td>\n' +
                            '            <td class="text-nowrap">\n' +
                            '                <a href="' + downloadLink + '" data-toggle="tooltip" data-original-title="Download" target="_blank"> <i class="fa fa-download text-inverse m-r-10"></i> </a>\n' +
                            '                <a href="#" data-toggle="tooltip" data-original-title="Delete" data-id="' + value.id + '" class="delete-btn"> <i class="fa fa-close text-danger"></i> </a>\n' +
                            '            </td>\n' +
                            '            </tr>');

                        content.append(el);

                        //set click event on delete button
                        el.find('a.delete-btn').on('click',function(){
                            alert('clicked');
                            $.ajax({
                                type : 'POST',
                                url : "{{url('member/test/publications/')}}/" + $(this).data('id'),
                                data : "_token={{csrf_token()}}&_method=DELETE",
                                success : function (data) {
                                    //window.location = data.url;
                                    location.reload();
                                }
                            });
                        });
                    });

                }
            });
            $('#ViewDocModal').modal('show');
        });

    </script>

@endsection

