@extends('layouts.backend.main')

@section('css')
    {{-- @include('layouts.admin_css.faq_css')--}}

    <link href="{{asset('Back-Assets/assets/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('Back-Assets/css/custom.css')}}" type="text/css" media="screen">

@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Focus Areas</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Matrix</a></li>
                <li class="breadcrumb-item active">Focus Areas</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <!-- begin panel -->
                <div class="card" data-sortable-id="tree-view-4">
                    <div class="card-body">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-lg-4 col-md-6">
                                    <button type="button" class="btn btn-block btn-outline-info">Create New Focus Area</button>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <button type="button" class="btn btn-block btn-outline-danger">Add Competency Area to All</button>
                                </div>
                            </div>
                        </div>
                        <hr>
                     <div class="row">
                         <div class="col-12"  style="margin-top: 2%">
                             <table id="InProgressTable" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                 <thead>
                                 <tr>
                                     <th>Focus Area</th>
                                     <th>No of Competency Area</th>
                                     <th></th>

                                 </tr>
                                 </thead>
                                 <tbody>
                                 <tr>
                                     <td>Concrete</td>
                                     <td>20</td>
                                     <th>
                                         <a href="#" data-toggle="tooltip" data-original-title="Add Competency Area"> <i class="fa fa-plus text-inverse m-r-10"></i> </a>
                                         <a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                         <a href="#" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>
                                     </th>

                                 </tr>

                                 </tbody>
                             </table>
                         </div>
                     </div>


                    </div>



                </div>
                <!-- end panel -->
            </div>
            <!-- end col-6 -->
        </div>
        <!-- end row -->
    </div>

@endsection

@section('script')
    {{--@include('layouts.admin_script.faq_script')--}}

    <script src="{{asset('Back-Assets/assets/plugins/jquerytree/jquery.tree.min.js')}}"></script>
    <script src="{{asset('Back-Assets/js/custom.min.js')}}"></script>
    <script src="{{asset('Back-Assets/js/jasny-bootstrap.js')}}"></script>

    <!-- This is data table -->
    <script src="{{asset('Back-Assets/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script>

        $('#InProgressTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "order": [ 3, 'dec' ],
        });
        $('#CompletedTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "order": [ 3, 'dec' ],

        });
    </script>

@endsection

