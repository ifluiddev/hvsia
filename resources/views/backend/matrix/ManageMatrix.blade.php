@extends('layouts.backend.main')
@section('css')
    {{-- @include('layouts.admin_css.faq_css')--}}
    <!-- Dropzone css -->
    <link href="{{asset('Back-Assets/assets/plugins/dropzone-master/dist/dropzone.css')}}" rel="stylesheet" type="text/css" />

@endsection
@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Manage Matrix</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Matrix</a></li>
                <li class="breadcrumb-item active">Manage</li>
            </ol>
        </div>
        <div>
        </div>
    </div>

    <!-- ============================================================== -->
    <!-- Main Content Starts here -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <!-- begin panel -->
                <div class="card" data-sortable-id="tree-view-4">
                    <div class="card-body">
                        <!-- TAB NAV Starts here -->
                        <ul class="nav nav-tabs customtab" role="tablist">
                            <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home2" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">In Progress</span></a> </li>
                            <li class="nav-item"> <a class="nav-link show" data-toggle="tab" href="#profile2" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Completed</span></a> </li>
                        </ul>
                        <!-- TAB Content Starts here -->
                        <div class="tab-content">
                            <div class="tab-pane p-20 active show" id="home2" role="tabpanel">
                                    <h3>In Progress</h3>
                                    <table id="InProgressTable" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Focus Area</th>
                                            <th>Competency Area</th>
                                            <th>Tested By</th>
                                            <th>Started Date</th>
                                            <th></th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Structural Design</td>
                                            <td>Concrete</td>
                                            <td>RSA CSIR/Gautrans</td>
                                            <td>2019/02/02</td>
                                            <th>
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                <div class="btn-group dropleft">
                                                  <button type="button" class="btn btn-sm btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Action
                                                  </button>
                                                      <div class="dropdown-menu" x-placement="left-start" style="position: absolute; transform: translate3d(-2px, 0px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                        <!-- Dropdown menu links -->
                                                          <a class="dropdown-item" data-toggle="modal" data-target="#MarkAsComplete"><i class="fa fa-check"></i> Mark as Complete</a>
                                                          <div class="dropdown-divider"></div>
                                                       <a class="dropdown-item bg-danger" style="color: white" data-toggle="modal" data-target="#DeleteModal"><i class="fa fa-trash"></i> Delete</a>
                                                      </div>
                                                </div>

                                                </div>
                                            </th>

                                        </tr>
                                        <tr>
                                            <td>Materials</td>
                                            <td>Concrete</td>
                                            <td>USACE ERDC Vicksburg</td>
                                            <td>2019/01/02</td>
                                            <th>
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <div class="btn-group dropleft">
                                                        <button type="button" class="btn btn-sm btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Action
                                                        </button>
                                                        <div class="dropdown-menu" x-placement="left-start" style="position: absolute; transform: translate3d(-2px, 0px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                            <!-- Dropdown menu links -->
                                                            <a class="dropdown-item" data-toggle="modal" data-target="#MarkAsComplete"><i class="fa fa-check"></i> Mark as Complete</a>
                                                            <div class="dropdown-divider"></div>
                                                           <a class="dropdown-item bg-danger" style="color: white" data-toggle="modal" data-target="#DeleteModal"><i class="fa fa-trash"></i> Delete</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </th>

                                        </tr>
                                        <tr>
                                            <td>Functional Performance</td>
                                            <td>Bitumen/polymer stab</td>
                                            <td>India CRRI</td>
                                            <td>2019/03/02</td>
                                            <th>
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <div class="btn-group dropleft">
                                                        <button type="button" class="btn btn-sm btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Action
                                                        </button>
                                                        <div class="dropdown-menu" x-placement="left-start" style="position: absolute; transform: translate3d(-2px, 0px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                            <!-- Dropdown menu links -->
                                                            <a class="dropdown-item" data-toggle="modal" data-target="#MarkAsComplete"><i class="fa fa-check"></i> Mark as Complete</a>
                                                            <div class="dropdown-divider"></div>
                                                           <a class="dropdown-item bg-danger" style="color: white" data-toggle="modal" data-target="#DeleteModal"><i class="fa fa-trash"></i> Delete</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </th>

                                        </tr>
                                        <tr>
                                            <td>Structural Performance</td>
                                            <td>Upgrading of LVRs</td>
                                            <td>USA FAA</td>
                                            <td>2019/04/02</td>
                                            <th>
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <div class="btn-group dropleft">
                                                        <button type="button" class="btn btn-sm btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Action
                                                        </button>
                                                        <div class="dropdown-menu" x-placement="left-start" style="position: absolute; transform: translate3d(-2px, 0px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                            <!-- Dropdown menu links -->
                                                            <a class="dropdown-item" data-toggle="modal" data-target="#MarkAsComplete"><i class="fa fa-check"></i> Mark as Complete</a>
                                                            <div class="dropdown-divider"></div>
                                                           <a class="dropdown-item bg-danger" style="color: white" data-toggle="modal" data-target="#DeleteModal"><i class="fa fa-trash"></i> Delete</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </th>

                                        </tr>
                                        <tr>

                                            <td>Rehabilitation Maintenance</td>
                                            <td>Light weight material</td>
                                            <td>RSA CSIR/Gautrans</td>
                                            <td>2019/05/10</td>
                                            <th>
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <div class="btn-group dropleft">
                                                        <button type="button" class="btn btn-sm btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Action
                                                        </button>
                                                        <div class="dropdown-menu" x-placement="left-start" style="position: absolute; transform: translate3d(-2px, 0px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                            <!-- Dropdown menu links -->
                                                            <a class="dropdown-item" data-toggle="modal" data-target="#MarkAsComplete"><i class="fa fa-check"></i> Mark as Complete</a>
                                                            <div class="dropdown-divider"></div>
                                                           <a class="dropdown-item bg-danger" style="color: white" data-toggle="modal" data-target="#DeleteModal"><i class="fa fa-trash"></i> Delete</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </th>

                                        </tr>
                                        <tr>
                                            <td>Construction</td>
                                            <td>Bridge Components</td>
                                            <td>Saudi Arabia RTRC</td>
                                            <td>2019/06/02</td>
                                            <th>
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <div class="btn-group dropleft">
                                                        <button type="button" class="btn btn-sm btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Action
                                                        </button>
                                                        <div class="dropdown-menu" x-placement="left-start" style="position: absolute; transform: translate3d(-2px, 0px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                            <!-- Dropdown menu links -->
                                                            <a class="dropdown-item" data-toggle="modal" data-target="#MarkAsComplete"><i class="fa fa-check"></i> Mark as Complete</a>
                                                            <div class="dropdown-divider"></div>
                                                           <a class="dropdown-item bg-danger" style="color: white" data-toggle="modal" data-target="#DeleteModal"><i class="fa fa-trash"></i> Delete</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>

                                        </tr>
                                        <tr>
                                            <td>Information Systems</td>
                                            <td>Waste materials</td>
                                            <td>Mexico Instituto Mexicano</td>
                                            <td>2019/05/02</td>
                                            <th>
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <div class="btn-group dropleft">
                                                        <button type="button" class="btn btn-sm btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Action
                                                        </button>
                                                        <div class="dropdown-menu" x-placement="left-start" style="position: absolute; transform: translate3d(-2px, 0px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                            <!-- Dropdown menu links -->
                                                            <a class="dropdown-item" data-toggle="modal" data-target="#MarkAsComplete"><i class="fa fa-check"></i> Mark as Complete</a>
                                                            <div class="dropdown-divider"></div>
                                                           <a class="dropdown-item bg-danger" style="color: white" data-toggle="modal" data-target="#DeleteModal"><i class="fa fa-trash"></i> Delete</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>

                                        </tr>
                                        <tr>
                                            <td>Vehicle Pavement Interaction</td>
                                            <td>Bridge Components</td>
                                            <td>Virginia DoT</td>
                                            <td>2019/08/02</td>
                                            <th>
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <div class="btn-group dropleft">
                                                        <button type="button" class="btn btn-sm btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Action
                                                        </button>
                                                        <div class="dropdown-menu" x-placement="left-start" style="position: absolute; transform: translate3d(-2px, 0px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                            <!-- Dropdown menu links -->
                                                            <a class="dropdown-item" data-toggle="modal" data-target="#MarkAsComplete"><i class="fa fa-check"></i> Mark as Complete</a>
                                                            <div class="dropdown-divider"></div>
                                                            <a class="dropdown-item bg-danger" style="color: white" data-toggle="modal" data-target="#DeleteModal"><i class="fa fa-trash"></i> Delete</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>

                                        </tr>

                                        </tbody>
                                    </table>
                            </div>
                            <div class="tab-pane p-20 show" id="profile2" role="tabpanel">
                                    <h3>Completed</h3>
                                <table id="CompletedTable" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Focus Area</th>
                                        <th>Competency Area</th>
                                        <th>Tested By</th>
                                        <th>Completed Date</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Structural Design</td>
                                        <td>Concrete</td>
                                        <td>RSA CSIR/Gautrans</td>
                                        <td>2019/01/02</td>
                                        <th>
                                            <a href="#" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-eye text-inverse m-r-10"></i> </a>
                                            <a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                            <a href="#" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>
                                        </th>

                                    </tr>
                                    <tr>
                                        <td>Materials</td>
                                        <td>Concrete</td>
                                        <td>USACE ERDC Vicksburg</td>
                                        <td>2019/01/03</td>
                                        <th>
                                            <a href="#" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-eye text-inverse m-r-10"></i> </a>
                                            <a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                            <a href="#" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>
                                        </th>

                                    </tr>
                                    <tr>
                                        <td>Functional Performance</td>
                                        <td>Bitumen/polymer stab</td>
                                        <td>India CRRI</td>
                                        <td>2019/01/04</td>
                                        <td>
                                            <a href="#" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-eye text-inverse m-r-10"></i> </a>
                                        <a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                        <a href="#" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Structural Performance</td>
                                        <td>Upgrading of LVRs</td>
                                        <td>USA FAA</td>
                                        <td>2019/02/01</td>
                                        <th>
                                            <a href="#" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-eye text-inverse m-r-10"></i> </a>
                                            <a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                            <a href="#" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>
                                        </th>

                                    </tr>
                                    <tr>

                                        <td>Rehabilitation Maintenance</td>
                                        <td>Light weight material</td>
                                        <td>RSA CSIR/Gautrans</td>
                                        <td>2019/02/03</td>
                                        <th>
                                            <a href="#" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-eye text-inverse m-r-10"></i> </a>
                                            <a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                            <a href="#" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>
                                        </th>

                                    </tr>
                                    <tr>
                                        <td>Construction</td>
                                        <td>Bridge Components</td>
                                        <td>Saudi Arabia RTRC</td>
                                        <td>2019/06/04</td>
                                        <th>
                                            <a href="#" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-eye text-inverse m-r-10"></i> </a>
                                            <a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                            <a href="#" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>
                                        </th>

                                    </tr>
                                    <tr>
                                        <td>Information Systems</td>
                                        <td>Waste materials</td>
                                        <td>Mexico Instituto Mexicano</td>
                                        <td>2019/06/10</td>
                                        <th>
                                            <a href="#" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-eye text-inverse m-r-10"></i> </a>
                                            <a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                            <a href="#" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>
                                        </th>

                                    </tr>
                                    <tr>
                                        <td>Vehicle Pavement Interaction</td>
                                        <td>Bridge Components</td>
                                        <td>Virginia DoT</td>
                                        <td>2019/06/05</td>
                                        <th>
                                            <a href="#" data-toggle="tooltip" data-original-title="View"> <i class="fa fa-eye text-inverse m-r-10"></i> </a>
                                            <a href="#" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                            <a href="#" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>
                                        </th>

                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>



                </div>
                <!-- end panel -->
            </div>
            <!-- end col-6 -->
        </div>
        <!-- end row -->
    </div>
<!--------------------       Modal Mark as Complete        ------------------->
    <div id="DeleteModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="vcenter" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title" id="vcenter">Delete Test</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <h4>You are about to delete:</h4>
                    <hr>
                    <ul>
                        <li><strong>Focus Area:</strong> {Name of focus Area}</li>
                        <li><strong>Competency Area:</strong> {Name of Competency Area}</li>
                        <li><strong>Created By:</strong> {Name of Organisation}</li>
                        <li><strong>Created Date:</strong> {Date of Creation}</li>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success waves-effect" data-dismiss="modal" aria-hidden="true">No, Don't Delete</button>
                    <button type="button" class="btn btn-danger waves-effect">Yes, Delete</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!--------------------       Modal DELETE        ------------------->
    <div id="MarkAsComplete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="vcenter" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="vcenter">Start a New Test</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <h4>You are about to Complete a test.</h4>
                    <h6>Please upload all Documentation below before continuing. </h6>
                    <hr>
                    <form action="#" class="dropzone">
                        <div class="fallback">
                            <input name="file" type="file" multiple />
                        </div>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal" aria-hidden="true">Cancel</button>
                    <button type="button" class="btn btn-success waves-effect">Move to Completed</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection
@section('script')
    <!-- Dropzone Plugin JavaScript -->
    <script src="{{asset('Back-Assets/assets/plugins/dropzone-master/dist/dropzone.js')}}"></script>
    <!-- This is data table -->
    <script src="{{asset('Back-Assets/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script>

        $('#InProgressTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "order": [ 3, 'dec' ],
        });
        $('#CompletedTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "order": [ 3, 'dec' ],

        });
    </script>
@endsection