@extends('layouts.backend.main')

@section('css')
    <!-- Multi Select CSS -->
    <link href="{{asset('Back-Assets/assets/plugins/multiselect/css/multi-select.css')}}" rel="stylesheet" type="text/css" xmlns=""/>
    <!-- Date picker plugins css -->
    <link href="{{asset('Back-Assets/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('Back-Assets/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')}}" rel="stylesheet">
    <!-- Daterange picker plugins css -->
    <link href="{{asset('Back-Assets/assets/plugins/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('Back-Assets/assets/plugins/clockpicker/dist/jquery-clockpicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('Back-Assets/assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">

@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Create New Test</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Matrix</a></li>
                <li class="breadcrumb-item active">Create Test</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <!-- begin panel -->
                <div class="card" data-sortable-id="tree-view-4">
                    <div class="card-body">
                        <div class="form-group">
                            <label>Select A Owner for test</label>
                            <select class="form-control">
                                <option selected disabled>--- Select Organisation ---</option>
                                <option>RSA CSIR/Gautrans</option>
                                <option>USACE ERDC Vicksburg</option>
                                <option>Florida DoT</option>
                                <option>VTI Sweden</option>
                                <option>Caltrans UCPRC</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Select A Focus Area</label>
                            <select class="form-control">
                                <option selected disabled>--- Select Focus Area ---</option>
                                <option>Concrete</option>
                                <option>Graphite</option>
                            </select>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-xlg-12">
                                <h5 class="box-title">Select your Competencies</h5>
                                <select multiple id="public-methods" name="public-methods[]">
                                    <option value="elem_1">elem 1</option>
                                    <option value="elem_2">elem 2</option>
                                    <option value="elem_3">elem 3</option>
                                    <option value="elem_4">elem 4</option>
                                    <option value="elem_5">elem 5</option>
                                    <option value="elem_6">elem 6</option>
                                    <option value="elem_7">elem 7</option>
                                    <option value="elem_8">elem 8</option>
                                    <option value="elem_9">elem 9</option>
                                    <option value="elem_10">elem 10</option>
                                    <option value="elem_11">elem 11</option>
                                    <option value="elem_12">elem 12</option>
                                    <option value="elem_13">elem 13</option>
                                    <option value="elem_14">elem 14</option>
                                    <option value="elem_15">elem 15</option>
                                    <option value="elem_16">elem 16</option>
                                    <option value="elem_17">elem 17</option>
                                    <option value="elem_18">elem 18</option>
                                    <option value="elem_19">elem 19</option>
                                    <option value="elem_20">elem 20</option>
                                    <option value="elem_21">elem 21</option>
                                    <option value="elem_22">elem 22</option>
                                    <option value="elem_23">elem 23</option>
                                    <option value="elem_24">elem 24</option>
                                    <option value="elem_25">elem 25</option>
                                    <option value="elem_26">elem 26</option>
                                    <option value="elem_27">elem 27</option>
                                    <option value="elem_28">elem 28</option>
                                    <option value="elem_29">elem 29</option>
                                    <option value="elem_30">elem 30</option>
                                    <option value="elem_31">elem 31</option>
                                    <option value="elem_32">elem 32</option>
                                    <option value="elem_33">elem 33</option>
                                    <option value="elem_34">elem 34</option>
                                    <option value="elem_35">elem 35</option>
                                    <option value="elem_36">elem 36</option>
                                    <option value="elem_37">elem 37</option>
                                    <option value="elem_38">elem 38</option>
                                    <option value="elem_39">elem 39</option>
                                    <option value="elem_40">elem 40</option>
                                    <option value="elem_41">elem 41</option>
                                    <option value="elem_42">elem 42</option>
                                    <option value="elem_43">elem 43</option>
                                    <option value="elem_44">elem 44</option>
                                    <option value="elem_45">elem 45</option>
                                    <option value="elem_46">elem 46</option>
                                    <option value="elem_47">elem 47</option>
                                    <option value="elem_48">elem 48</option>
                                    <option value="elem_49">elem 49</option>
                                    <option value="elem_50">elem 50</option>
                                    <option value="elem_51">elem 51</option>
                                    <option value="elem_52">elem 52</option>
                                    <option value="elem_53">elem 53</option>
                                    <option value="elem_54">elem 54</option>
                                    <option value="elem_55">elem 55</option>
                                    <option value="elem_56">elem 56</option>
                                    <option value="elem_57">elem 57</option>
                                    <option value="elem_58">elem 58</option>
                                    <option value="elem_59">elem 59</option>
                                    <option value="elem_60">elem 60</option>
                                    <option value="elem_61">elem 61</option>
                                    <option value="elem_62">elem 62</option>
                                    <option value="elem_63">elem 63</option>
                                    <option value="elem_64">elem 64</option>
                                    <option value="elem_65">elem 65</option>
                                    <option value="elem_66">elem 66</option>
                                    <option value="elem_67">elem 67</option>
                                    <option value="elem_68">elem 68</option>
                                    <option value="elem_69">elem 69</option>
                                    <option value="elem_70">elem 70</option>
                                    <option value="elem_71">elem 71</option>
                                    <option value="elem_72">elem 72</option>
                                    <option value="elem_73">elem 73</option>
                                    <option value="elem_74">elem 74</option>
                                    <option value="elem_75">elem 75</option>
                                    <option value="elem_76">elem 76</option>
                                    <option value="elem_77">elem 77</option>
                                    <option value="elem_78">elem 78</option>
                                    <option value="elem_79">elem 79</option>
                                    <option value="elem_80">elem 80</option>
                                    <option value="elem_81">elem 81</option>
                                    <option value="elem_82">elem 82</option>
                                    <option value="elem_83">elem 83</option>
                                    <option value="elem_84">elem 84</option>
                                    <option value="elem_85">elem 85</option>
                                    <option value="elem_86">elem 86</option>
                                    <option value="elem_87">elem 87</option>
                                    <option value="elem_88">elem 88</option>
                                    <option value="elem_89">elem 89</option>
                                    <option value="elem_90">elem 90</option>
                                    <option value="elem_91">elem 91</option>
                                    <option value="elem_92">elem 92</option>
                                    <option value="elem_93">elem 93</option>
                                    <option value="elem_94">elem 94</option>
                                    <option value="elem_95">elem 95</option>
                                    <option value="elem_96">elem 96</option>
                                    <option value="elem_97">elem 97</option>
                                    <option value="elem_98">elem 98</option>
                                    <option value="elem_99">elem 99</option>
                                    <option value="elem_100">elem 100</option>
                                    <option value="elem_101">elem 101</option>
                                    <option value="elem_102">elem 102</option>
                                    <option value="elem_103">elem 103</option>
                                    <option value="elem_104">elem 104</option>
                                    <option value="elem_105">elem 105</option>
                                    <option value="elem_106">elem 106</option>
                                    <option value="elem_107">elem 107</option>
                                    <option value="elem_108">elem 108</option>
                                    <option value="elem_109">elem 109</option>
                                    <option value="elem_110">elem 110</option>
                                    <option value="elem_111">elem 111</option>
                                    <option value="elem_112">elem 112</option>
                                    <option value="elem_113">elem 113</option>
                                    <option value="elem_114">elem 114</option>
                                    <option value="elem_115">elem 115</option>
                                    <option value="elem_116">elem 116</option>
                                    <option value="elem_117">elem 117</option>
                                    <option value="elem_118">elem 118</option>
                                    <option value="elem_119">elem 119</option>
                                    <option value="elem_120">elem 120</option>
                                    <option value="elem_121">elem 121</option>
                                    <option value="elem_122">elem 122</option>
                                    <option value="elem_123">elem 123</option>
                                    <option value="elem_124">elem 124</option>
                                </select>
                                <div class="button-box m-t-20">
                                    <a id="select-all" class="btn btn-danger" href="#">select all</a>
                                    <a id="deselect-all" class="btn btn-info" href="#">deselect all</a>
                                </div>
                            </div>
                            <div class="col-lg-12 col-xlg-12">

                            </div>
                        </div>
                        <hr>
                        <blockquote class="blockquote bg-warning">Only fill out the following section if you are back dating a old test else ignore and create</blockquote>
                        <div class="row">
                            <div class="col-lg-6 col-xlg-6">
                                <label>Select a Started Date</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="datepicker-autoclose" placeholder="mm/dd/yyyy">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="icon-calender"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-xlg-6">
                                <label>Select a Completed Date</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="datepicker-autoclose2" placeholder="mm/dd/yyyy">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="icon-calender"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="button" class="btn waves-effect waves-light pull-right  btn-success">Create & Submit</button>
                    </div>

                </div>
                <!-- end panel -->

            </div>
            <!-- end col-6 -->

        </div>
        <!-- end row -->
    </div>

@endsection

@section('script')
    {{--@include('layouts.admin_script.faq_script')--}}


    <script src="{{asset('Back-Assets/assets/plugins/jquerytree/jquery.tree.min.js')}}"></script>
    <script src="{{asset('Back-Assets/js/custom.min.js')}}"></script>
    <script src="{{asset('Back-Assets/js/jasny-bootstrap.js')}}"></script>

    <script src="{{asset('Back-Assets/assets/plugins/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('Back-Assets/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
    <script src="{{asset('Back-Assets/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js')}}" type="text/javascript"></script>
    <script src="{{asset('Back-Assets/assets/plugins/dff/dff.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('Back-Assets/assets/plugins/multiselect/js/jquery.multi-select.js')}}"></script>

    <!-- Clock Plugin JavaScript -->
    <script src="{{asset('Back-Assets/assets/plugins/clockpicker/dist/jquery-clockpicker.min.js')}}"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="{{asset('Back-Assets/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="{{asset('Back-Assets/assets/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
    <script src="{{asset('Back-Assets/assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('Back-Assets/assets/plugins/moment/moment.js')}}"></script>
    <script src="{{asset('Back-Assets/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>


    <script type="text/javascript">
        jQuery(document).ready(function() {

            $('#public-methods').multiSelect();
            $('#select-all').click(function() {
                $('#public-methods').multiSelect('select_all');
                return false;
            });
            $('#deselect-all').click(function() {
                $('#public-methods').multiSelect('deselect_all');
                return false;
            });

            $('#add-option').on('click', function() {
                $('#public-methods').multiSelect('addOption', {
                    value: 42,
                    text: 'test 42',
                    index: 0
                });
                return false;
            });

        });

     // Date Picker
        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true,
            format : 'yyyy-mm-dd'
        });
        jQuery('#datepicker-autoclose2').datepicker({
            autoclose: true,
            todayHighlight: true,
            format : 'yyyy-mm-dd'
        });
    </script>

@endsection

