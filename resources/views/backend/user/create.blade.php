@extends('layouts.backend.main')

@section('css')
    {{-- @include('layouts.admin_css.faq_css')--}}

    <link href="{{asset('Back-Assets/assets/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('Back-Assets/css/custom.css')}}" type="text/css" media="screen">

@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Manage User</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Content</a></li>
                <li class="breadcrumb-item active">Manage User</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <!-- begin panel -->
                <div class="card" data-sortable-id="tree-view-4">
                    <div class="card-body">

                        <h4> Fill in the form </h4>
                        {{--@if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif--}}
                    </div>
                    <div class="card-body">
                        <form action="{{route('admin.users.store')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label class="control-label">First Name <span class="text-danger">*</span></label>
                                <input type="text" id="first-name" name="first_name" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" required value="{{old('first_name')}}">
                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback">
                                              <strong>{{ $errors->first('first_name') }} </strong>
                                            </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">Last Name <span class="text-danger">*</span></label>
                                <input type="text" id="last-name" name="last_name" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" required value="{{old('last_name')}}">
                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback">
                                              <strong>{{ $errors->first('last_name') }} </strong>
                                            </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">User Email <span class="text-danger">*</span></label>
                                <input type="text" id="email" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" required value="{{old('email')}}">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                              <strong>{{ $errors->first('email') }} </strong>
                                            </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">Phone Number </label>
                                <input type="text" id="phone-number" name="phone_number" class="form-control {{ $errors->has('phone_number') ? ' is-invalid' : '' }}" required value="{{old('phone_number')}}">
                                @if ($errors->has('phone_number'))
                                    <span class="invalid-feedback">
                                              <strong>{{ $errors->first('phone_number') }} </strong>
                                            </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">Location <span class="text-danger">*</span></label>
                                <select name="location_id" class="form-control {{ $errors->has('location_id') ? ' is-invalid' : '' }}">
                                    @foreach($locations as $row)
                                        <option value="{{$row->id}}" {{$row->id==old('location_id')?'selected' : ''}}>{{$row->location}}</option>
                                    @endforeach

                                </select>
                                @if ($errors->has('location_id'))
                                    <span class="invalid-feedback">
                                              <strong>{{ $errors->first('location_id') }} </strong>
                                            </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="control-label">Password <span class="text-danger">*</span></label></label>
                                <input type="text" id="password" name="password" class="form-control" >
                            </div>
                            <button type="submit" class="btn waves-effect btn-block waves-light btn-rounded btn-info pull-right"><i class="fa fa-plus"></i> Create New User</button>
                            <br/>
                        </form>

                    </div>



                </div>
                <!-- end panel -->
            </div>
            <!-- end col-6 -->
        </div>
        <!-- end row -->


        @endsection

        @section('script')
            {{--@include('layouts.admin_script.faq_script')--}}

            <script src="{{asset('Back-Assets/assets/plugins/jquerytree/jquery.tree.min.js')}}"></script>
            <script src="{{asset('Back-Assets/js/custom.min.js')}}"></script>
            <script src="{{asset('Back-Assets/js/jasny-bootstrap.js')}}"></script>

@endsection
