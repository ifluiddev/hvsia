@extends('layouts.backend.main')

@section('css')
   {{-- @include('layouts.admin_css.faq_css')--}}

    <link href="{{asset('Back-Assets/assets/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" />
   <link rel="stylesheet" href="{{asset('Back-Assets/css/custom.css')}}" type="text/css" media="screen">

@endsection

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Manage Administrator</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Content</a></li>
                <li class="breadcrumb-item active">Manage Administrator</li>
            </ol>
        </div>

    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Administrators</h4>
                        <h6 class="card-subtitle pull-right"><a href="{{route('admin.admins.create')}}" type="button" class="btn waves-effect waves-light btn-sm btn-info"><i class="fa fa-plus"></i> Create New Administrator</a></h6>
                        @include('layouts.frontend.inc.response')
                        <div class="table-responsive">
                            <table class="table table-striped ">
                                <thead>
                                <tr>
                                    <th>ID #</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th class="text-nowrap">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($admins as $row)
                                    <tr>
                                        <td>{{$row->id}}</td>
                                        <td>
                                            {{$row->name}}
                                        </td>
                                        <td>
                                            {{$row->email}}
                                        </td>
                                        <td>
                                            {{$row->role->role_name}}
                                        </td>
                                        <td class="text-nowrap">
                                            <a href="{{route('admin.admins.edit',['id' => $row->id])}}" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                            <a href="#modal-dialog"  data-id = "{{$row->id}}"  data-toggle="modal" style="width:100%" title="Delete"  id="delete-admin" class="trigger-modal"> <i class="fa fa-close text-danger"></i> </a>
                                        </td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>



        </div>
        <!-- end row -->
    </div>

        <!--====================================== #modal-dialog ========================================-->
        <div class="modal fade" id="modal-dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Administrator</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <p>
                            Are you sure you want to delete this Administrator
                        </p>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="btn btn-white"  data-dismiss="modal">Close</a>
                        <form action="{{route('admin.admins.destroy')}}" method="post">
                            {{csrf_field()}}
                            @method('DELETE')
                            <input type="hidden"  name="id" id="admin-id" value="1">
                            <input type="submit" class="btn btn-danger" id="delete" value="Delete">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #modal-without-animation -->


@endsection

@section('script')
    {{--@include('layouts.admin_script.faq_script')--}}

    <script>

        $('.trigger-modal').on('click', function () {
            var id = $(this).attr('data-id');
            $('#admin-id').val(id);
            $('#modal-dialog').modal('show');

        });

    </script>

@endsection
