@extends('layouts.frontend.main')
@section('page_title'){{$cms->page_title}}@endsection
@section('meta_desc'){{$cms->page_meta_description}}@endsection

@section('content')
    @if(isset($cms))

        @template('home',$cms->slug)
            @include('frontend.cms_template.home')
        @endif
        @template('cms',$cms->slug)
            @include('frontend.cms_template.cms')
        @endif
    @endif
@endsection