@extends('layouts.frontend.main')

@section('content')
    <section class="heading-page">
        <img src="{{asset('Front-Assets/images/project/projetc-heading.jpg')}}" alt="">
        <div class="heading-page-content position-center">
            <div class="page-title">
                <h1>Gallery</h1>
            </div>
            <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Gallery</li>
                </ul>
            </nav>
        </div>
    </section>

    <!-- Projects -->
    <section class="projects p-t-80 p-b-80">
        <h4 class="heading-section">Heading section</h4>
        <div class="container">
            <div class="projects-content">
                <ul class="filter-projects display-flex-center">
                    <li><span class="is-checked" data-filter="*">All</span></li>
                    @foreach($categories as $cat)
                        <li><span data-filter=".{{$cat->slug}}">{{$cat->name}}</span></li>
                        @endforeach
                </ul>

                <div class="grid">
                    <div class="row">
                        @foreach($galleries as $gal)
                            <article class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 item element-item {{$gal->category->slug}}">
                                <figure class="figure-hover">
                                    <a href="#">
                                        <img src="/uploads/gallery/{{$gal->image}}" alt="">
                                        <span class="overlay"></span>
                                    </a>
                                </figure>
                                <div class="info position-center">
                                    <div class="title">
                                        <h3 class="title-bold-white title-bold">
                                            {{$gal->title}}
                                        </h3>
                                    </div>
                                    <div class="desc">
                                        {{$gal->content}}
                                    </div>
                                </div>
                                <a href="#" class="view-more">View more</a>
                            </article>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
