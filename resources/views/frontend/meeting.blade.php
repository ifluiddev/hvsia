@extends('layouts.frontend.main')

@section('content')
    <style>
        ul.timeline {
            list-style-type: none;
            position: relative;
        }
        ul.timeline:before {
            content: ' ';
            background: #d4d9df;
            display: inline-block;
            position: absolute;
            left: 29px;
            width: 2px;
            height: 100%;
            z-index: 400;
        }
        ul.timeline > li {
            margin: 20px 0;
            padding-left: 20px;
        }
        ul.timeline > li:before {
            content: ' ';
            background: white;
            display: inline-block;
            position: absolute;
            border-radius: 50%;
            border: 3px solid #22c0e8;
            left: 20px;
            width: 20px;
            height: 20px;
            z-index: 400;
        }
    </style>
    <section class="heading-page">
        <img src="{{asset('Front-Assets/images/project/projetc-heading.jpg')}}" alt="">
        <div class="heading-page-content position-center">
            <div class="page-title">
                <h1>Upcoming Meetings</h1>
            </div>
            <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Meeting</li>
                </ul>
            </nav>
        </div>
    </section>

    <!-- Projects -->
    <section class="projects p-t-60 p-b-80">
        <div class="container">
            <div class="projects-content">
                <div class="container mb-5">
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <ul class="timeline">
                                <!--- Start Loop Through This --->
                                <li>
                                    <strong>New Web Design</strong>
                                    <strong class="float-right">21 March, 2014 @ 14:30</strong>
                                    <p>Description</p>
                                </li>
                                <!--- End  Loop Through This --->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
