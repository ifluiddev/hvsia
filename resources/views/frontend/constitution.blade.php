@extends('layouts.frontend.main')

@section('content')

    <section class="about-us">
        <div class="container">
            <div class="about-us-content">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 about-us-info">
                        <div class="section-title-left">
                            <h2>The Constitution</h2>
                        </div>
                        <p>
                            The HVSIA was established in 2002 during an International HVS workshop in South Africa. The founder members of the HVS included all agencies in possession of an HVS at that time: South Africa (Gautrans Provincial Department of Roads and Transport and the CSIR); Finland & Sweden (VTT & VTI), USA Army Corps of Engineers ERDC CRREL & Waterways Experimental Station; USA Florida DOT, USA California Pavement Research Centre.
                        </p>
                        <h5>The objectives of the HVSIA are to:</h5>
                        <hr>
                        <ul>
                            <li>
                                <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                <p>Promote and share knowledge related to HVS technology;</p>
                            </li>
                            <br/>
                            <li>
                                <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                <p>Establish a structure for ongoing interactions on topics related to pavement engineering with a specific focus on the HVS technology; </p>
                            </li>
                            <li>
                                <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                <p>Establish mechanisms for funding, monitoring and completing studies of common issues through the optimum participation of members;</p>
                            </li>
                            <li>
                                <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                <p>Provide expertise so that studies of interest can be expeditiously defined, managed and results reviewed;</p>
                            </li>
                            <li>
                                <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                <p>Optimize the use of resources through the coordination of HVS related research;</p>
                            </li>
                            <li>
                                <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                <p>Discuss and document HVS-related research practice and implementation of results;</p>
                            </li>
                            <li>
                                <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                <p>Identify new technology and upgrades desired by the group.</p>
                            </li>

                        </ul>

                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 about-us-image">
                        <img src="{{asset('Front-Assets/images/aboutus/about-us.png')}}" alt="">
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 about-us-info">

                    </div>
                </div>
                <div class="row" style="margin-bottom: 3%">

                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 about-us-image">
                        <img src="{{asset('Front-Assets/images/aboutus/about-us.png')}}" alt="">
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 about-us-info">
                        <div class="section-title-left">

                        <h5>To pursue its objectives, the Alliance shall:</h5>
                        <hr />
                        <ul>
                            <li>
                                <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                <p>Hold meetings to communicate activities, results and findings related to HVS technology;</p>
                            </li>
                            <br/>
                            <li>
                                <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                <p>Facilitate the collection and dissemination of information relating to HVS technology through the provision of communication structures between members of the Alliance.</p>
                            </li>
                            <li>
                                <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                <p>Meetings shall be held at least once a year.  The format of the meetings shall consist of presentations and discussions on activities/topics of common interest to the members, supplemented by site visits and/or demonstrations where appropriate;</p>
                            </li>
                            <li>
                                <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                <p>Each Member of the Alliance will support the attendance of his representatives at the annual meetings;</p>
                            </li>
                            <li>
                                <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                <p>The Lead Agency or Host will cover the venue costs for the meetings, preparation and distribution costs of formal minutes and the annual report.</p>
                            </li>
                        </ul>

                        <h5>The members of the Alliance shall be:</h5>
                        <hr />
                        <ul>
                            <li>
                                <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                <p>HVS owner (either the pavement research manager or APT programme manager), and/or any organization or individual responsible for the HVS operations program.</p>
                            </li>
                            <br/>
                            <li>
                                <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                <p>An organization automatically becomes a member after a purchasing order has been issued to procure an HVS.</p>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 about-us-info">
                        <div class="section-title-left">
                            <h5>The Executive Council</h5>
                            <hr />
                            <p>The Executive Council (ExCo) shall consist of one representative from each of the HVS owners. All membership, constitutional and financial matters will be dealt with by the ExCo. The ExCo duties shall be to:</p>
                            <ul>
                                <li>
                                    <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                    <p>Manage the affairs and policies of the Alliance.</p>
                                </li>
                                <li>
                                    <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                    <p>Establish other committees for managing specific portfolios with delegated authority to act on behalf of the ExCo.</p>
                                </li>
                                <li>
                                    <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                    <p>Identify and/or approve organizations or individuals for membership.</p>
                                </li>
                                <li>
                                    <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                    <p>Elect the Lead Agency and Chairperson.</p>
                                </li>
                                <li>
                                    <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                    <p>Chairman has the authority to invite non-HVS owners/operators to attend HVSIA meetings. The chair may also approach other HVS users to host future HVSIA meetings at their facility.</p>
                                </li>
                                <li>
                                    <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                    <p>HVSIA Chair Rotation. Although the original constitution (2002) stated that the rotation will be on a 3-yearly basis it was reduced to 2 years (2014) to allow all new owners to chair and host future meetings.</p>
                                </li>
                                <li>
                                    <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                    <p>Voting: Issues related to the activities and operations of the Alliance, all members of the ExCo will be allowed to vote.</p>
                                </li>

                            </ul>
                        </div>

                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 about-us-image">
                        <img src="{{asset('Front-Assets/images/aboutus/about-us.png')}}" alt="">
                    </div>

                </div>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 about-us-info">
                        <div class="section-title-left">
                            <h4>TERMS AND DEFINITIONS</h4>
                            <hr />
                            <ul>
                                <li>
                                    <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                    <p><b>Alliance :</b> The body constituted to coordinate activities and information sharing between Heavy Vehicle Simulator (HVS) owners and other relevant parties</p>
                                </li>
                                <li>
                                    <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                    <p><b>Owner:</b> Any organization owning or jointly owning a HVS.</p>
                                </li>
                                <li>
                                    <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                    <p><b>Operator:</b> Any organization involved in the operations of a HVS, including data collection and analysis.</p>
                                </li>
                                <li>
                                    <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                    <p><b>Executive_Council:</b> The Council of HVS owners constituted to drive the Alliance</p>
                                </li>
                                <li>
                                    <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                    <p><b>Lead Agency:</b> The organization nominated by the Executive Council to Chair the Executive Council and Alliance meetings.</p>
                                </li>
                                <li>
                                    <span><i class="fa fa-check" aria-hidden="true"></i></span>
                                    <p><b>HVS:</b> Full scale accelerated pavement testing device built under licenses from the Council of Scientific and Industrial Research (CSIR), Republic of South Africa</p>
                                </li>

                            </ul>
                        </div>

                    </div>


                </div>
        </div>
    </section>

    <a href="#" class="totop">Back to top</a>
@endsection
