@extends('layouts.frontend.main')

@section('content')

    <!-- Slide show  -->
    <section class="slide-show">
        <h4 class="heading-section">Heading section</h4>
        <!-- START REVOLUTION SLIDER 5.4.1 fullscreen mode -->
        <div id="au_rev_slider" class="rev_slider fullscreenbanner slide-content" style="display:none;" data-version="5.4.1">
            <ul>
                <!-- SLIDE  -->
                <li data-index="rs-82" data-transition="slidingoverlaydown">
                    <!-- MAIN IMAGE -->
                    <img src="{{asset('Front-Assets/images/home/home1-slideshow1.jpg')}}" alt="">

                    <!-- LAYERS -->
                    <div class="tp-caption tp-resizeme icon-home" data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                         data-responsive="on" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['110','110','50','30']" data-width="['770', '770', '770', '380']">
                        <img src="{{asset('Front-Assets/images/home/icon-home.png')}}" alt="">
                    </div>
                    <div class="tp-caption tp-resizeme slide-title" data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                         data-responsive="on" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['center','center','center','center']" data-voffset="['-43','-43','-53','-53']" data-width="['770', '770', '770', '380']"
                         data-fontsize="['30','30','20','20']">
                        Welcome to HVSIA
                    </div>
                    <div class="tp-caption tp-resizeme slide-desc" data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                         data-responsive="on" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['center','center','center','center']" data-voffset="['4', '4', '4', '4']" data-width="['770', '770', '770', '380']"
                         data-height="['auto']" data-whitespace="normal" data-type="text" data-textalign="['center']">
                        The HVS International Alliance is a voluntary association of Heavy Vehicle Simulator users, which has been established to promote interaction between users and to co-ordinate testing and the dissemination of test results
                    </div>
                    <div class="tp-caption tp-resizeme slide-button" data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                         data-responsive="on" data-x="['center','center','center','center']" data-hoffset="['0', '0', '0', '0']" data-y="['center','center','center','center']" data-voffset="['90', '90', '90', '90']" data-width="['770', '770', '770', '380']">
                        <a href="" class="btn-global btn-yellow">Contact us</a>
                        <a href="" class="btn-global btn-global-hover btn-global-border-hover">Learn more</a>
                    </div>
                </li>
                <!-- SLIDE  -->
                <li data-index="rs-83" data-transition="3dcurtain-vertical">
                    <!-- MAIN IMAGE -->
                    <img src="{{asset('Front-Assets/images/home/home1-slideshow2.jpg')}}" alt="">

                    <!-- LAYERS -->
                    <div class="tp-caption tp-resizeme icon-home" data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                         data-responsive="on" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['110','110','50','30']" data-width="['770', '770', '770', '380']">
                        <img src="{{asset('Front-Assets/images/home/icon-home.png')}}" alt="">
                    </div>
                    <div class="tp-caption tp-resizeme slide-title" data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                         data-responsive="on" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['center','center','center','center']" data-voffset="['-43','-43','-53','-53']"
                         data-fontsize="['30','30','20','20']" data-width="['770', '770', '770', '380']">
                        Committed to superior quality and results.
                    </div>
                    <div class="tp-caption tp-resizeme slide-desc" data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                         data-responsive="on" data-x="['center','center','center','center']" data-hoffset="['0', '0', '0', '0']" data-y="['center','center','center','center']" data-voffset="['4', '4', '4', '4']" data-width="['770', '770', '770', '380']"
                         data-whitespace="normal" data-height="['auto']" data-textalign="['center']">
                        Etiam dignissim diam quis enim lobortis. Tempor orci dapibus ultrices in. Nunc pulvinar sapien et ligula. Ipsum faucibus vitae aliquet nec ullamcorper sit. Risus nec feugiat in fermentum.
                    </div>
                    <div class="tp-caption tp-resizeme slide-button" data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                         data-responsive="on" data-x="['center','center','center','center']" data-hoffset="['0', '0', '0', '0']" data-y="['center','center','center','center']" data-voffset="['90', '90', '90', '90']" data-width="['770', '770', '770', '380']">
                        <a href="" class="btn-global btn-yellow">Contact us</a>
                        <a href="" class="btn-global btn-global-hover btn-global-border-hover">Learn more</a>
                    </div>
                </li>
            </ul>
            <!-- <div class="tp-bannertimer" style="height: 10px; background: rgba(255,255,255,0.25);"></div> -->
        </div>
        <!-- END REVOLUTION SLIDER -->
    </section>

    <!-- Request quote -->
    <section class="home5-aboutus background-grey">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12 home5-aboutus-content">
                    <div class="section-title-left">
                        <h2>Welcome to the HVS International Alliance</h2>
                    </div>
                    <div class="desc">
                        <p>
                            The HVS International Alliance is a voluntary association of Heavy Vehicle Simulator users, which has been established to promote interaction between users and to co-ordinate testing and the dissemination of test results. This website has been established to capture pertinent information and to display meta-data in respect of the results of tests. Detailed results can be obtained by visiting individual websites or by downloading some of the limited results available on this website under test information.
                        </p>
                    </div>
                    <div class="signature">
                        <img src="{{asset('Front-Assets/images/home/home5-about-singature.png')}}" alt="">
                    </div>
                </div>
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12 home5-video">
                    <figure class="vimeo">
                        <a href="http://player.vimeo.com/video/112734492">
                            <img src="{{asset('Front-Assets/images/home/home5-aboutus.jpg')}}" alt="">
                            <span class="icon-play"><i class="fa fa-play" aria-hidden="true"></i></span>
                        </a>
                    </figure>
                    <div class="name title-bold">
                        Philip Williams
                    </div>
                    <div class="position">
                        CEO in Bricktower
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Our Services -->
    <section class="services p-t-80 p-b-100">
        <h4 class="heading-section">Heading section</h4>
        <div class="container">
            <div class="services-content">
                <div class="row">
                    <article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 item">
                        <figure class="figure-hover">
                            <a href="single-service1.html">
                                <img src="{{asset('Front-Assets/images/home/service-1.jpg')}}" alt="">
                                <span class="overlay"></span>
                            </a>
                            <span>+</span>
                        </figure>
                        <div class="info">
                            <div class="title">
                                <h3 class="title-bold">
                                    <a href="single-service1.html" class="font-color-black">
                                        GPDRT and CSIR
                                    </a>
                                </h3>
                            </div>
                            <div class="desc">
                                Lorem ipsum dolor sit amet, consecttur adipiscing eli ellentesque tincidunt.
                            </div>
                        </div>
                    </article>
                    <article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 item">
                        <figure class="figure-hover">
                            <a href="single-service2.html">
                                <img src="{{asset('Front-Assets/images/home/service-2.jpg')}}" alt="">
                                <span class="overlay"></span>
                            </a>
                            <span>+</span>
                        </figure>
                        <div class="info">
                            <div class="title">
                                <h3 class="title-bold">
                                    <a href="single-service2.html" class="font-color-black">
                                        USA California PRC
                                    </a>
                                </h3>
                            </div>
                            <div class="desc">
                                Lorem ipsum dolor sit amet, consecttur adipiscing eli ellentesque tincidunt.
                            </div>
                        </div>
                    </article>
                    <article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 item">
                        <figure class="figure-hover">
                            <a href="single-service3.html">
                                <img src="{{asset('Front-Assets/images/home/service-3.jpg')}}" alt="">
                                <span class="overlay"></span>
                            </a>
                            <span>+</span>
                        </figure>
                        <div class="info">
                            <div class="title">
                                <h3 class="title-bold">
                                    <a href="single-service3.html" class="font-color-black">
                                        USA Florida DOT
                                    </a>
                                </h3>
                            </div>
                            <div class="desc">
                                Lorem ipsum dolor sit amet, consecttur adipiscing eli ellentesque tincidunt.
                            </div>
                        </div>
                    </article>
                    <article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 item">
                        <figure class="figure-hover">
                            <a href="single-service4.html">
                                <img src="{{asset('Front-Assets/images/home/service-4.jpg')}}" alt="">
                                <span class="overlay"></span>
                            </a>
                            <span>+</span>
                        </figure>
                        <div class="info">
                            <div class="title">
                                <h3 class="title-bold">
                                    <a href="single-service4.html" class="font-color-black">
                                        USA CoE: Waterways experimental station
                                    </a>
                                </h3>
                            </div>
                            <div class="desc">
                                Lorem ipsum dolor sit amet, consecttur adipiscing eli ellentesque tincidunt.
                            </div>
                        </div>
                    </article>
                </div>
                <div class="all-button">
                    <a href="#" class="btn-global btn-yellow btn-global-yellow-hover" data-text="All Services">All Locations</a>
                </div>
            </div>
        </div>
    </section>



    <a href="#" class="totop">Back to top</a>
@endsection
