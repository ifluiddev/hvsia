<!--==============================Slider=================================-->
{{--@include('front_end.slider.slider')--}}
<!--======= Expending =======-->

<section class="slide-show">
    <h4 class="heading-section">Heading section</h4>
    <!-- START REVOLUTION SLIDER 5.4.1 fullscreen mode -->
    <div id="au_rev_slider" class="rev_slider fullscreenbanner slide-content" style="display:none;" data-version="5.4.1">
        <ul>
            <!-- SLIDE  -->

            @foreach($banners as $row)

                @if ($loop->index%2 == 0)
                    @php
                        $transition = '3dcurtain-vertical';
                    @endphp
                    @else
                    @php
                        $transition = 'slidingoverlaydown';
                    @endphp
                @endif
            <li data-index="rs-8{{$loop->index}}" data-transition="{{$transition}}">
                <!-- MAIN IMAGE -->
                <img src="/uploads/banner_image/{{$row->bg_image}}" alt="">

                <!-- LAYERS -->
                <div class="tp-caption tp-resizeme icon-home" data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                     data-responsive="on" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']" data-voffset="['110','110','50','30']" data-width="['770', '770', '770', '380']">
                    <img src="/uploads/banner_icon/{{$row->icon}}" alt="">
                </div>
                <div class="tp-caption tp-resizeme slide-title" data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                     data-responsive="on" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['center','center','center','center']" data-voffset="['-43','-43','-53','-53']" data-width="['770', '770', '770', '380']"
                     data-fontsize="['30','30','20','20']">
                    {{$row->title}}
                </div>
                <div class="tp-caption tp-resizeme slide-desc" data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                     data-responsive="on" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['center','center','center','center']" data-voffset="['4', '4', '4', '4']" data-width="['770', '770', '770', '380']"
                     data-height="['auto']" data-whitespace="normal" data-type="text" data-textalign="['center']">
                    {{$row->content}}
                </div>
                <div class="tp-caption tp-resizeme slide-button" data-frames='[{"delay":0,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                     data-responsive="on" data-x="['center','center','center','center']" data-hoffset="['0', '0', '0', '0']" data-y="['center','center','center','center']" data-voffset="['90', '90', '90', '90']" data-width="['770', '770', '770', '380']">
                    <a href="{{$row->btn_1_url}}" class="btn-global btn-yellow">{{$row->btn_1_name}}</a>
                    <a href="{{$row->btn_2_url}}" class="btn-global btn-global-hover btn-global-border-hover">{{$row->btn_2_name}}</a>
                </div>
            </li>
            @endforeach
        </ul>
        <!-- <div class="tp-bannertimer" style="height: 10px; background: rgba(255,255,255,0.25);"></div> -->
    </div>
    <!-- END REVOLUTION SLIDER -->
</section>
<!-- Request quote -->
<section class="home5-aboutus background-grey">
    <div class="container">
        <div class="row">
            <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12 home5-aboutus-content">
                <div class="section-title-left">
                    <h2>Welcome to the HVS International Alliance</h2>
                </div>
                <div class="desc">
                    <p>
                        The HVS International Alliance is a voluntary association of Heavy Vehicle Simulator users, which has been established to promote interaction between users and to co-ordinate testing and the dissemination of test results. This website has been established to capture pertinent information and to display meta-data in respect of the results of tests. Detailed results can be obtained by visiting individual websites or by downloading some of the limited results available on this website under test information.
                    </p>
                </div>
            </div>
            <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12 home5-video">
                <img src="{{asset('Front-Assets/images/home/company-news-4.jpg')}}" alt="">

            </div>
        </div>
    </div>
</section>

<!-- Our Services -->
<section class="services p-t-80 p-b-100">
    <h4 class="heading-section">Heading section</h4>
    <div class="container">
        <div class="services-content">

            <div class="row" style="margin-bottom: 3%">
                <article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 item">
                    <figure class="figure-hover">
                        <a href="http://csir.co.za">
                            <img src="{{asset('Front-Assets/images/home/new/1.jpg')}}" alt="">
                            <span class="overlay"></span>
                        </a>
                        <span>+</span>
                    </figure>
                    <div class="info">
                        <div class="title">
                            <h3 class="title-bold">
                                <a href="http://csir.co.za" class="font-color-black">
                                    South Africa: <br/><small>CSIR and GPDRT</small>
                                </a>
                            </h3>
                        </div>
                        <div class="desc">
                        </div>
                    </div>
                </article>
                <article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 item">
                    <figure class="figure-hover">
                        <a href="http://www.ucprc.ucdavis.edu">
                            <img src="{{asset('Front-Assets/images/home/new/2.jpg')}}" alt="">
                            <span class="overlay"></span>
                        </a>
                        <span>+</span>
                    </figure>
                    <div class="info">
                        <div class="title">
                            <h3 class="title-bold">
                                <a href="http://www.ucprc.ucdavis.edu" class="font-color-black">
                                    USA California:<br/> <small>University of California PRC</small>
                                </a>
                            </h3>
                        </div>
                        <div class="desc">
                            
                        </div>
                    </div>
                </article>
                <article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 item">
                    <figure class="figure-hover">
                        <a href="https://www.fdot.gov">
                            <img src="{{asset('Front-Assets/images/home/new/3.jpg')}}" alt="">
                            <span class="overlay"></span>
                        </a>
                        <span>+</span>
                    </figure>
                    <div class="info">
                        <div class="title">
                            <h3 class="title-bold">
                                <a href="https://www.fdot.gov" class="font-color-black">
                                    USA Florida:<br/> <small>FDOT</small>
                                </a>
                            </h3>
                        </div>
                        <div class="desc">
                            
                        </div>
                    </div>
                </article>
                <article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 item">
                    <figure class="figure-hover">
                        <a href="https://www.erdc.usace.army.mil/Media/Fact-Sheets/Fact-Sheet-Article-View/Article/476661/materials-testing-center/">
                            <img src="{{asset('Front-Assets/images/home/new/4.jpg')}}" alt="">
                            <span class="overlay"></span>
                        </a>
                        <span>+</span>
                    </figure>
                    <div class="info">
                        <div class="title">
                            <h3 class="title-bold">
                                <a href="https://www.erdc.usace.army.mil/Media/Fact-Sheets/Fact-Sheet-Article-View/Article/476661/materials-testing-center/" class="font-color-black">
                                    USA CoE: <br /> <small>US Army Corps of Engineers <br/> ERDC – WES</small>
                                </a>
                            </h3>
                        </div>
                        <div class="desc">
                            
                        </div>
                    </div>
                </article>
            </div>

            <div class="row" style="margin-bottom: 3%">
                <article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 item">
                    <figure class="figure-hover">
                        <a href="http://www.crridom.gov.in/content/heavy-vehicle-simulatorhvs-accelerated-pavement-testing-facility">
                            <img src="{{asset('Front-Assets/images/home/new/5.jpg')}}" alt="">
                            <span class="overlay"></span>
                        </a>
                        <span>+</span>
                    </figure>
                    <div class="info">
                        <div class="title">
                            <h3 class="title-bold">
                                <a href="http://www.crridom.gov.in/content/heavy-vehicle-simulatorhvs-accelerated-pavement-testing-facility" class="font-color-black">
                                    India: <br/> <small>CSIR CRRI</small>
                                </a>
                            </h3>
                        </div>
                        <div class="desc">
                           
                        </div>
                    </div>
                </article>
                <article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 item">
                    <figure class="figure-hover">
                        <a href="http://zsb.chd.edu.cn/">
                            <img src="{{asset('Front-Assets/images/home/new/6.jpg')}}" alt="">
                            <span class="overlay"></span>
                        </a>
                        <span>+</span>
                    </figure>
                    <div class="info">
                        <div class="title">
                            <h3 class="title-bold">
                                <a href="http://zsb.chd.edu.cn/" class="font-color-black">
                                    China:<br/> <small>University of Chang`an, Xian</small>
                                </a>
                            </h3>
                        </div>
                        <div class="desc">
                            
                        </div>
                    </div>
                </article>
                <article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 item">
                    <figure class="figure-hover">
                        <a href="#">
                            <img src="{{asset('Front-Assets/images/home/new/7.jpg')}}" alt="">
                            <span class="overlay"></span>
                        </a>
                        <span>+</span>
                    </figure>
                    <div class="info">
                        <div class="title">
                            <h3 class="title-bold">
                                <a href="#" class="font-color-black">
                                    Indonesia <br/>
                                    <small style="color: #ff341d">No Link Available yet</small>
                                </a>
                            </h3>
                        </div>
                        <div class="desc">
                            
                        </div>
                    </div>
                </article>
                <article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 item">
                    <figure class="figure-hover">
                        <a href="https://www.erdc.usace.army.mil/Media/Fact-Sheets/Fact-Sheet-Article-View/Article/476661/materials-testing-center/">
                            <img src="{{asset('Front-Assets/images/home/new/8.jpg')}}" alt="">
                            <span class="overlay"></span>
                        </a>
                        <span>+</span>
                    </figure>
                    <div class="info">
                        <div class="title">
                            <h3 class="title-bold">
                                <a href="https://www.erdc.usace.army.mil/Media/Fact-Sheets/Fact-Sheet-Article-View/Article/476661/materials-testing-center/" class="font-color-black">
                                    USA CoE:<br/><small>US Army Corps of Engineers <br/> Cold Regions Research Facility</small>
                                </a>
                            </h3>
                        </div>
                        <div class="desc">
                            
                        </div>
                    </div>
                </article>
            </div>

            <div class="row" style="margin-bottom: 3%">
                <article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 item">
                    <figure class="figure-hover">
                        <a href="https://www.airporttech.tc.faa.gov/">
                            <img src="{{asset('Front-Assets/images/home/new/9.jpg')}}" alt="">
                            <span class="overlay"></span>
                        </a>
                        <span>+</span>
                    </figure>
                    <div class="info">
                        <div class="title">
                            <h3 class="title-bold">
                                <a href="https://www.airporttech.tc.faa.gov/" class="font-color-black">
                                    USA Florida:<br/><small>FAA</small>
                                </a>
                            </h3>
                        </div>
                        <div class="desc">
                            
                        </div>
                    </div>
                </article>
                <article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 item">
                    <figure class="figure-hover">
                        <a href="http://www.kict.re.kr/">
                            <img src="{{asset('Front-Assets/images/home/new/10.jpg')}}" alt="">
                            <span class="overlay"></span>
                        </a>
                        <span>+</span>
                    </figure>
                    <div class="info">
                        <div class="title">
                            <h3 class="title-bold">
                                <a href="http://www.kict.re.kr/" class="font-color-black">
                                    South Korea:<br><small>KICT</small>
                                </a>
                            </h3>
                        </div>
                        <div class="desc">
                            
                        </div>
                    </div>
                </article>
                <article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 item">
                    <figure class="figure-hover">
                        <a href="https://www.vti.se/en/Research-areas/Accelerated-testing-road-construction/">
                            <img src="{{asset('Front-Assets/images/home/new/11.jpg')}}" alt="">
                            <span class="overlay"></span>
                        </a>
                        <span>+</span>
                    </figure>
                    <div class="info">
                        <div class="title">
                            <h3 class="title-bold">
                                <a href="https://www.vti.se/en/Research-areas/Accelerated-testing-road-construction/" class="font-color-black">
                                    Sweden:<br /><small>VTI</small>
                                </a>
                            </h3>
                        </div>
                        <div class="desc">
                            
                        </div>
                    </div>
                </article>
                <article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 item">
                    <figure class="figure-hover">
                        <a href="http://www.lanamme.ucr.ac.cr/">
                            <img src="{{asset('Front-Assets/images/home/new/12.jpg')}}" alt="">
                            <span class="overlay"></span>
                        </a>
                        <span>+</span>
                    </figure>
                    <div class="info">
                        <div class="title">
                            <h3 class="title-bold">
                                <a href="http://www.lanamme.ucr.ac.cr/" class="font-color-black">
                                    Costa Rica:<br/><small>Lanamme</small>
                                </a>
                            </h3>
                        </div>
                        <div class="desc">
                            
                        </div>
                    </div>
                </article>
            </div>

            <div class="row" style="margin-bottom: 3%">
                <article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 item">
                    <figure class="figure-hover">
                        <a href="https://www.setsintl.net/page/home">
                            <img src="{{asset('Front-Assets/images/home/new/13.jpg')}}" alt="">
                            <span class="overlay"></span>
                        </a>
                        <span>+</span>
                    </figure>
                    <div class="info">
                        <div class="title">
                            <h3 class="title-bold">
                                <a href="https://www.setsintl.net/page/home" class="font-color-black">
                                    Saudi Arabia:<br/><small>MOT</small>
                                </a>
                            </h3>
                        </div>
                        <div class="desc">
                            
                        </div>
                    </div>
                </article>
                <article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 item">
                    <figure class="figure-hover">
                        <a href="https://www.gob.mx/imt">
                            <img src="{{asset('Front-Assets/images/home/new/14.jpg')}}" alt="">
                            <span class="overlay"></span>
                        </a>
                        <span>+</span>
                    </figure>
                    <div class="info">
                        <div class="title">
                            <h3 class="title-bold">
                                <a href="https://www.gob.mx/imt" class="font-color-black">
                                    Mexico<br /><small>IMT</small>
                                </a>
                            </h3>
                        </div>
                        <div class="desc">
                            
                        </div>
                    </div>
                </article>
                <article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 item">
                    <figure class="figure-hover">
                        <a href="http://www.virginiadot.org/">
                            <img src="{{asset('Front-Assets/images/home/new/15.jpg')}}" alt="">
                            <span class="overlay"></span>
                        </a>
                        <span>+</span>
                    </figure>
                    <div class="info">
                        <div class="title">
                            <h3 class="title-bold">
                                <a href="http://www.virginiadot.org/" class="font-color-black">
                                    USA Virginia:<br/><small>VTTI</small>
                                </a>
                            </h3>
                        </div>
                        <div class="desc">
                            
                        </div>
                    </div>
                </article>
                <article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 item">
                    <figure class="figure-hover">
                        <a href="https://rowancreates.org/">
                            <img src="{{asset('Front-Assets/images/home/new/9.jpg')}}" alt="">
                            <span class="overlay"></span>
                        </a>
                        <span>+</span>
                    </figure>
                    <div class="info">
                        <div class="title">
                            <h3 class="title-bold">
                                <a href="https://rowancreates.org/" class="font-color-black">
                                    USA:<br /><small>Rowan University<br/> CREATEs</small>
                                </a>
                            </h3>
                        </div>
                        <div class="desc">
                            
                        </div>
                    </div>
                </article>
            </div>

            <div class="row" style="margin-bottom: 3%">
                <article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 item">
                    <figure class="figure-hover">
                        <a href="https://www.argentina.gob.ar/transporte/vialidad-nacional">
                            <img src="{{asset('Front-Assets/images/home/new/5.jpg')}}" alt="">
                            <span class="overlay"></span>
                        </a>
                        <span>+</span>
                    </figure>
                    <div class="info">
                        <div class="title">
                            <h3 class="title-bold">
                                <a href="https://www.argentina.gob.ar/transporte/vialidad-nacional" class="font-color-black">
                                    Argentina:<br/><small>DNV</small>
                                </a>
                            </h3>
                        </div>
                        <div class="desc">

                        </div>
                    </div>
                </article>
            </div>

        </div>
    </div>
</section>



<a href="#" class="totop">Back to top</a>
<div class="second">
    <div class="container">
        <div class="row">
            <!-- Clients -->
            <div class="col-md-12" style="padding-bottom: 0px">
                {!!$cms->content!!}
            </div>
        </div>
    </div>
</div>
