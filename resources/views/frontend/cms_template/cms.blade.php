

<section class="about-us">
    <div class="container">
        <div class="about-us-content">
            {!!$cms->content!!}
        </div>
</section>

<a href="#" class="totop">Back to top</a>