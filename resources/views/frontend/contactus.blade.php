@extends('layouts.frontend.main')

@section('content')
    <section class="heading-page">
        <img src="{{asset('Front-Assets/images/contact/contact-heading.jpg')}}" alt="">
        <div class="heading-page-content position-center">
            <div class="page-title">
                <h1>Contact us</h1>
            </div>
            <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Contact</li>
                </ul>
            </nav>
        </div>
    </section>
    <div class="contact-us">
        <div class="container">
            <div class="contact-title">
                <h2>Contact Us</h2>
            </div>
            <div class="contact-us-content">
                <div class="row">
                    <div class="col-xl-11 col-lg-11 col-md-11 col-sm-12 col-12 contact-form">
                        <form  method="POST" action="{{route('post.contact-message')}}">
                                @csrf
                                @include('layouts.frontend.inc.response')
                            <div class="wrap-group">
                                <label for="name">Your name</label>
                                <input type="text" name="name" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{old('name')}}" id="name" required="" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;">
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                              <strong>{{ $errors->first('name') }} </strong>
                                            </span>
                                @endif
                            </div>
                            <div class="wrap-group">
                                <label for="phone">Your phone</label>
                                <input type="text" class="{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" value="{{old('phone_number')}}" id="phone" name="phone_number" required>
                                @if ($errors->has('phone_number'))
                                    <span class="invalid-feedback">
                                              <strong>{{ $errors->first('phone_number') }} </strong>
                                            </span>
                                @endif
                            </div>
                            <div class="wrap-group">
                                <label for="email">Your email</label>
                                <input type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{old('email')}} " name="email" id="email" placeholder="Ex. Julian.smith@email.com" required>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                              <strong>{{ $errors->first('email') }} </strong>
                                            </span>
                                @endif
                            </div>
                            <div class="wrap-group">
                                <label for="mes">Message</label>
                                <textarea name="message" id="mes" required="" class="{{ $errors->has('message') ? ' is-invalid' : '' }}" >{{old('message')}}</textarea>
                                @if ($errors->has('message'))
                                    <span class="invalid-feedback">
                                              <strong>{{ $errors->first('message') }} </strong>
                                            </span>
                                @endif
                            </div>
                            <div class="wrap-group">
                                <label for="mes">Select Country you want to contact</label>
                                <select title="select" name="country_email" class="form-control {{ $errors->has('country_email') ? ' is-invalid' : '' }}" id="sel1" >
                                    <option value="" selected disabled="">Select Country</option>
                                    <option value="info@hvsia.com">South Africa</option>
                                    <option value="info@hvsia.com">USA California</option>
                                    <option value="info@hvsia.com">Sweden</option>
                                    <option value="info@hvsia.com">USA Florida</option>
                                    <option value="info@hvsia.com">USA Vicksburg</option>
                                    <option value="info@hvsia.com">USA Hanover</option>
                                    <option value="info@hvsia.com">USA Virginia</option>
                                    <option value="info@hvsia.com">USA Atlantic City </option>
                                    <option value="info@hvsia.com">USA Costa Rica</option>
                                    <option value="info@hvsia.com">China</option>
                                    <option value="info@hvsia.com">India</option>
                                    <option value="info@hvsia.com">Indonesia</option>
                                    <option value="info@hvsia.com">South Korea</option>
                                    <option value="info@hvsia.com">Mexico</option>
                                    <option value="info@hvsia.com">Saudi-Arabia</option>
                                </select>
                                @if ($errors->has('country_email'))
                                    <span class="invalid-feedback">
                                              <strong>{{ $errors->first('country_email') }} </strong>
                                            </span>
                                @endif
                            </div>
                            <div class="wrap-group">
                                <input type="submit" value="submit" class="btn-global btn-yellow">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
