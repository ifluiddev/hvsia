<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('Back-Assets/assets/images/favicon.png')}}">
    <title>HVSIA Register</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{asset('Back-Assets/assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset('Back-Assets/css/style.css')}}" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="{{asset('Back-Assets/css/colors/blue.css')}}" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
</div>
<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<section id="wrapper">
    <div class="login-register" style="background-image:url({{asset('Back-Assets/assets/images/background/login.jpg')}});">
        <div class="login-box card">
            <div class="card-body">
                <div align="center"><img src="{{asset('Front-Assets/images/logo.png')}}" alt="Consulting" /></div>
                <form class="form-horizontal form-material" id="loginform" action="{{route('register')}}" method="post">

                    <h3 class="box-title m-b-20" align="center">Sign Up</h3>
                    @include('layouts.frontend.inc.response')
                    @csrf
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" type="text" name="first_name" required="" placeholder="First Name" value="{{old('first_name')}}">
                            @if ($errors->has('first_name'))
                                <span class="invalid-feedback">
                                              <strong>{{ $errors->first('first_name') }} </strong>
                                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input type="text" id="last-name" name="last_name" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" required value="{{old('last_name')}}" placeholder="Last Name">
                            @if ($errors->has('last_name'))
                                <span class="invalid-feedback">
                                              <strong>{{ $errors->first('last_name') }} </strong>
                                            </span>
                            @endif

                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input type="text" id="email" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" required value="{{old('email')}}" placeholder="Email">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                              <strong>{{ $errors->first('email') }} </strong>
                                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" name="password" required="" placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                              <strong>{{ $errors->first('password') }} </strong>
                                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" name="password_confirmation" required="" placeholder="Confirm Password">
                            @if ($errors->has('password_confirmation'))
                                <span class="invalid-feedback">
                                              <strong>{{ $errors->first('password_confirmation') }} </strong>
                                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="checkbox checkbox-success">
                                <input id="checkbox-signup" type="checkbox" required>
                                <label for="checkbox-signup"> I agree to all <a href="#">Terms</a></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Sign Up</button>
                        </div>
                    </div>
                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">
                            <div>Already have an account? <a href="/login" class="text-info m-l-5"><b>Sign In</b></a></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="{{asset('Back-Assets/assets/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="{{asset('Back-Assets/assets/plugins/bootstrap/js/popper.min.js')}}"></script>
<script src="{{asset('Back-Assets/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="{{asset('Back-Assets/js/jquery.slimscroll.js')}}"></script>
<!--Wave Effects -->
<script src="{{asset('Back-Assets/js/waves.js')}}"></script>
<!--Menu sidebar -->
<script src="{{asset('Back-Assets/js/sidebarmenu.js')}}"></script>
<!--stickey kit -->
<script src="{{asset('Back-Assets/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
<script src="{{asset('Back-Assets/assets/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!--Custom JavaScript -->
<script src="{{asset('Back-Assets/js/custom.min.js')}}"></script>
<!-- ============================================================== -->
<!-- Style switcher -->
<!-- ============================================================== -->
<script src="{{asset('Back-Assets/assets/plugins/styleswitcher/jQuery.style.switcher.js')}}"></script>
</body>

</html>