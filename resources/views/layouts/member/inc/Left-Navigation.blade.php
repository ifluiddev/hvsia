<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->
        <div class="user-profile">
            <!-- User profile image -->
            <div class="profile-img"> <img src="{{asset('Back-Assets/assets/images/users/7.jpg')}}" alt="user" />
                <!-- this is blinking heartbit-->
                <div class="notify setpos"> <span class="heartbit"></span> <span class="point"></span> </div>
            </div>
            <!-- User profile text-->
            <div class="profile-text">
                <h5>{{auth()->user()->getFullname()}}</h5>

                <a href="pages-login.html" class="" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a>

            </div>
        </div>
        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-devider"></li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="../" aria-expanded="false">
                        <i class="mdi mdi-home"></i>
                        <span class="hide-menu">Home</span>
                    </a>

                </li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="{{route('member.show.index')}}" aria-expanded="false">
                        <i class="mdi mdi-gauge"></i>
                        <span class="hide-menu">Dashboard</span>
                    </a>

                </li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="{{route('member.meetings.index')}}" aria-expanded="false">
                        <i class="mdi mdi-calendar"></i>
                        <span class="hide-menu">Meetings</span>
                    </a>

                </li>
                <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">Matrix</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('member.matrix.tables.index')}}">Matrix Table</a></li>
                        <li><a href="{{route('member.tests.index')}}">My Tests</a></li>
                        <li><a href="{{route('member.matrices.index')}}">Activity Matrix</a></li>

                    </ul>
                </li>


            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>