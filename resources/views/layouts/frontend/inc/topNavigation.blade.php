<header class="header header-1" style="z-index: 999999">
    <div class="hidden-tablet-landscape-up header-mobile">
        <div class="header-top-mobile">
            <div class="container-fluid">
                <div class="logo">
                    <a href="{{route('frontend-home')}}">
                        <img src="{{asset('Front-Assets/images/logo.png')}}" alt="Consulting" />
                    </a>
                </div>
                <button class="hamburger hamburger--spin hidden-tablet-landscape-up" id="toggle-icon">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                </button>
            </div>

        </div>

        <div class="au-nav-mobile" >
            <nav class="menu-mobile">
                <div>
                    <ul class="au-navbar-menu">

                        <!--=========== Single Menu ===============-->
                        @foreach(\App\CMS::menus() as $row)
                            <li class="menu-item curent-menu-item">
                                <a href="
                                    @if($row->redirect_url)
                                {{$row->redirect_url}}
                                @else
                                @if($row->is_index_page)
                                {{url('/')}}
                                @else
                                {{url('/page/'.$row->slug)}}
                                @endif
                                @endif
                                        " style="color: grey!important;"><b>{{$row->page_name}}</b>

                                    <div class="skewed_slide"></div>
                                </a>
                                @if($row->sub_menus()->count() > 0)
                                    <span class="arrow">
                                    <i></i>
                                </span>
                                    <ul class="sub-menu">
                                        @foreach($row->sub_menus() as $child)

                                            <li class="menu-item">
                                                <a href="
                                                    @if($child->redirect_url)
                                                {{$child->redirect_url}}
                                                @else
                                                @if($child->is_index_page)
                                                {{url('/')}}
                                                @else
                                                {{url('/page/'.$child->slug)}}
                                                @endif
                                                @endif
                                                        " style="color: grey!important;">{{$child->page_name}}

                                                </a>


                                            </li>


                                        @endforeach
                                    </ul>
                                @endif

                            </li>

                        @endforeach

                        @auth
                            <li class="menu-item"><a href="{{route('member.show.index')}}">Member
                                </a>
                                <span class="arrow">
                                    <i></i>
                                </span>
                                <ul class="sub-menu">
                                    <li class="menu-item"><b><a href="{{route('member.show.index')}}"><i></i>Dashboard</a></b></li>
                                    <li class="menu-item"><b><a href="{{route('member.meetings.index')}}"><i></i>Upcoming Meetings</a></b></li>
                                    <li class="menu-item"><b><a href="{{route('member.matrix.tables.index')}}"><i></i>Matrix Table</a></b></li>
                                    <li class="menu-item">
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </li>
                            @else
                                <li class="menu-item"><a href="#"><b>Member</b>
                                    </a>
                                    <span class="arrow">
                                    <i></i>
                                </span>
                                    <ul class="sub-menu">

                                        <li class="menu-item"><b><a href="{{route('login')}}"><i></i>Login</a></b></li>


                                    </ul>
                                </li>
                            @endauth
                    </ul>

                </div>
            </nav>
        </div>
        <!-- <div class="clear"></div> -->
        <div class="header-top">
            <div class="container-fluid">
                <div class="header-top-content display-flex">
                    <div class="header-top-desc">

                    </div>

                </div>
            </div>
        </div>
        <div class="clear hidden-tablet-landscape" ></div>
    </div>
    <div class="hidden-tablet-landscape   header-top">
        <div class="container hidden-tablet-landscape">
            <div class="header-top-content display-flex hidden-tablet-landscape">
                <div class="header-top-desc">
                    A forum for HVS owners and operators.
                </div>
                <div class="header-top-info">
                    <ul>


                        <li style="padding-top: 7px">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                            <span>info@hvsia.com</span>
                        </li>
                        <li>
                               {{-- <input class="search_input" type="text" name="" placeholder="Search...">
                                <a href="#" class="search_icon"><i class="fas fa-search"></i></a>--}}
                                <form class="searchbar" method="GET" action="{{url('/search')}}" >
                                    <input type="text" name="search" class="search_input" placeholder="Keyword Search...">
                                    <button  class="search_icon"><i class="fas fa-search"></i></button>
                                </form>

                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="hidden-tablet-landscape header-bottom" id="js-navbar-fixed">
        <div class="container">
            <div class="header-bottom-content display-flex">
                <div class="logo">
                    <a href="{{route('frontend-home')}}"><img src="{{asset('Front-Assets/images/logo.png')}}" alt=""></a>
                </div>
                <nav class="menu">
                    <div>
                        <ul class="menu-primary">

                            <!--=========== Single Menu ===============-->
                            @foreach(\App\CMS::menus() as $row)
                                <li class="menu-item curent-menu-item">
                                    <a href="
                                    @if($row->redirect_url)
                                    {{$row->redirect_url}}
                                    @else
                                    @if($row->is_index_page)
                                    {{url('/')}}
                                    @else
                                    {{url('/page/'.$row->slug)}}
                                    @endif
                                    @endif
                                            " style="color: grey!important;"><b>{{$row->page_name}}</b>

                                        <div class="skewed_slide"></div>
                                    </a>
                                    @if($row->sub_menus()->count() > 0)
                                        <ul class="sub-menu">
                                            @foreach($row->sub_menus() as $child)

                                                <li class="menu-item">
                                                    <a href="
                                                    @if($child->redirect_url)
                                                    {{$child->redirect_url}}
                                                    @else
                                                    @if($child->is_index_page)
                                                    {{url('/')}}
                                                    @else
                                                    {{url('/page/'.$child->slug)}}
                                                    @endif
                                                    @endif
                                                            " style="color: grey!important;">{{$child->page_name}}

                                                        <div class="skewed_slide"></div>
                                                    </a>


                                                </li>


                                            @endforeach
                                        </ul>
                                    @endif

                                </li>
                            @endforeach

                            @auth
                                <li class="menu-item"><a href="{{route('member.show.index')}}">Member
                                    </a>
                                    <span class="arrow">
                                    <i></i>
                                </span>
                                    <ul class="sub-menu">
                                        <li class="menu-item"><b><a href="{{route('member.show.index')}}"><i></i>Dashboard</a></b></li>
                                        <li class="menu-item"><b><a href="{{route('member.meetings.index')}}"><i></i>Upcoming Meetings</a></b></li>
                                        <li class="menu-item"><b><a href="{{route('member.matrix.tables.index')}}"><i></i>Matrix Table</a></b></li>
                                        <li class="menu-item">
                                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                                @else
                                    <li class="menu-item"><a href="#" style="color: grey!important;>"<b>Member</b>
                                        </a>
                                        <ul class="sub-menu">
                                            <li class="menu-item"><b><a href="{{route('login')}}" style="color: grey!important;"><i></i>Login</a></b></li>


                                        </ul>
                                    </li>
                                    @endauth
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>