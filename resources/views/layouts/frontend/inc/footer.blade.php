<hr/>
<footer class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="footer-top-content">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 footer-info">
                        <div class="footer-logo">
                            <a href="{{route('frontend-home')}}"><img src="{{asset('Front-Assets/images/logo.png')}}" alt="Bricktower"></a>
                        </div>
                        <div class="info-desc">
                           A forum for HVS owners and operators.
                        </div>
                        <div class="socials">
                            <ul>
                                <li><a href="https://www.facebook.com/HVSIA/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="https://twitter.com/hvsia"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="https://www.linkedin.com/in/hvs-ia-884231108/"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="#" hidden><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 footer-links">
                        <div class="footer-title">
                            <h3>useful links</h3>
                        </div>
                        <div class="footer-links-menu">
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li><a href="/page/about-us">About Us</a></li>
                                <li><a href="/gallery">Gallery</a></li>
                            </ul>
                            <ul>
                                <li><a href="/contact-us">Contact us</a></li>

                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 footer-contact">
                        <div class="footer-title">
                            <h3>Contact us</h3>
                        </div>
                        <ul>

                            <li>
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                <span>info@hvsia.com</span>
                            </li>
                            <li>

                                <span>Managed by the CSIR, South Africa</span>
                            </li>
                        </ul>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="footer-bottom-content display-flex">
                <div class="copyright">
                    Copyright © 2019 CSIR Developed by <span>Tec-Novation</span>
                </div>

            </div>
        </div>
    </div>
</footer>