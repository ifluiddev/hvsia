<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->
        <div class="user-profile">
            <!-- User profile image -->
            <div class="profile-img"> <img src="{{asset('Back-Assets/assets/images/users/7.jpg')}}" alt="user" />
                <!-- this is blinking heartbit-->
                <div class="notify setpos"> <span class="heartbit"></span> <span class="point"></span> </div>
            </div>
            <!-- User profile text-->
            <div class="profile-text">
                <h5>{{auth()->user()->name}}</h5>
               
                <a href="{{route('admin.logout')}}" class="" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a>

            </div>
        </div>
        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">

                <li class="nav-devider"></li>
                <li class="nav-small-cap">Content Management</li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="../" aria-expanded="false">
                        <i class="mdi mdi-home"></i>
                        <span class="hide-menu">Home</span>
                    </a>

                </li>
                <li>
                    <a class="has-arrow waves-effect waves-dark" href="{{route('admin.show.index')}}" aria-expanded="false">
                        <i class="mdi mdi-gauge"></i>
                        <span class="hide-menu">Dashboard</span>
                    </a>

                </li>
                <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">Content</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('admin.show.manage-banner')}}">Manage Banners</a></li>
                        <li><a href="{{route('admin.show.manage-cms')}}">Manage Content</a></li>
                        <li><a href="{{route('admin.show.manage-gallery')}}">Manage Gallery</a></li>
                    </ul>
                </li>
                <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="fa fa-location-arrow"></i><span class="hide-menu">Locations</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('admin.locations.index')}}">Manage</a></li>
                        <li><a href="{{route('admin.locations.create')}}">Create New</a></li>
                    </ul>
                </li>
                <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-account"></i><span class="hide-menu">Members</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('admin.users.index')}}">Manage</a></li>
                        <li><a href="{{route('admin.users.create')}}">Create New</a></li>
                    </ul>
                </li>
                <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-shield"></i><span class="hide-menu">Administrators</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('admin.admins.index')}}">Manage</a></li>
                        <li><a href="{{route('admin.admins.create')}}">Create New</a></li>
                    </ul>
                </li>
                <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-calendar"></i><span class="hide-menu">Meetings</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('admin.meetings.index')}}">Manage Meetings</a></li>
                        <li><a href="{{route('admin.meetings.create')}}">Create New Meeting</a></li>
                    </ul>
                </li>

                <li class="nav-small-cap">Matrix System</li>
                <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-chip"></i><span class="hide-menu">Matrix</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('admin.tests.index')}}">Matrix</a></li>
                        <li><a href="{{route('admin.competency-areas.index')}}">Competencies</a></li>
                        <li><a href="{{route('admin.focus-areas.index')}}">Focus Areas</a></li>
                        <li><a href="{{route('admin.tests.create')}}">New Test</a></li>
                        <li><a href="{{route('admin.matrix.tables.index')}}">Matrix Table</a></li>
                    </ul>
                </li>

            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>