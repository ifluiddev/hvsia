<header class="header header-1">
    <div class="hidden-tablet-landscape-up header-mobile">
        <div class="header-top-mobile">
            <div class="container-fluid">
                <div class="logo">
                    <a href="{{route('frontend-home')}}">
                        <img src="{{asset('Front-Assets/images/logo.png')}}" alt="Consulting" />
                    </a>
                </div>
            </div>
        </div>
        <div class="au-nav-mobile">
            <nav class="menu-mobile">
                <div>
                    {{--<ul class="au-navbar-menu">
                        <li class="menu-item curent-menu-item">
                            <a href="index.html" class="font-color-black">Home</a>
                            <span class="arrow">
                                    <i></i>
                                </span>
                            <ul class="sub-menu">
                                <li class="menu-item"><a href="/" class="font-color-black">Home</a></li>
                                <li class="menu-item"><a href="/" class="font-color-black">Constitution</a></li>
                                <li class="menu-item"><a href="/" class="font-color-black">User Guide</a></li>
                                <li class="menu-item"><a href="/" class="font-color-black">Acronyms</a></li>
                                <li class="menu-item"><a href="/" class="font-color-black">Countries</a></li>
                            </ul>
                        </li>
                        <li class="menu-item">
                            <a href="services1.html" class="font-color-black">Locations</a>
                        </li>
                        <li class="menu-item">
                            <a href="projects1.html" class="font-color-black">Projects</a>
                            <span class="arrow">
                                    <i></i>
                                </span>
                            <ul class="sub-menu">
                                <li class="menu-item"><a href="projects1.html" class="font-color-black">projects 1</a></li>
                                <li class="menu-item"><a href="projects2.html" class="font-color-black">projects 2</a></li>
                                <li class="menu-item"><a href="projects3.html" class="font-color-black">projects 3</a></li>
                                <li class="menu-item"><a href="projects4.html" class="font-color-black">projects 4</a></li>
                                <li class="menu-item"><a href="projects5.html" class="font-color-black">projects 5</a></li>
                            </ul>
                        </li>
                        <li class="menu-item">
                            <a href="aboutus.html" class="font-color-black">About</a>
                        </li>
                        <li class="menu-item">
                            <a href="shop.html" class="font-color-black">Shop</a>
                            <span class="arrow">
                                    <i></i>
                                </span>
                            <ul class="sub-menu">
                                <li class="menu-item"><a href="shop.html" class="font-color-black">Shop 1</a></li>
                                <li class="menu-item"><a href="shop2.html" class="font-color-black">Shop 2</a></li>
                            </ul>
                        </li>
                        <li class="menu-item">
                            <a href="contactus.html" class="font-color-black">Contact us</a>
                        </li>
                        <li class="menu-item">
                            <a href="blog.html" class="font-color-black">Blog</a>
                            <span class="arrow">
                                    <i></i>
                                </span>
                            <ul class="sub-menu">
                                <li class="menu-item"><a href="blog.html" class="font-color-black">Blog 1</a></li>
                                <li class="menu-item"><a href="blog2.html" class="font-color-black">Blog 2</a></li>
                                <li class="menu-item"><a href="blog3.html" class="font-color-black">Blog 3</a></li>
                            </ul>
                        </li>
                    </ul>--}}
                    <ul class="au-navbar-menu">

                        <!--=========== Single Menu ===============-->
                        @foreach(\App\CMS::menus() as $row)
                            <li class="menu-item curent-menu-item">
                                <a href="
                                    @if($row->redirect_url)
                                {{$row->redirect_url}}
                                @else
                                @if($row->is_index_page)
                                {{url('/')}}
                                @else
                                {{url('/page/'.$row->slug)}}
                                @endif
                                @endif
                                        "><b>{{$row->page_name}}</b>

                                    <div class="skewed_slide"></div>
                                </a>
                                @if($row->sub_menus()->count() > 0)
                                    <ul class="sub-menu">
                                        @foreach($row->sub_menus() as $child)

                                            <li class="menu-item">
                                                <a href="
                                                    @if($child->redirect_url)
                                                {{$child->redirect_url}}
                                                @else
                                                @if($child->is_index_page)
                                                {{url('/')}}
                                                @else
                                                {{url('/page/'.$child->slug)}}
                                                @endif
                                                @endif
                                                        ">{{$child->page_name}}

                                                    <div class="skewed_slide"></div>
                                                </a>


                                            </li>


                                        @endforeach
                                    </ul>
                                @endif

                            </li>
                        @endforeach
                               {{-- <li class="mega-dropdown mg-3col-mid"><a href="#"><b>Login</b>
                                        <div class="skewed_slide"></div>
                                    </a>
                                    <ul class="submenu-container">
                                        <!-- row 1 -->
                                        <div class="col-md-6">
                                            <a class="owl-nav-title" href="#">Login & Register</a>
                                            <ul class="owl-nav-list">
                                                <li><b><a href="{{route('login')}}"><i></i>Login</a></b></li>
                                                <li><a href="{{route('register')}}"><i></i> Register</a></li>
                                            </ul>
                                        </div>


                                    </ul>
                                </li>--}}
                    </ul>
                </div>
            </nav>
        </div>
        <!-- <div class="clear"></div> -->
        <div class="header-top">
            <div class="container-fluid">
                <div class="header-top-content display-flex">
                    <div class="header-top-desc">
                        We Are Specialists In Construction And Architecture!
                    </div>
                    <div class="header-top-info">
                        <ul>
                            <li>
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <span>(+1) 115 221 2564</span>
                            </li>
                            <li>
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                <span>info@example.com</span>
                            </li>
                            <li>
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <span>Building Cror, New York, NY 93459</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="hidden-tablet-landscape header-top">
        <div class="container">
            <div class="header-top-content display-flex">
                <div class="header-top-desc">
                    We Are Specialists In Heavy Vehicle Simulation !
                </div>
                <div class="header-top-info">
                    <ul>

                        <li>
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                            <span>Lplessis@csir.co.za  </span>
                        </li>
                        <li>
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            <span>South Africa, CSIR/Gautrans</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="hidden-tablet-landscape header-bottom" id="js-navbar-fixed">
        <div class="container">
            <div class="header-bottom-content display-flex">
                <div class="logo">
                    <a href="index.html"><img src="{{asset('Front-Assets/images/logo.png')}}" alt=""></a>
                </div>
                <nav class="menu">
                    <div>
                        {{--<ul class="menu-primary">
                            <li class="menu-item curent-menu-item">
                                <a href="/" class="font-color-black">Home</a>
                                <ul class="sub-menu">
                                    <li class="menu-item"><a href="/" class="font-color-black">Home</a></li>
                                    <li class="menu-item"><a href="/constitution" class="font-color-black">Constitution</a></li>
                                    <li class="menu-item"><a href="/" class="font-color-black">User Guide</a></li>
                                    <li class="menu-item"><a href="/" class="font-color-black">Acronyms</a></li>
                                    <li class="menu-item"><a href="/" class="font-color-black">Countries</a></li>
                                </ul>
                            </li>
                            <li class="menu-item">
                                <a href="/locations" class="font-color-black">Locations</a>
                            </li>
                            <li class="menu-item">
                                <a href="/" class="font-color-black">HVSIA Info</a>
                                <ul class="sub-menu">
                                    <li class="menu-item"><a href="/" class="font-color-black">Meetings</a></li>
                                    <hr>
                                    <li class="menu-item"><a href="projects2.html" class="font-color-black">Matrix and More</a></li>
                                </ul>
                            </li>
                            <li class="menu-item">
                                <a href="/" class="font-color-black">Links</a>
                                <ul class="sub-menu">
                                    <li class="menu-item"><a href="/" class="font-color-black">pavement interactive website</a></li>
                                    <hr>
                                    <li class="menu-item"><a href="/" class="font-color-black">TRB -AFD40</a></li>
                                </ul>
                            </li>

                            <li class="menu-item">
                                <a href="/contactus" class="font-color-black">Contact us</a>
                            </li>
                        </ul>--}}
                        <ul class="menu-primary">

                            <!--=========== Single Menu ===============-->
                            @foreach(\App\CMS::menus() as $row)
                                <li class="menu-item curent-menu-item">
                                    <a href="
                                    @if($row->redirect_url)
                                    {{$row->redirect_url}}
                                    @else
                                    @if($row->is_index_page)
                                    {{url('/')}}
                                    @else
                                    {{url('/page/'.$row->slug)}}
                                    @endif
                                    @endif
                                            "><b>{{$row->page_name}}</b>

                                        <div class="skewed_slide"></div>
                                    </a>
                                    @if($row->sub_menus()->count() > 0)
                                        <ul class="sub-menu">
                                            @foreach($row->sub_menus() as $child)

                                                <li class="menu-item">
                                                    <a href="
                                                    @if($child->redirect_url)
                                                    {{$child->redirect_url}}
                                                    @else
                                                    @if($child->is_index_page)
                                                    {{url('/')}}
                                                    @else
                                                    {{url('/page/'.$child->slug)}}
                                                    @endif
                                                    @endif
                                                            ">{{$child->page_name}}

                                                        <div class="skewed_slide"></div>
                                                    </a>


                                                </li>


                                            @endforeach
                                        </ul>
                                    @endif

                                </li>
                            @endforeach

                            {{--@auth
                                <li class="menu-item"><a href="#">
                                        <img style="margin-top: 2%;height: 18px;width: 18px;"   src="{{'/uploads'.config('filesystems.storage.avatar_path').auth()->user()->avatar}}" class="  rounded-circle">
                                        <div class="skewed_slide"></div>
                                    </a>
                                    <ul class="sub-menu">
                                        <!-- row 1 -->
                                        <div class="col-md-6">
                                            <ul class="owl-nav-list">
                                                <li><a href="{{route('member_dashboard')}}" class="">Member Dashboard</a></li>
                                                <li>
                                                    <a href="{{ route('logout') }}" class="" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                        {{ __('Logout') }}
                                                    </a>
                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        @csrf
                                                    </form>
                                                </li>
                                            </ul>
                                        </div>


                                    </ul>
                                </li>
                                @else
                                    <li class="menu-item"><a href="#"><b>Login</b>
                                            <div class="skewed_slide"></div>
                                        </a>
                                        <ul class="sub-menu">
                                            <!-- row 1 -->
                                            <div class="col-md-6">
                                                <a class="owl-nav-title" href="#">Login & Register</a>
                                                <ul class="owl-nav-list">
                                                    <li><b><a href="{{route('login')}}"><i></i>Login</a></b></li>
                                                    <li><a href="{{route('register')}}"><i></i> Register</a></li>
                                                </ul>
                                            </div>


                                        </ul>
                                    </li>
                                    @endauth--}}
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>