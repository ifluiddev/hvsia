<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="@yield('meta_desc')">
    <title>@yield('page_title')</title>
    <link rel="stylesheet" href="{{asset('Front-Assets/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('Front-Assets/vendor/css-hamburgers/dist/hamburgers.min.css')}}">
    <link rel="stylesheet" href="{{asset('Front-Assets/vendor/slick/slick-theme.css')}}">
    <link rel="stylesheet" href="{{asset('Front-Assets/vendor/slick/slick.css')}}">
    <link rel="stylesheet" href="{{asset('Front-Assets/fonts/font-awesome-5/css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="{{asset('Front-Assets/fonts/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('Front-Assets/vendor/revolution/layers.css')}}">
    <link rel="stylesheet" href="{{asset('Front-Assets/vendor/revolution/navigation.css')}}">
    <link rel="stylesheet" href="{{asset('Front-Assets/vendor/revolution/settings.css')}}">
    <link rel="stylesheet" href="{{asset('Front-Assets/vendor/revolution/settings-source.css')}}">
    <link rel="stylesheet" href="{{asset('Front-Assets/vendor/revolution/tp-color-picker.css')}}">
    <link rel="stylesheet" href="{{asset('Front-Assets/vendor/nouislider/nouislider.min.css')}}">
    <link rel="stylesheet" href="{{asset('Front-Assets/css/style.css')}}">
    <link rel="shortcut icon" href="{{asset('/Front-Assets/favicon.ico')}}">
</head>

<body>
<!-- page load-->
<div class="images-preloader">
    <div id="preloader_1" class="rectangle-bounce">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
@include('layouts.backend.inc.preview-topNavigation')
<main>
@yield('content')
</main>
@include('layouts.frontend.inc.footer')

<!-- Back to top -->
<div id="back-to-top">
    <i class="fa fa-angle-up"></i>
</div>

<!-- JS -->
<script src="{{asset('Front-Assets/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('Front-Assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('Front-Assets/vendor/slick/slick.min.js')}}"></script>
<script src="{{asset('Front-Assets/js/masonry.pkgd.min.js')}}"></script>
<script src="{{asset('Front-Assets/js/imagesloaded.pkgd.js')}}"></script>
<script src="{{asset('Front-Assets/js/isotope-docs.min.js')}}"></script>
<script src="{{asset('Front-Assets/vendor/nouislider/nouislider.min.js')}}"></script>
<script src="{{asset('Front-Assets/vendor/sweetalert/sweetalert.min.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAEmXgQ65zpsjsEAfNPP9mBAz-5zjnIZBw"></script>
<script src="{{asset('Front-Assets/js/theme-map.js')}}"></script>

<!-- REVOLUTION JS FILES -->
<script src="{{asset('Front-Assets/js/revolution/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{asset('Front-Assets/js/revolution/jquery.themepunch.revolution.min.js')}}"></script>

<!-- SLICEY ADD-ON FILES -->
<script src='{{asset('Front-Assets/js/revolution/revolution.addon.slicey.min.js?ver=1.0.0')}}'></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<!-- <script src="{{asset('Front-Assets/js/revolution/extensions/revolution.extension.actions.min.js')}}"></script>
<script src="{{asset('Front-Assets/js/revolution/extensions/revolution.extension.carousel.min.js')}}"></script>
<script src="{{asset('Front-Assets/js/revolution/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script src="{{asset('Front-Assets/js/revolution/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script src="{{asset('Front-Assets/js/revolution/extensions/revolution.extension.migration.min.js')}}"></script>
<script src="{{asset('Front-Assets/js/revolution/extensions/revolution.extension.navigation.min.js')}}"></script>
<script src="{{asset('Front-Assets/js/revolution/extensions/revolution.extension.parallax.min.js')}}"></script>
<script src="{{asset('Front-Assets/js/revolution/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script src="{{asset('Front-Assets/js/revolution/extensions/revolution.extension.video.min.js')}}"></script> -->


<script src="{{asset('Front-Assets/js/main.js')}}"></script>
</body>

</html>