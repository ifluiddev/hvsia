<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCMSFootersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_footers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cms_id');
            $table->foreign('cms_id')
                ->references('id')->on('cms')
                ->onDelete('cascade');
            $table->unsignedInteger('menu_order')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_footers');
    }
}
