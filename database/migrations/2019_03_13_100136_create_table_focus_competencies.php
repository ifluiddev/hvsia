<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFocusCompetencies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('focus_competencies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('focus_area_id')->unsigned()->nullable();
            $table->foreign('focus_area_id')
                ->references('id')
                ->on('focus_areas')
                ->onDelete('cascade');
            $table->integer('competency_area_id')->unsigned()->nullable();
            $table->foreign('competency_area_id')
                ->references('id')
                ->on('competency_areas')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('focus_competencies');
    }
}
