<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Hash;

class InsertSuperAdminUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $role= App\Role::where('role_name','Administrator')->first();
        $user=  new App\Admin();
        $user->name='Admin';
        $user->email='admin@hvsia.co.za';
        $user->job_title='Manager';
        $user->role_id = $role->id;
        $user->password=  Hash::make('hvsiaadmin');
        $user->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        App\Admin::where('email','admin@hvsia.co.za')->first()->delete();
    }
}
