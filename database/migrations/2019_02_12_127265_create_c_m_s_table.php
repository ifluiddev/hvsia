<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCMSTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('page_name');
            $table->longText('content')->nullable();
            $table->string('template')->default('cms');
            $table->boolean('page_status')->default(0);
            $table->unsignedInteger('page_parent')->default(0);
            $table->unsignedInteger('menu_order')->default(0);
            $table->boolean('is_visible')->default(1);
            $table->boolean('is_index_page')->default(0);
            $table->string('redirect_url')->nullable();
            $table->string('slug')->nullable();
            $table->text('page_title')->nullable();
            $table->text('page_meta_description')->nullable();
            $table->string('banner_description')->nullable();
            $table->timestamps();
        });

        // Full Text Index

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {


        Schema::dropIfExists('cms');
    }
}
